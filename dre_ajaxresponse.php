<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 10.08.16
 * Time: 12:28
 */

require_once __DIR__.'/bootstrap.php';
header("content-type:application/json; charset=utf-8");

if($_POST['class'] === 'bn_oxbasket' || $_GET['class'] === 'bn_oxbasket') {
    /**
     * we can pass any action like block, follow, unfollow, send PM....
     * if we get a 'follow' action then we could take the user ID and create a SQL command
     * but with no database, we can simply assume the follow action has been completed and return 'ok'
     **/
    print_r($_GET);

    $basket = oxNew('bn_oxbasket');

    $arrReturn = array();
    foreach($this->getSession()->getBasket()->getContents() AS $objBasketItem) {
        $arrReturn['basketitem'][] = array(
            'title'     =>  $objBasketItem->getTitle(),
        );
    }

    print_r(json_encode($arrReturn));

    exit();
}
