<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eShop PE
 */

/**
 * Validates if there are no Javascript.
 */
class oxNoJsValidator
{
    /**
     * Checks if provided config value is not vulnerable.
     *
     * @param string $configValue
     *
     * @return bool
     */
    public function isValid($configValue)
    {
        $isValid = true;
        if (preg_match('/<script.*>/', $configValue) !== 0) {
            $isValid = false;
        }

        return $isValid;
    }
}
