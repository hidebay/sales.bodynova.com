<?php
require_once dirname(__FILE__).'/dre_bnoxprice.php';

class dre_oxbasket extends dre_oxbasket_parent
{
    protected function _calcItemsPrice()
    {
        //
        parent::_calcItemsPrice();

        $oDiscountList = oxRegistry::get("oxDiscountList");

        foreach ( $this->_aBasketContents as $oBasketItem ) {

            if ( !$oBasketItem->isDiscountArticle() && ( $oArticle = $oBasketItem->getArticle( true ) ) ) {

                $oBasketPrice = $oArticle->getBasketPrice( $oBasketItem->getAmount(), $oBasketItem->getSelList(), $this );

                if ( !$oArticle->skipDiscounts() && $this->canCalcDiscounts() ) {
                    // apply basket type discounts for item
                    $aDiscounts = $oDiscountList->getBasketItemDiscounts( $oArticle, $this, $this->getBasketUser() );
                    //
                    reset( $aDiscounts );
                    $oBasketItem->clearUsedDiscount();
                    foreach ( $aDiscounts as $oDiscount ) {

                        // kopiere original objkt
                        $oCopyBasketPrice = clone $oBasketPrice;

                        // wende rabatt an
                        $oBasketPrice->setDiscount($oDiscount->getAddSum(), $oDiscount->getAddSumType());
                        $oBasketPrice->calculateDiscount();

                        // den benutzen discount wert mit namen des rabatts an den item im basket speichern
                        $oBasketItem->addUsedDiscount(
                            $oDiscount->oxdiscount__oxtitle->rawValue,
                            ($oCopyBasketPrice->getPrice() - $oBasketPrice->getPrice())
                        );
                    }
                }
            }

        }
    }



    public function getUsedDiscountsAsGroup()
    {
        $arrUsedDiscounts = array('Staffelpreis'=>0);
        foreach($this->_aBasketContents AS $oBasketItem) {

            if(count($oBasketItem->arrUsedDiscounts)) {
                foreach($oBasketItem->arrUsedDiscounts AS $strName=>$dValue) {
                    // initalisieren mit 0
                    if(!isset($arrUsedDiscounts[$strName]))
                        $arrUsedDiscounts[$strName] = 0;
                    //
                    $arrUsedDiscounts[$strName]+= $dValue * $oBasketItem->getAmount();
                    // ende
                }
            } else {
                //
                $dStaffelMultiplyed = $oBasketItem->getPrice()->dPriceBeforeStaffel * $oBasketItem->getAmount();
                $dBrutto = $oBasketItem->getPrice()->getPrice();
                //
                if($dStaffelMultiplyed != $dBrutto) {
                    $arrUsedDiscounts['Staffelpreis']+= $dStaffelMultiplyed-$dBrutto;
                }
            }
        }
        return $arrUsedDiscounts;
    }


    public function getBruttoSum()
    {
        $dBrutto = parent::getBruttoSum();
        foreach($this->_aBasketContents AS $oBasketItem) {
            if(count($oBasketItem->arrUsedDiscounts)) {
                foreach($oBasketItem->arrUsedDiscounts AS $strName=>$dValue) {
                    $dBrutto += $dValue * $oBasketItem->getAmount();
                }
            } else {
                //
                $dStaffelMultiplyed = $oBasketItem->getPrice()->dPriceBeforeStaffel * $oBasketItem->getAmount();
                $dTmpBrutto = $oBasketItem->getPrice()->getPrice();
                //
                if($dStaffelMultiplyed != $dTmpBrutto) {
                    $dBrutto+= $dStaffelMultiplyed-$dTmpBrutto;
                }
            }
        }

        return $dBrutto;
    }


    public function getBasketContents()
    {
        return $this->_aBasketContents;
    }
}