<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 18.11.14
 * Time: 17:10
 */

class dre_oxbasketitem extends dre_oxbasketitem_parent{
    public function getRegularUnitPrice()
    {
        //
        $ret = $this->_oRegularUnitPrice;

        if($ret->dPriceBeforeStaffel)
            $ret->setPrice($ret->dPriceBeforeStaffel);

        return $ret;
    }

    public $arrUsedDiscounts = array();
    public function clearUsedDiscount()
    {
        $this->arrUsedDiscounts = array();
    }

    public function addUsedDiscount($strName, $dValue)
    {
        // initialisieren wenn nicht gesetzt
        if(!isset($this->arrUsedDiscounts[$strName]))
            $this->arrUsedDiscounts[$strName] = 0;
        //
        $this->arrUsedDiscounts[$strName]+= $dValue *  $this->getAmount();
        // ende
    }
}