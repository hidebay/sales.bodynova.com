<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 25.01.15
 * Time: 22:33
 */

class debug {

    protected $_var = null;
    protected $_all = null;

    /*
    function __construct($param){
        if (isset($param)){
            if (is_array($param)){
                $this->_all = $param;
            }
        }
    }
    */

    function dies($param){
        $this->_var = $param;

        echo '<pre>';
        print_r($this->_var);
        echo '</pre>';
    }

    function all($param){
        if (is_array($param)){
            foreach($param as $key){
                $this->dies($key);
            }
        }
    }
}