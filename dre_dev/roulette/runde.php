<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 25.01.15
 * Time: 20:27
 */

class runde {

    protected $rundennr = null;
    protected $einsatz  = null;
    protected $farbe    = null;



    function __construct($param) {
        if (is_int($param)) {
            // numerical ID was given
            $rundennr = $param;
        } elseif (is_array($param)) {
            if (isset($param['rundennr'])) {
                $this->rundennr = $param['rundennr'];
            }
            if (isset($param['einsatz'])) {
                $this->einsatz = $param['einsatz'];
            }
            if (isset($param['farbe'])) {
                $this->farbe = $param['farbe'];
            }
        }
    }

    function spiel(){
        #$this::player->seteinsatz(4);
    }

    function getergebnis(){
        return array(
            'Rundennr'  =>  $this->rundennr,
            'einsatz'   =>  $this->einsatz,
            'farbe'     =>  $this->farbe,

        );
    }

}

// ID
//new User(12345);
// email
//new User(array('email'=>'user@example.com'));
// username
//new User(array('username'=>'John Doe'));
// multiple
