<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 25.01.15
 * Time: 21:56
 */

class tisch {

    protected $_nummer         = null;
    protected $_farbe          = null;
    protected $_playerEinsatz  = null;

    public function play(){
        $nummer = rand(0, 30);
        $this->_nummer = $nummer;
        $this->_farbe = $this->calfarbe();
        return array(
            'nummer'    => $nummer,
            'farbe'     => $this->_farbe,
        );
    }

    public function calfarbe(){

        if ($this->_nummer == 0){
            $this->_farbe = 'gruen';

        } else {

            if ($this->_nummer % 2 != 0){
                $this->_farbe = 'schwarz';
            } else {
                $this->_farbe = 'rot';
            }
            return $this->_farbe;
        }
    }

    public function playerEinsatz($param){
        if(isset($param)){
            if(is_array($param)){
                $ergebnis = array();

                if($this->_farbe == $param['farbe']){
                    $ergebnis['status'] = 'gewinnt';
                    $ergebnis['einsatz'] = $param['einsatz'];
                    #$this->_playerEinsatz = $ergebnis['einsatz'];
                }else{
                    $ergebnis['status'] = 'verloren';
                    $ergebnis['einsatz'] = 0;
                    $this->_playerEinsatz = $ergebnis['einsatz'];
                }
                return $ergebnis;
            }
        }else{
            echo 'Fehlender Parameter!!!';
        }

    }

    /*
    public function spielt($param){
        $ergebnis = array(
            #'farbe' => $this->getfarbe(),
        );

        if (is_array($param)){

            $ergebnis['farbe']  =   $this->getfarbe();
            $ergebnis['zahl']   =   $this->_nummer;
            #echo 'B: '.$ergebnis['farbe'];
        }else{

        }

        return $ergebnis;
    }
    */
    
    public function getNummer(){
        return $this->_nummer;
    }
    public function getFarbe(){
        return $this->_farbe;
    }
}