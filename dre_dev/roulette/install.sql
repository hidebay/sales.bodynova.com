/*
 MySql
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `runden`
-- ----------------------------
DROP TABLE IF EXISTS `runden`;
CREATE TABLE `runden` (
  `ID` int(3) NOT NULL AUTO_INCREMENT COMMENT 'ID oder Rundennummer',
  `spieler` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Name des Spielers',
  `gesetzt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'worauf hat der Spieler gesetzt',
  `farbegesetzt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'gesetzte Farbe',
  `zahlgekommen` int(10) DEFAULT NULL COMMENT 'Zahl in der die Kugel landet',
  `farbegekommen` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'gekommene Farbe',
  `einsatz` int(10) DEFAULT NULL COMMENT 'Einsatz des Spielers',
  `haben` int(10) DEFAULT NULL COMMENT 'Gesamtguthaben des Spielers',
  `gewinn` int(10) DEFAULT NULL COMMENT 'Gewinn in dieser Runde',
  `verlust` int(10) DEFAULT NULL COMMENT 'Verlust aus dieser Runde',
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='Gespielte Runden und Ergebnisse';

SET FOREIGN_KEY_CHECKS = 0;


/*
CREATE TABLE `roulette`.`runden` (
  `ID` int(3) NOT NULL AUTO_INCREMENT COMMENT 'ID oder Rundennummer',
  `spieler` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Name des Spielers',
  `gesetzt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'worauf hat der Spieler gesetzt',
  `farbegesetzt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'gesetzte Farbe',
  `zahlgekommen` int(10) DEFAULT NULL COMMENT 'Zahl in der die Kugel landet',
  `farbegekommen` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'gekommene Farbe',
  `einsatz` int(10) DEFAULT NULL COMMENT 'Einsatz des Spielers',
  `haben` int(10) DEFAULT NULL COMMENT 'Gesamtguthaben des Spielers',
  `gewinn` int(10) DEFAULT NULL COMMENT 'Gewinn in dieser Runde',
  `verlust` int(10) DEFAULT NULL COMMENT 'Verlust aus dieser Runde',
  `TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Timestamp'
  PRIMARY KEY (`ID`)
) COMMENT='gespielte Runden des Spielers';

ALTER TABLE `roulette`.`runden`
ADD COLUMN `spieler` varchar(20) AFTER `ID`,
CHANGE COLUMN `gesetzt` `gesetzt` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'worauf hat der Spieler gesetzt' AFTER `spieler`,
CHANGE COLUMN `farbegesetzt` `farbegesetzt` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'gesetzte Farbe' AFTER `gesetzt`,
CHANGE COLUMN `zahlgekommen` `zahlgekommen` int(10) DEFAULT NULL COMMENT 'Zahl in der die Kugel landet' AFTER `farbegesetzt`,
CHANGE COLUMN `farbegekommen` `farbegekommen` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'gekommene Farbe' AFTER `zahlgekommen`,
CHANGE COLUMN `einsatz` `einsatz` int(10) DEFAULT NULL COMMENT 'Einsatz des Spielers' AFTER `farbegekommen`,
CHANGE COLUMN `haben` `haben` int(10) DEFAULT NULL COMMENT 'Gesamtguthaben des Spielers' AFTER `einsatz`,
CHANGE COLUMN `gewinn` `gewinn` int(10) DEFAULT NULL COMMENT 'Gewinn in dieser Runde' AFTER `haben`,
CHANGE COLUMN `verlust` `verlust` int(10) DEFAULT NULL COMMENT 'Verlust aus dieser Runde' AFTER `gewinn`;
*/