#!/usr/bin/env bash
grep -R --include=*.tpl '\boxmultilang\b' tpl/ | sed -r "s/ident=([^$])/\\nident=\1/g" | grep ^ident= | sed -r 's/}.*$//' | tr "'" '"' | sort -u | while read LINE; do KEY=$(echo "$LINE" | awk -F'"' '{print$2}'); echo -e $LINE"\t"$KEY"\t"$(grep -Rl --include=*.php $KEY {./,../../translations/} | wc -l); done | grep '\b 0$'

# funktioniert leider noch nicht.
