<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 06.02.15
 * Time: 15:59
 */
/*
Tipp:
Standardantwort/FAQ:
Richtig debuggen:
Man bemerkt, dass ein Skript nicht das tut, was es soll.
Man schreibt an den Anfang des Scriptes die Zeile: error_reporting(-1);
Man verwendet ini_set('display_errors', true); damit die Fehler auch angezeigt werden.
Man versucht, die Stelle die daran Schuld sein kann, schonmal einzugrenzen. Falls dies nicht geht, wird zunächst das komplette Skript als fehlerhaft angesehen.
An markanten Stellen im Skript lässt man sich wichtige Variableninhalte ausgeben und ggf. auch in bedingten Anweisungen eine kurze Ausgabe machen, um zu überprüfen, welche Bedingung ausgeführt wurde. Wichtig bei MySQL Fehlern (...not a valid MySQL result resource...): mysqli_error() verwenden oder Abfrage ausgeben und zb mit phpmyadmin testen.
Schritt 5 wird so lange wiederholt, bis Unstimmigkeiten im Skript auffallen
Damit hat man das Problem (Unstimmigkeit) gefunden und kann versuchen diese zu beheben. Hierzu dienen dann die PHP-Dokumentation und andere Quellen als Ratgeber.
Lässt sich das konkrete Problem trotzdem nicht beheben, kann man in Foren um Rat fragen.
Das Programm läuft und man kann die Debug-Ausgaben wieder entfernen.

Die Ausgaben per var_dump oder echo kann man sich ersparen, in dem man einen Debugger benutzt.

Wiki Links:
Ausführliche Informationen zum Thema

Debuggen                        = (http://phpforum.de/forum/showthread.php?s=2a996f616332a18047791e49251687f4&t=216988)
Dokumentationen und Tutorials   = (http://phpforum.de/forum/showthread.php?s=2a996f616332a18047791e49251687f4&t=220115)
in Tutorials                    = (http://phpforum.de/forum/nuwiki.php?s=2a996f616332a18047791e49251687f4&do=book.view&bookid=6)
*/