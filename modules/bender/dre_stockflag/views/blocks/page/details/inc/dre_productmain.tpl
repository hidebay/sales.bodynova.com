[{block name="stockflag"}]
    [{if $oDetailsProduct->isBuyable()}]
        [{if $oDetailsProduct->oxarticles__bnflagbestand->value == 2}]
        [{*}]<!-- Der Artikel ist nicht auf Lager und muss erst nachbestellt werden -->[{*}]
            <span class="stockFlag notOnStock">
                [{if $oDetailsProduct->oxarticles__oxnostocktext->value}]
                    [{$oDetailsProduct->oxarticles__oxnostocktext->value}]
                [{elseif $oViewConf->getStockOffDefaultMessage()}]
                    [{oxmultilang ident="MESSAGE_NOT_ON_STOCK"}]
                [{/if}]
                [{if $oDetailsProduct->getDeliveryDate()}]
                    [{oxmultilang ident="AVAILABLE_ON"}] [{$oDetailsProduct->getDeliveryDate()}]
                [{/if}]
            </span>
        [{*}]<!--[{elseif $oDetailsProduct->getStockStatus() == 1}]-->[{*}]
        [{elseif $oDetailsProduct->oxarticles__bnflagbestand->value == 1}]
        [{*}]<!-- Wenige Exemplare auf Lager -schnell bestellen! -->[{*}]
            <span class="stockFlag lowStock">
                [{oxmultilang ident="LOW_STOCK"}]
            </span>
        [{*}]<!--[{elseif $oDetailsProduct->getStockStatus() == 0}]-->[{*}]
        [{elseif $oDetailsProduct->oxarticles__bnflagbestand->value == 0}]
        [{*}]<!-- Sofort lieferbar. -->[{*}]
            <span class="stockFlag">
                [{if $oDetailsProduct->oxarticles__oxstocktext->value}]
                    [{$oDetailsProduct->oxarticles__oxstocktext->value}]
                [{elseif $oViewConf->getStockOnDefaultMessage()}]
                    [{oxmultilang ident="READY_FOR_SHIPPING"}]
                [{/if}]
            </span>
        [{/if}]
    [{/if}]
[{/block}]
