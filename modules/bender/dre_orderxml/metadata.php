<?php
/**
 */

/**
 * Metadata version
 */
$sMetadataVersion = '0.1';

/**
 * Module information
 *	Dieses Modul erweitert den Head im Admin um den Cache zu leeren.
 */
$aModule = array(
    'id'           => 'dre_orderxml',
    'title'        => '<img src="../favicon.ico" title="Body Order XML">ody Order als XML',
    'description'  => array(
            'de' => 'Modul um die Orders als XML Datei in der FritzBox zu speichern.',
            'en' => '',
    ),
    'thumbnail'    => 'picture.png',
    'version'      => '0.1',
    'author'       => 'Andre Bender',
    'url'          => 'www.bodynova.de',
    'email'        => 'a.bender@bodynova.de',
    'extend'       => array(
    'oxorder'		=>	'bender/dre_orderxml/models/dre_oxorderxml',
    ),
);
