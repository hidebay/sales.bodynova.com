<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 19.07.16
 * Time: 15:01
 */
class orderarticles2xml{
    protected $oxorderid;

    protected $ordernr;

    protected $arrOxorderArticlesIds;

    protected $arrOxorderArticles = array();

    protected $dataArray = array();
    
    public function getOrderId(){
        return $this->oxorderid;
    }
    
    public function setOrderId($value){
        if(isset($value)){
            $this->oxorderid = $value;
        }
    }

    public function getOrderNr(){
        return $this->ordernr;
    }

    public function setOrderNr($value){
        if(isset($value)){
            $this->ordernr = $value;
        }
    }

    public function getOrderArticles(){
        return $this->arrOxorderArticles;
    }

    public function setOrderArticleIds(){
        $sql =  "SELECT OXID FROM oxorderarticles where oxorderid = '".$this->getOrderId()."'";
        $this->arrOxorderArticlesIds = oxDb::getDb()->getArray($sql);
    }

    public function getOrderArticlesIds(){
        return $this->arrOxorderArticlesIds[0];
    }

    public function setOrderArticles(){
        if(count($this->arrOxorderArticlesIds)){

            foreach($this->arrOxorderArticlesIds as $value){
                $article = oxnew('oxorderarticle');
                #echo $value[0].'<br>';
                $article->load($value[0]);
                #echo 'Article: '.$article->oxorderarticles__oxid->value . '<br>';

                $this->arrOxorderArticles[] =  $article;
            }
            #echo '<pre>';
            #print_r($this->arrOxorderArticles);
        }
    }

    public function setOrderArticleXmlArray(){
        
        foreach($this->arrOxorderArticles as $oxorderarticle){
            $this->dataArray[] = array('ORDER' => array(
                'OXID'              => $oxorderarticle->oxorderarticles__oxid->value,
                'OXORDERID'    => $oxorderarticle->oxorderarticles__oxorderid->value,
                'OXAMOUNT'     => $oxorderarticle->oxorderarticles__oxamount->value,
                'OXARTID'      => $oxorderarticle->oxorderarticles__oxartid->value,
                'OXARTNUM'     => $oxorderarticle->oxorderarticles__oxartnum->value,
                'OXTITLE'      => $oxorderarticle->oxorderarticles__oxtitle->value,
                'OXSHORTDESC'  => $oxorderarticle->oxorderarticles__oxshortdesc->value,
                'OXSELVARIANT' => $oxorderarticle->oxorderarticles__oxselvariant->value,
                'OXNETPRICE'   => str_replace('.',',',round($oxorderarticle->oxorderarticles__oxnetprice->value, 2)),
                'OXBRUTPRICE'  => str_replace('.',',',round($oxorderarticle->oxorderarticles__oxbrutprice->value, 2)),
                'OXVATPRICE'   => str_replace('.',',',round($oxorderarticle->oxorderarticles__oxvatprice->value, 2)),
                'OXVAT'        => str_replace('.',',',round($oxorderarticle->oxorderarticles__oxvat->value,2)),
                'OXPERSPARAM'  => $oxorderarticle->oxorderarticles__oxpersparam->value,
                'OXPRICE'      => str_replace('.',',',round($oxorderarticle->oxorderarticles__oxprice->value, 2)),
                'OXBPRICE'     => str_replace('.',',',round($oxorderarticle->oxorderarticles__oxbprice->value, 2)),
                'OXNPRICE'     => str_replace('.',',',round($oxorderarticle->oxorderarticles__oxnprice->value, 2)),
                'OXWRAPID'     => $oxorderarticle->oxorderarticles__oxwrapid->value,
                'OXEXTURL'     => $oxorderarticle->oxorderarticles__oxexturl->value,
                'OXURLDESC'    => $oxorderarticle->oxorderarticles__oxurldesc->value,
                'OXURLIMG'     => $oxorderarticle->oxorderarticles__oxurlimg->value,
                'OXTHUMB'      => $oxorderarticle->oxorderarticles__oxthumb->value,
                'OXPIC1'       => $oxorderarticle->oxorderarticles__oxpic1->value,
                'OXPIC2'       => $oxorderarticle->oxorderarticles__oxpic2->value,
                'OXPIC3'       => $oxorderarticle->oxorderarticles__oxpic3->value,
                'OXPIC4'       => $oxorderarticle->oxorderarticles__oxpic4->value,
                'OXPIC5'       => $oxorderarticle->oxorderarticles__oxpic5->value,
                'OXWEIGHT'     => round($oxorderarticle->oxorderarticles__oxweight->value, 2),
                'OXSTOCK'      => $oxorderarticle->oxorderarticles__oxstock->value,
                'OXDELIVERY'   => $oxorderarticle->oxorderarticles__oxdelivery->value,
                'OXINSERT'     => $oxorderarticle->oxorderarticles__oxinsert->value,
                'OXTIMESTAMP'  => $oxorderarticle->oxorderarticles__oxtimestamp->value,
                'OXLENGTH'     => $oxorderarticle->oxorderarticles__oxlength->value,
                'OXWIDTH'      => round($oxorderarticle->oxorderarticles__oxwidth->value, 2),
                'OXHEIGHT'     => round($oxorderarticle->oxorderarticles__oxheight->value, 2),
                'OXFILE'       => $oxorderarticle->oxorderarticles__oxfile->value,
                'OXSEARCHKEYS' => $oxorderarticle->oxorderarticles__oxsearchkeys->value,
                'OXTEMPLATE'   => $oxorderarticle->oxorderarticles__oxtemplate->value,
                'OXQUESTIONEMAIL'   => $oxorderarticle->oxorderarticles__oxquestionemail->value,
                'OXISSEARCH'   => $oxorderarticle->oxorderarticles__oxissearch->value,
                'OXFOLDER'     => $oxorderarticle->oxorderarticles__oxfolder->value,
                'OXSUBCLASS'   => $oxorderarticle->oxorderarticles__oxsubclass->value,
                'OXSTORNO'     => $oxorderarticle->oxorderarticles__oxstorno->value,
                'OXORDERSHOPID'=> $oxorderarticle->oxorderarticles__oxordershopid->value,
                'OXISBUNDLE'   => $oxorderarticle->oxorderarticles__oxisbundle->value,
                'D3_GALOCATOR' => $oxorderarticle->oxorderarticles__d3_galocator->value,
                'OXSHOP'       =>'haendler',
            ));
        }
    }
    
    public function getOrderArticleXmlArray(){
        return $this->dataArray;
    }

    public function erstelleOrderArticlesXmlDatei(){
        $xml = null;
        try {
            $xml = new array2xml('ROW');
            $arArticles = $this->getOrderArticleXmlArray();
            $anzahlAlleOrders = count($arArticles);
            #echo $anzahlAlleOrders;
            $null = null;
            $id1  = 0;
            //$null = null;


            foreach ($arArticles as $keyarray) {
                $xml->createOrderartNode($keyarray, $null, $anzahlAlleOrders, $id1);
                $id1++;
            }

            #echo $xml;
        } catch (Exception $e) {

            $oxEmail = oxNew('oxemail');
            $oxEmail->setSubject('BODYNOVA FEHLER');
            $oxEmail->setBody("Error in Datei: orderarticles2xml.php function: erstelleOrderArticlesXmlDatei()\n".
            "Geworfener Fehler: ".$e->getMessage());
            $oxEmail->setRecipient('a.bender@bodynova.de', 'André Bender');
            $oxEmail->setReplyTo('server@bodynova.de' , 'Server Bodynova');
            $oxEmail->send();
        }

        $fp = fopen( dirname(__DIR__).'/OrderDaten/'.$this->getOrderNr().'_orderarticles.xml', 'w');
        fwrite($fp, $xml);
        fclose($fp);
    }

    public function ftpUploadFritzBox(){
        try {
            // FTP Upload auf fritz.nas
            $ftp_server = '92.50.75.170';
            $ftp_user = 'haendlershop';
            $ftp_pass = 'peanuts30';
            #$file = dirname(__FILE__).'/'.barcode_outimage($code,getvar('encoding'),getvar('scale'),getvar('mode'));
            #$file = $bild;
            $file = dirname(__DIR__).'/OrderDaten/' . $this->getOrderNr() . '_orderarticles.xml';
            $destination_file = 'JetFlash-Transcend16GB-01/FRITZ/OrderImport/' . $this->getOrderNr() . '_orderarticles.xml';
            $cid = ftp_connect($ftp_server);
            ftp_login($cid, $ftp_user, $ftp_pass);
            ftp_pasv($cid, true);
            ftp_put($cid, $destination_file, $file, FTP_BINARY);
            ftp_close($cid);
        } catch (Exception $e){
            #echo $e->getMessage();

            $oxEmail = oxNew('oxemail');
            $oxEmail->setSubject('BODYNOVA FEHLER');
            $oxEmail->setBody("Error in Datei: orderarticles2xml.php function: ftpUploadFritzBox()\n".
                "Geworfener Fehler: ".$e->getMessage());
            $oxEmail->setRecipient('a.bender@bodynova.de', 'André Bender');
            $oxEmail->setReplyTo('server@bodynova.de' , 'Server Bodynova');
            $oxEmail->send();
        }
    }
}
