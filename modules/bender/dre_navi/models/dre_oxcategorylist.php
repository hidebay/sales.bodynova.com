<?php

class dre_oxcategorylist extends dre_oxcategorylist_parent{
    /**
     * constructs the sql string to get the category list
     *
     * @param bool   $blReverse list loading order, true for tree, false for simple list (optional, default false)
     * @param array  $aColumns  required column names (optional)
     * @param string $sOrder    order by string (optional)
     *
     * @return string
     */
    protected function _getSelectString($blReverse = false, $aColumns = null, $sOrder = null)
    {
        $sViewName  = $this->getBaseObject()->getViewName();
        $sFieldList = $this->_getSqlSelectFieldsForTree($sViewName, $aColumns);
        /*
        //excluding long desc
        if (!$this->isAdmin() && !$this->_blHideEmpty && !$this->getLoadFull()) {
            $oCat = oxNew( 'oxCategory' );
            if (!($this->_sActCat && $oCat->load($this->_sActCat) && $oCat->oxcategories__oxrootid->value)) {
                $oCat = null;
                $this->_sActCat = null;
            }

            $sUnion = $this->_getDepthSqlUnion($oCat, $aColumns);
            $sWhere = $this->_getDepthSqlSnippet($oCat);
        } else {*/
            $sUnion = '';
            $sWhere = '1';
        //}
        //echo "a:";
        if (!$sOrder) {
            $sOrdDir    = $blReverse?'desc':'asc';
            $sOrder     = "oxrootid $sOrdDir, oxleft $sOrdDir";
        }

        return "select $sFieldList from $sViewName where $sWhere $sUnion order by $sOrder";
    }
}