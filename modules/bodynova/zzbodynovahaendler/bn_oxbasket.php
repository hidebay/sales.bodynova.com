<?php

/**
 * @param $a
 * @param $b
 * @return bool
 */
function customBasketSorting($a,$b) {
	return $a->getTitle() > $b->getTitle();
}
function drecustomBasketSorting($a,$b) {
	return $a->getSort() > $b->getSort();
}
/**
 * Class bn_oxbasket
 */
class bn_oxbasket
extends bn_oxbasket_parent
{
	
	/**
	 * Returns basket items array
	 *
	 * @return array
	 */
	public function getContents()
	{
		/*
		//
		if(!count($_POST)) {
			// store keys to articles
			$arrOxidsToKeys = array();
			#$id = 0;
			foreach($this->_aBasketContents AS $strKey=>$objBasketItem) {
				$arrOxidsToKeys[$objBasketItem->getProductId()] = $strKey;
				#$objBasketItem->setSort($id);
				#$id++;
			}

			// Sort the fuck
			usort($this->_aBasketContents, 'drecustomBasketSorting');


			// merge keys again into articles
			$arrRetNew = array();
			$key = 0;
			foreach($this->_aBasketContents AS $objBasketItem) {
				//
				$arrRetNew[$objBasketItem->getProductId()] = $objBasketItem;
				// ende
				#echo '<pre>';
				#print_r($objBasketItem);
				#die();
				#$key++;

			}
			$this->_aBasketContents = $arrRetNew;
			// ende
		}*/
		//
		return $this->_aBasketContents;
		// ende
	}

	/**
	 * @param $userid
	 * @param $wk
	 */
	public function setWkAnsicht(){
		if($_POST){
			$userid = $_POST['userid'];
			if($_POST['wkAnsicht'] === 'erweitert'){
				$wk = '1';
			} else {
				$wk = '0';
			}
			$user = oxNew('oxUser');
			$user->load($userid);
			$user->oxuser__wkansicht->rawValue = $wk;

			echo '<pre>';
			print_r($user);
			die();
			$user->save();
		}

	}

	public function sortneu(){

		$key = array();
		
		
		/*foreach($this->_aBasketContents AS $objBasketItem) {

			//$key[] = $objBasketItem->getBasketItemKey();


			//print_r( $objBasketItem);//->getSort();
			//
			//$arrRetNew[$objBasketItem->getProductId()] = $objBasketItem;
			// ende
			#echo '<pre>';
			#print_r($objBasketItem);
			#die();
			#$key++;
		}*/
		
		
		usort($this->_aBasketContents, 'drecustomBasketSorting');
		#echo '<pre>';
		#print_r($this->_aBasketContents);
		#die();
		return json_encode($this->_aBasketContents);
	}

	/**
	 *
	 * bn_oxbasket!!!
	 *
	 * Adds user item to basket. Returns oxBasketItem object if adding succeeded
	 *
	 * @param string $sProductID       id of product
	 * @param double $dAmount          product amount
	 * @param mixed  $aSel             product select lists (default null)
	 * @param mixed  $aPersParam       product persistent parameters (default null)
	 * @param bool   $blOverride       marker to accumulate passed amount or renew (default false)
	 * @param bool   $blBundle         marker if product is bundle or not (default false)
	 * @param mixed  $sOldBasketItemId id if old basket item if to change it
	 * @param newSort(default null)
	 *
	 * @throws oxOutOfStockException oxArticleInputException, oxNoArticleException
	 *
	 * @return object
	 */
	/*
	public function addToBasket($sProductID, $dAmount, $aSel = null, $aPersParam = null, $blOverride = false, $blBundle = false, $sOldBasketItemId = null , $newSort = null)
	{
		parent::addToBasket$sProductID, $dAmount, $aSel = null, $aPersParam = null, $blOverride = false, $blBundle = false, $sOldBasketItemId = null , $newSort = null);
		$menge = count($this->_aBasketContents);

		// enabled ?
		if (!$this->isEnabled()) {
			return null;
		}

		// basket exclude
		if ($this->getConfig()->getConfigParam('blBasketExcludeEnabled')) {
			if (!$this->canAddProductToBasket($sProductID)) {
				$this->setCatChangeWarningState(true);

				return null;
			} else {
				$this->setCatChangeWarningState(false);
			}
		}

		$sItemId = $this->getItemKey($sProductID, $aSel, $aPersParam, $blBundle);
		if ($sOldBasketItemId && (strcmp($sOldBasketItemId, $sItemId) != 0)) {
			if (isset($this->_aBasketContents[$sItemId])) {
				// we are merging, so params will just go to the new key
				unset($this->_aBasketContents[$sOldBasketItemId]);
				// do not override stock
				$blOverride = false;
			} else {
				// value is null - means isset will fail and real values will be filled
				$this->_changeBasketItemKey($sOldBasketItemId, $sItemId);
			}
		}

		// after some checks item must be removed from basket
		$blRemoveItem = false;

		// initialling exception storage
		$oEx = null;

		if (isset($this->_aBasketContents[$sItemId])) {

			//updating existing
			try {
				// setting stock check status
				$this->_aBasketContents[$sItemId]->setStockCheckStatus($this->getStockCheckMode());
				//validate amount
				//possibly throws exception
				$this->_aBasketContents[$sItemId]->setAmount($dAmount, $blOverride, $sItemId);
				
				/* added by André set sorting
				if(isset($newSort)){
					$this->_aBasketContents[$sItemId]->setSort($newSort);
				}
				
			} catch (oxOutOfStockException $oEx) {
				// rethrow later
			}

		} else {
			//inserting new
			$oBasketItem = oxNew('oxbasketitem');
			try {
				$oBasketItem->setStockCheckStatus($this->getStockCheckMode());
				$oBasketItem->init($sProductID, $dAmount, $aSel, $aPersParam, $blBundle);

				/* added by André
				$oBasketItem->setSort($menge+1);

			} catch (oxNoArticleException $oEx) {
				// in this case that the article does not exist remove the item from the basket by setting its amount to 0
				//$oBasketItem->dAmount = 0;
				$blRemoveItem = true;

			} catch (oxOutOfStockException $oEx) {
				// rethrow later
			} catch (oxArticleInputException $oEx) {
				// rethrow later
				$blRemoveItem = true;
			}

			$this->_aBasketContents[$sItemId] = $oBasketItem;
		}

		//in case amount is 0 removing item
		if ($this->_aBasketContents[$sItemId]->getAmount() == 0 || $blRemoveItem) {
			$this->removeItem($sItemId);
		} elseif ($blBundle) {
			//marking bundles
			$this->_aBasketContents[$sItemId]->setBundle(true);
		}

		//calling update method
		$this->onUpdate();

		if ($oEx) {
			throw $oEx;
		}

		// notifying that new basket item was added
		if (!$blBundle) {
			$this->_addedNewItem($sProductID, $dAmount, $aSel, $aPersParam, $blOverride, $blBundle, $sOldBasketItemId);
		}

		// returning basket item object
		if (is_a($this->_aBasketContents[$sItemId], 'oxBasketItem')) {
			$this->_aBasketContents[$sItemId]->setBasketItemKey($sItemId);
		}
		return $this->_aBasketContents[$sItemId];
	}
	*/
}
