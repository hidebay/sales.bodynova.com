<?php
/**
 * Created by PhpStorm.
 * User: manuelgeil
 * Date: 06.10.14
 * Time: 12:43
 */

class ajaxbasket extends oxUBase {

	// teplate for the view
	protected $_sThisTemplate = 'bn_json.tpl';

	/**
	 * Adds a whole bulk (array) of items to the basket:
	 *
	 *  key => oxid
	 *  val => amount
	 */
	public function addBulk() {
		//
		$arrResponse = array(
			'oxid'              =>  $_GET['oxid'],
			'added'             =>  array(),
			'itemcount_before'  =>  $this->getSession()->getBasket()->getItemsCount(),
			'PostAmount'        =>  $_POST['amount'],
		);
		$obBasket = $this->getSession()->getBasket();
		
		// add items if present
		$intCount = 0;
		if(isset($_POST['amount'])) {
			$oBasket = oxNew('oxcmp_basket');
			//$oBasket->_setLastCallFnc('tobasket');
			$arrResponse['vorher'] = $obBasket->getContents();
			$aProducts = array();
			// walk through each item and add it
			foreach ( $_POST['amount'] AS $oxid => $intAmount) {
				if($intAmount) {
					
					$intCount+= $intAmount;
					//$aProducts[] = $oBasket->_getItems($oxid, $intAmount, $_POST['sel'][$oxid], null, true);
					$arrResponse['tobasket'] = $oxid.'/'.$intAmount.'/'.$_POST['sel'][$oxid].'/'.null.'/'.true;
					$arrResponse['basket'] = $oBasket->tobasket( $oxid, $intAmount, $_POST['sel'][$oxid],null, true );
					$arrResponse['added'] = array($oxid, $intAmount, $_POST['sel'][$oxid]);
				}
			}
			
		}
		$session = $this->getSession();
		$session->saveBasket();
		//$arrResponse['arproducts'] = $aProducts;
		//$obBasket->onUpdate();
		//$obBasket->calculateBasket(true);
		
		$arrResponse['nachher'] = $obBasket->getContents();
		$arrResponse['BasketInfo'] = $obBasket->getBasketSummary();
		//$oBasket->_setLastCall($oBasket->_getLastCallFnc(), $aProducts, $oBasketInfo);
		//
		$arrResponse['itemscount'] = $arrResponse['itemcount_before'] + $intCount;
		//
		$this->_aViewData['response'] = $arrResponse;
		// ende
	}


	/**
	 *
	 */
	public function getCategoryURL()
	{
		//
		$ret = array();
		//
		$oProduct = oxNew( 'oxarticle' );
		$oProduct->load( $_GET['oxid'] );
		//
		$ret['url'] = $oProduct->getCategory()->getLink();
		//
		die(json_encode($ret));
		// ende
	}


	/**
	 *
	 */
	public function getArticleDetails()
	{
		//
		$this->_sThisTemplate = 'bn_acrticledetails.tpl';
		//
		$oProduct = oxNew( 'oxarticle' );
		$oProduct->load( $_GET['oxid'] );
		$oProduct->getLongDescription();
		//
		$this->_aViewData['oProduct'] = $oProduct;
		// ende
	}

	/**
	 *  WK Ansicht speichern
	 * @param $userid
	 * @param $wk
	 */
	public function setWkAnsicht(){
		if($_GET){
			$arrReturn = array('wkansicht' => 0);
			//
			$userid = $_GET['userid'];
			$user = oxNew('oxUser');
			$user->load($userid);
			$user->oxuser__wkansicht->rawValue = $_GET['wkAnsicht'];
			//
			if($user->save()){
				$arrReturn['wkansicht'] = 1;
				die(json_encode($arrReturn));
			} else {
				$arrReturn['wkansicht'] = 0;
				die(json_encode($arrReturn));
			}
		}
	}


	/**
	 *
	 */
	public function getCurrentBasket()
	{
		//
		$oLang = oxRegistry::getLang();
		$oCurrency = $this->getConfig()->getActShopCurrencyObject();
		$sCurrencySign = $oCurrency->sign;
		$sCurrencySide = $oCurrency->side;
		//
		$arrReturn = array(
			'articlecount'	=>	0,
			'pricesum'		=>	0,
		);
		//
		foreach($this->getSession()->getBasket()->getContents() AS $objBasketItem) {
			//
			$strPrice = $oLang->formatCurrency(oxPrice::getPriceInActCurrency($objBasketItem->getUnitPrice()->getNettoPrice()), $oCurrency);
			//
			$arrReturn['articles'][] = array(
				'amount'    =>  $objBasketItem->getAmount(),
				'title'     =>  $objBasketItem->getTitle(),
				'price'     =>  $sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $strPrice : $strPrice . ' ' . $sCurrencySign,
				'sort'		=>  $objBasketItem->getSort(),
			);
			//
			$arrReturn['articlecount']+= $objBasketItem->getAmount();
			$arrReturn['pricesum']+= ($objBasketItem->getUnitPrice()->getNettoPrice() * $objBasketItem->getAmount());
			// ende
		}
		//usort($arrReturn['articles'], "drecustomBasketSorting" );
		//
		$arrReturn['pricesum2'] = $oLang->formatCurrency(oxPrice::getPriceInActCurrency($arrReturn['pricesum']), $oCurrency);
		$arrReturn['pricesum'] = $sCurrencySide == 'Front' ? $sCurrencySign . ' ' . $arrReturn['pricesum'] : $arrReturn['pricesum']. ' ' . $sCurrencySign;
		//
		
		$oLang = oxRegistry::getLang();
		$iLang = $oLang->getTplLanguage();
		
		$arrReturn['summentext'] = $oLang->translateString("TOTAL", $iLang);
		$arrReturn['AufschlagText'] = $oLang->translateString("SURCHARGE", $iLang);
		$arrReturn['DiscountText'] = $oLang->translateString("DISCOUNT", $iLang);
		
		$oBasket = $this->getSession()->getBasket();
		#$oBasket->calculateBasket();
		
		#$oBasket->_save();
		$disId = null;
		#$aDiscounts = oxRegistry::get("oxDiscountList")->getBasketBundleDiscounts($oBasket, $oBasket->getBasketUser());
		
		if(is_array($oBasket->getDiscounts())){
			foreach ($oBasket->getDiscounts() as $oDiscount) {
				$disId = $oDiscount->sOXID;
			}
			
			$oDiscount = oxNew('oxDiscount');
			$oDiscount->loadInLang($iLang, $disId);
			
			$arrReturn['myDiscount'] = $oDiscount;
			
			$arrReturn['discounts'] = $this->getSession()->getBasket()->getDiscounts();
		}
		
		
		/*
		foreach ($this->getSession()->getBasket()->getDiscounts() as $oDiscount) {
			$arrReturn['myDiscount'] = $oDiscount->getId();
		}
		*/
		#$oDiscount = oxNew('oxDiscount');
		#$arrReturn['myDiscount'] = $this->getSession()->getBasket()->getDiscounts()->getId();
		/*
		$oBasket->formatDiscount();
		$arrReturn['myDiscount'] = $oBasket->getDiscounts();*/
		/*
		/*
		$i = 0;
		foreach ($this->getSession()->getBasket()->getDiscounts() as $oDiscount) {
			$arrReturn['DiscountText'.$i] = $oDiscount->getTotalDiscount();//$oLang->translateString($oDiscount->sDiscount, $iLang);
			$i++;
		}
		
		*/
		
		//$oBasket->getDiscountedProductsBruttoPrice();
		//$oBasket->getBasketSummary();
		#$oBasket = $this->getSession()->getBasket();
		$arrReturn['GesamtPreis'] = $oBasket->_getDiscountedProductsSum();//getDiscountedProductsBruttoPrice();
		die(json_encode($arrReturn));
		// ende
	}

	public function test(){
		$arrReturn = array(
			'test' => '123',
		);

		foreach ( $_POST as $post){
			$arrReturn['post'][] = $post;
		}


		die(json_encode($arrReturn));
	}

	/**
	 * @throws null
	 * @throws oxArticleInputException
	 * @throws oxNoArticleException
	 * @throws oxOutOfStockException
	 */
	public function basketSortierung(){
		$arrReturn = null;

		$arrReturn['$_POST'] = $_POST;

		foreach ($_POST as $item)
		{
			$arrReturn['basketitem'] = $item;
			//$arrReturn['basketitem'] = array('key' => $item[0] , array('neuSortübergabe' => $item[1] , 'altSortübergabe' => $item[2]));
		}
		
		$Basket = $this->getSession()->getBasket();
		$arrReturn['$Basket'] = $this->getSession()->getBasket();
		$arrReturn['$Basket->getContents'] = $this->getSession()->getBasket()->getContents();


		/* Test für Sessions */
		#$Session = $this->getSession();
		#$Session->setVariable('testName', 'testvalue');
		#$arrReturn['Session'] = $Session->getVariable('testName');
		/* /Test */

		$id = 0;
		foreach($Basket->getContents() AS $objBasketItem) {
			$arrReturn['$objBasketItem'][] = $objBasketItem;
			$arrReturn['$objBasketItem->getProductId'][] = $objBasketItem->getProductId();
			$arrReturn['$objBasketItem->getBasketItemKey'][] = $objBasketItem->getBasketItemKey();
			$arrReturn['$objBasketItem->getSortAlt'][] = $objBasketItem->getSort();

			foreach ($arrReturn['basketitem'] as $basketitem){
				$arrReturn['innerForEach'][] = $basketitem;
				if($basketitem[0][1] === $objBasketItem->getBasketItemKey()){
					$arrReturn['innerForEach'][]['match'] = $objBasketItem->getBasketItemKey();
					$arrReturn['innerForEach'][]['match']['sort'] = $objBasketItem->getSort();
					$arrReturn['innerForEach'][]['match']['neusort'] = $basketitem[2][1];
					$objBasketItem->setSort($basketitem[2][1]);
					$arrReturn['innerForEach'][]['match']['neusortSaved'] =  $objBasketItem->getSort();
				}
			}

			//$key = array_search($objBasketItem->getBasketItemKey() , $arrReturn['basketitem']['key']);
			//$Basket->addToBasket($objBasketItem->getProductId() , $objBasketItem->getAmount() , null, null, false, false, $objBasketItem->getBasketItemKey(),  $key);
			//addToBasket($sProductID, $dAmount, $aSel = null, $aPersParam = null, $blOverride = false, $blBundle = false, $sOldBasketItemId = null , $newSort = null)
			/*
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['getsortorig'] =  $objBasketItem->getSort();
			$objBasketItem->setSort($key);
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['sort'] = $objBasketItem->getSort();
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['Basketitemkey'] = $objBasketItem->getBasketItemKey();
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['setSort'] = $key;
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['test'] = $arrReturn['basketitem']['key'][$key];
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['id'] = $id;
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['key'] = $key;
			$arrReturn['basketitem']['neuSort']['gespeichert'][$key]['getsort'] = $objBasketItem->getSort();
			$id++;
			#$this->saveSortDB($objBasketItem->getBasketItemKey() , $objBasketItem->getSort());
			*/
			//$arrReturn['basketitem']['check'][] = array( 'obj' => $objBasketItem->getBasketItemKey(), 'arr' => $key );
			//$objBasketItem->setSort();
		}
		#$arrReturn['aproducts'] = oxRegistry::getConfig()->getRequestParameter('aproducts');

		//$Basket->deleteBasket();
		//$Basket->addToBasket()


		//$oxcmpBasket = oxNew('oxcmp_basket');
		//$oxcmpBasket->render();

		//$oxcmpBasket->changebasket();
		//$oxcmpBasket->_addItems($arrReturn['$objBasketItem->getProductId'][0]);
		//$Basket->c

		//$arrReturn['oxcomp_basket'] = 	$oxcmpBasket;
		//$Basket->s
		//$arrneu = $Basket->sortneu();
		#foreach ($arrneu as $item){
		#	$arrReturn['basketitem']['neuSort']['gespeichert']['sortneu'][] = $item;
		#}
		#$Session->freeze();
		#$Basket->_save();
		die(json_encode($arrReturn));
	}

	public function saveSortDB($id , $sort ){

		$oDb = oxDb::getDb();
		$ssql = "update oxuserbasketitems set sort = '" . $sort . "' where OXID = '" . $id . "'";
		$oDb->execute($ssql);
	}

	public function getSortDB($id){
		$sortSelect = "Select sort from oxuserbasketitems where OXID = '".$id."'";
		$sort     = oxDb::getDb()->getOne($sortSelect);
		return $sort;
	}


	/**
	 * User Sidebar-Left-Favoriten aus DB holen
	 */
	public function getUserFavorites(){
		$user = oxNew('bn_oxuser');
		$user->load($_POST['userid']);


		$arrResponse = array();
		$arrResponse['userfav'] = $user->getUserFavorites($_POST['userid']);
		$arrResponse['post'] = $_POST;
		$arrResponse['userid'] = $_POST['userid'];

		die(json_encode($arrResponse));
	}


	public function addUserFavorites(){
		$arrResponse = array();
		$arrResponse['post'] = $_POST;

		$userid = $_POST['item'][0][6][1];

		$user = oxNew('bn_oxuser');
		$user->load($userid);
		$arrFav = array();
		foreach($_POST['item'] as $arrUserFav){

			$arrResponse['debug'][]['oxid']   = $arrUserFav[1][1];
			$arrResponse['debug'][]['text']   = $arrUserFav[2][1];
			$arrResponse['debug'][]['type']   = $arrUserFav[3][1];
			$arrResponse['debug'][]['sort']   = $arrUserFav[4][1];
			$arrResponse['debug'][]['anzahl'] = $arrUserFav[5][1];
			$arrResponse['debug'][]['userid'] = $arrUserFav[6][1];

			$arrFav[] = array(
				0 => 'generated Oxid',
				1 => 1,
				2 => $arrUserFav[6][1],
				3 => $arrUserFav[1][1],
				4 => $arrUserFav[3][1],
				5 => $arrUserFav[4][1],
				6 => $arrUserFav[2][1]
			);
		}

		/*
		foreach($arrFav as $key){
			$arrResponse['$arrFav'][] = $key;
		}
		*/

		$arrResponse['response'] = $user->setUserFavorites($arrFav , $userid);
		//
		// ...code
		//

		//debug:
		$arrResponse['userid'] = $userid;
		$arrResponse['test'] = $arrFav[0][0];
		//$arrResponse['response'] = $user->setUserFavorites($array , $userid);

		die(json_encode($arrResponse));
	}

	public function refreshUserFavorites(){

	}

	public function delUserFavorite(){
		$arrResponse = array();

		$id = $_POST['item'];
		//$arrResponse['id'] = $id;

		$oDb = oxDb::getDb();
		$ssql = "delete from favoriten where OXID = '" . $id . "'";
		$arrResponse['del'] = $oDb->execute($ssql);

		// delete from `dev_haendler_livedb`.`FAVORITEN` where `OXID`='7d43df2dc4617ca5d9376467a40fc063'

		die(json_encode($arrResponse));
	}

	public function delAllFavorites(){
		$arrResponse = array();

		$id = $_POST['userid'];
		//$arrResponse['id'] = $id;

		$oDb = oxDb::getDb();
		$ssql = "delete from favoriten where oxuserid = '" . $id . "'";
		$arrResponse['del'] = $oDb->execute($ssql);

		// delete from `dev_haendler_livedb`.`FAVORITEN` where `OXID`='7d43df2dc4617ca5d9376467a40fc063'

		die(json_encode($arrResponse));
	}
	/**
	 * Testfunktion
	 */
	public function standard(){
		$arrResponse = array();
		$this->_aViewData['response'] = $arrResponse;
		//code...
		//$arrResponse['post'] = $_POST;
		$arrResponse['test'] = 'Hallo Welt';

		die(json_encode($arrResponse));
	}
}
