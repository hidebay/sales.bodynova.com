<?php

class bn_oxcmp_user extends bn_oxcmp_user_parent
{
	protected $arUserFavorites = 'test';


	/**
	 * Sets oxcmp_oxuser::blIsComponent = true, fetches user error
	 * code and sets it to default - 0. Executes parent::init().
	 *
	 * Session variable:
	 * <b>usr_err</b>
	 */
	public function init()
	{
		$this->_saveDeliveryAddressState();
		$this->_loadSessionUser();
		$this->_saveInvitor();

		parent::init();
	}

	/**
	 * Executes parent::render(), oxcmp_user::_loadSessionUser(), loads user delivery
	 * info. Returns user object oxcmp_user::oUser.
	 *
	 * @return  object  user object
	 */
	public function render()
	{
		// checks if private sales allows further tasks
		$this->_checkPsState();

		parent::render();



		return $this->getUser();
	}


	/**
	 * Add flag "forcechangepassword=1" to url an redirects to password change form to a force a password
	 * change when the user passwor still is "bodynova"
	 *
	 * @param $oUser
	 */
	protected function _afterLogin( $oUser )
	{
		// call parent
		$res = parent::_afterLogin( $oUser );
		//
		if($_POST['lgn_pwd']==='bodynova') {
			//
			oxRegistry::getUtils()->redirect( $this->getConfig()->getShopHomeURL() . 'cl=account_password&forcechangepassword=1' );
			return;
			// ende
		}
		//
		#$myConfig = $this->getConfig();
		#oxRegistry::getUtils()->redirect( $myConfig->getShopCurrentUrl() .'cl=account&sourcecl=start' );
		
		#print_r($res);
		#die;
		return 'start';
		// ende
	}


	/**
	 * to avoid the basked moved to a non user session (per default the haendlershop forces a logged in user
	 * we do not move the user basket back into a session where no user is present
	 */
	
	protected function _afterLogout()
	{
		//
		parent::_afterLogout();
		// resetting & recalc basket
		/*if ( ( $oBasket = $this->getSession()->getBasket() ) ) {
			$oBasket->resetUserInfo();
			$oBasket->onUpdate();
			$oBasket->deleteBasket();
		}*/
		$myConfig = $this->getConfig();
		oxRegistry::getUtils()->redirect( $myConfig->getShopCurrentUrl() .'cl=account&sourcecl=start' );
		
		// ende
	}

	

	/**
	 *
	 */
	public function logout()
	{
		parent::logout();
		
		//oxRegistry::getUtils()->redirect( $this->getConfig()->getShopHomeURL() .'cl=account&sourcecl=start&redirected=1');
		
		$myConfig = $this->getConfig();
		/*
		echo '<br>redirect: ';
		echo oxRegistry::getConfig()->getRequestParameter('redirect');
		
		echo '<br> Login Status:';
		echo $this->getLoginStatus();
		
		echo '<br>sslurl: ';
		echo $myConfig->getConfigParam('sSSLShopURL');
		
		
		echo '<br>logoutlink: ';
		echo $this->_getLogoutLink();
		*/
		
		//if (oxRegistry::getConfig()->getRequestParameter('redirect') && $myConfig->getConfigParam('sSSLShopURL')) {
		//	oxRegistry::getUtils()->redirect($this->_getLogoutLink());
		//}
		/*
		echo '<br>home dir: ';
		echo $this->getConfig()->getShopHomeURL();
		
		echo '<br>getShopUrl: ';
		echo $myConfig->getShopUrl();
		
		echo '<br>getShopSecureHomeUrl(): ';
		echo $myConfig->getShopSecureHomeUrl();
		
		echo '<br>getShopCurrentUrl: ';
		echo $myConfig->getShopCurrentUrl();
		*/
		
		//weiterleitung funktioniert bei nicht angemeldet bleiben:
		oxRegistry::getUtils()->redirect( $myConfig->getShopCurrentUrl() .'cl=account&sourcecl=start' );
		
		}

	public function getUserFavorites(){
		return $this->arUserFavorites;
	}
}
