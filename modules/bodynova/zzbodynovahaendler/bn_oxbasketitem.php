<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 09.08.16
 * Time: 11:56
 */
class bn_oxbasketitem extends oxBasketItem{


    public $sort = 0;
	protected $_artnum = null;
	protected $_ve = null;
    
    public function __construct() {
        parent::__construct();
    }

    
    public function getSort(){
        return $this->sort;
    }

    public function setSort($value){
        $this->sort = $value;
    }
	
	/**
	 * Returns product title
	 *
	 * @return string
	 */
	public function getArtNum()
	{
		if ($this->_artnum  === null) {
			
			$oArticle = $this->getArticle();
			$this->_artnum = $oArticle->oxarticles__oxartnum->value;
		}
		
		return $this->_artnum;
	}
	
	public function getVE(){
		if ($this->_ve  === null) {
			
			$oArticle = $this->getArticle();
			$this->_ve = $oArticle->oxarticles__verpackungseinheit->value;
		}
		return $this->_ve;
	}
	
	public function getParentId(){
		$oArticle = $this->getArticle();
		if($oArticle->getParentId() !== ''){
			return $oArticle->getParentId();
		}
		return $oArticle->oxarticles__oxid->value;
	}

}
