<?php
require_once dirname(__FILE__).'/tcpdf/tcpdf.php';

class bn_pricelistpdf extends tcpdf
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        //
        $this->SetMargins(
            15.0,
            25.0,
            15.0,
            true
        );
        //
        $this->AddPage();
    }

    /**
     *
     */
    public function Header() {
        // Logo
        //$image_file = K_PATH_IMAGES.'logo_example.jpg';
        //$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font

        $this->SetTopMargin(10);


        $this->SetFont($this->getFontFamily(), 'B', 12, 'default', false);

        $this->Cell(120, 6, $_GET['name'].' - Stand: '.date('d.m.Y'), '', 0, 'L');
        $this->Ln();

        $this->Cell(120, 6, '', '', 0, 'L');
        $this->Ln();


        //
        $this->setCellPaddings(1,1,1,1);
        $this->writeBnCell('Name', 'Varname', 'Artnum:', 'UVP/€', 'EK/€', 'EAN');


        //$this->SetFont($this->getFontFamily(), '');
    }


    /**
     * @param $strName
     * @param $strArtnum
     * @param $strUvpPreis
     * @param $strEkPreis
     */
    public function writeBnCell($strName, $strVarName, $strArtnum, $strUvpPreis, $strEkPreis, $strEAN)
    {
	    $this->SetFont($this->getFontFamily(), '', 9, 'default', false);
	    
        $this->Cell(70, 6, $strName, 'TLRB', 0, 'L', false, '', 3);
	    $this->Cell(40, 6, $strVarName, 'TLRB', 0, 'L', false,'', 3);
        $this->Cell(15, 6, $strArtnum, 'TLRB', 0, 'L', false,'', 3);
        $this->Cell(12, 6, str_replace('.', ',', $strUvpPreis), 'TLRB', 0, 'R', false,'', 3);
        $this->Cell(12, 6, str_replace('.',',',$strEkPreis), 'TLRB', 0, 'R', false,'', 3);
	    $this->Cell(25, 6, $strEAN, 'TLRB', 0, 'R', false,'', 3);
        $this->Ln();
    }

    /**
     *
     */
    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);
        //
        $this->SetTopMargin(29);
    }

    /**
     * @param $strHtml
     */
    public function setHeaderHtml($strHtml)
    {
        $this->strHeaderHtml = $strHtml;
    }
}
