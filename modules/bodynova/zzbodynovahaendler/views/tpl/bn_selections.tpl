<script>/*$('.imagecolumn img').magnify();*/</script>
<form>
	<table class="tableselections table table-hover" border="0" style="margin-bottom: 0">
		<!--<thead>
			<th class="hidden-xs"></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</thead>-->
		[{foreach from=$arrSelections item=_aProduct}]
			<tr id="article_[{$_aProduct.oxid}]" class="tabellenspalte">
				<td class="hidden-xs imagecolumn">
					[{*$_aProduct|var_dump*}]
					<div class="pictureBoxVar">
						<a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$_aProduct.oxid}]')" class="viewAllHover glowShadow corners" title="[{$_aProduct.title}]">
							[{if $_aProduct.pic ne ''}]
								[{*$_aProduct.pic*}]
								<img class="lazy-img img-thumbnail" src="" data-src="https://bodynova.de/out/pictures/generated/product/1/450_450_100/[{$_aProduct.pic}]" alt="[{$_aProduct.title}]" >
							[{else}]
								<img class="lazy-img img-thumbnail" src="" data-src="https://bodynova.de/out/imagehandler.php?artnum=[{$_aProduct.artnum}]&size=450_450_100" alt="[{$_aProduct.title}]">
							[{/if}]
						</a>
					</div>
				</td>
				<td class="ebenetitel">
                    <!--<span class="headline"></span>-->
					<div class="pull-left input-line">
						<div class="amount_input spinner input-group bootstrap-touchspin">
							<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-down">-</button></span>
							<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
							<input class="form-control amount" type="numeric" name="amount[[{$_aProduct.oxid}]]" value="0" style="display: block;">
							<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-up">+</button></span>
						</div>
					</div>
					<div class="pull-left selection-text">
                        <strong>[{$_aProduct.artnum}]</strong>
                        &nbsp;[{$_aProduct.title}]

						[{if $_aProduct.lists}]
							<table>
								[{foreach from=$_aProduct.lists item=_list key=_fkey}]
								<tr>
									<td>[{$_list.name}]</td>
									<td>
										<select name="sel[[{$_aProduct.oxid}]][[{$_fkey}]]">
											[{foreach from=$_list item=_item key=_skey}]
												[{if $_item->name}]
													<option value="[{$_skey}]">[{$_item->name}]</option>
												[{/if}]
											[{/foreach}]
										</select>
									</td>
								</tr>
								[{/foreach}]
							</table>
						[{/if}]
					</div>
					<div class="pull-right" style="margin-top: 2px; margin-left: 5px">
						[{if $_aProduct.veEinheit ne '0' and $_aProduct.veEinheit > 1}]
							<span  style="margin:0">
								[{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$_aProduct.veEinheit}]<a onclick="addVeToInput('[{$_aProduct.oxid}]' , '[{$_aProduct.veEinheit}]')" style="margin-top:0" type="button" class="btn btn-default btn-prima btn-basket ladda-button bntooltip" data-placement="top" title="[{oxmultilang ident="TT-VE-ADD"}]"><span style="width: 5px !important;" class=""><strong>+</strong></span></a>
							</span>
						[{/if}]
						[{*$_aProduct.preisneu*}]

					</div>
					[{* Anzeige nur bei Admin *}][{* Preis Neu *}]
					[{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'*}]
						[{*if $_aProduct.preisneu > 0}]

							<div class="pull-right" style="margin-top: 2px; margin-left:5px;font-size: small">
									<span style="margin:0">
											[{oxmultilang ident="PREISNEU" suffix="COLON"}] <b>[{oxprice price=$_aProduct.preisneu currency=$oView->getActCurrency()}]</b>
									</span>
							</div>
						[{/if*}]
					[{*/if*}]
				</td>
				[{*}]
					<td class="uvptitle">UVP</td>
					<td class="uvpprice">
						[{oxprice price=$_aProduct.uvpprice currency=$oView->getActCurrency()}]
					</td>
				[{*}]
                <td class="Warenkorb"></td>
				<td class="selprice">
                    <label id="productPrice_[{$iIndex}]" class="price" style="margin:0">
					    [{oxprice price=$_aProduct.price currency=$oView->getActCurrency()}]
                    </label>
				</td>
                <td class="ampelcolumn">
					[{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
				</td>
			</tr>
		[{/foreach}]
		<tr class="tabellenspalte">
			<td class="hidden-xs imagecolumn"></td>
            <td class="ebenetitel"></td>
            <td class="Warenkorb">
                <div class="input-line pull-right">
                    <button onclick="addBulkCart('[{$oxid}]')" type="button" style="margin-top: 0" class="btn btn-default btn-prima btn-basket ladda-button btn-success pull-right"><span class="glyphicon glyphicon-shopping-cart"></span></button>
                </div>
            </td>
            <td class="selprice"></td>
			<td class="ampelcolumn"></td>
		</tr>
	</table>
</form>
<script>

	$(function() {
		/*
		$('.lazy-img').each(function(){
			if($(this).visible()){
				$(this).imageloader();
			}
		});
		/*
		$('.lazy-img').imageloader({
			each: function (elm) {
				console.log($(elm).visible());
			},
			callback: function (elm) {
				$(elm).fadeIn();
			}
		});
		*/
		//console.log($('.lazy-img').visible());
	});

</script>
