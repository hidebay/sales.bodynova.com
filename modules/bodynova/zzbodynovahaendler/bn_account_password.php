<?php
class bn_account_password extends bn_account_password_parent
{
    public function render()
    {
        //
        if($_GET['forcechangepassword']) {
            oxUtilsView::getInstance()->addErrorToDisplay( oxLang::getInstance()->translateString('PLEASE_CHANGE_PASSWORD'), false, true );
        }
        //
        return parent::render();
        // ende
    }
}