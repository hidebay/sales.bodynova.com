<?php
/**
 * Created by PhpStorm.
 * User: manuelgeil
 * Date: 06.10.14
 * Time: 14:09
 */

class bn_oxorder extends bn_oxorder_parent {
	/**
	 * Whitelabel Bestellungen.
	 * @var null
	 */
	protected $_isWhitelabel = null;
	protected $_orderDatei   = null;
	protected $_usedDiscountValue   = null;
	protected $_usedDiscountTitle   = null;
	
	/**
	 * Class constructor, initiates parent constructor (parent::oxBase()).
	 */
	public function __construct()
	{
		parent::__construct();
		$this->init('oxorder');
		
		// set usage of separate orders numbering for different shops
		//$this->setSeparateNumbering($this->getConfig()->getConfigParam('blSeparateNumbering'));
		
	}
	
	
	/**
	 * @param $oBasket
	 */
	public function validateDelivery( $oBasket )
	{
		return;
	}

	/**
	 * @param $oBasket
	 */
	public function validatePayment( $oBasket )
	{
		return;
	}


	public function setWhitelabel($wert)
	{
		$this->_isWhitelabel = $wert;
	}
	
	public function getWhitelabel(){
		return $this->_isWhitelabel;
	}
	
	public function setOrderDateiname($wert)
	{
		$this->_orderDatei = $wert;
	}
	
	public function getOrderDateiname(){
		return $this->_orderDatei;
	}
	
	public function getUsedDiscountValue(){
		return $this->_usedDiscountValue;
	}
	public function setUsedDiscountValue($wert){
		$this->_usedDiscountValue = $wert;
	}
	public function getUsedDiscountTitle(){
		return $this->_usedDiscountTitle;
	}
	public function setUsedDiscountTitle($wert){
		$this->_usedDiscountTitle = $wert;
	}
	
	/**
	 * Updates order transaction status. Faster than saving whole object
	 *
	 * @param string $sStatus order transaction status
	 *
	 * @return null
	 */
	protected function _setOrderStatus( $sStatus )
	{
		// execute Parent Actions
		parent::_setOrderStatus( $sStatus );
		//
		/*
		if($sStatus == 'OK') {
			$oDb = oxDb::getDb();
			$sQ = 'update oxorder set iswhitelabel=' . $oDb->quote($this->getWhitelabel()) . ' where oxid=' . $oDb->quote($this->getId());
			$oDb->execute($sQ);
		}
		*/
		// ende
	}
	
	
	
	
	public function finalizeOrder(oxBasket $oBasket, $oUser, $blRecalculatingOrder = false){
		
		// check if this order is already stored
		$sGetChallenge = oxRegistry::getSession()->getVariable('sess_challenge');
		if ($this->_checkOrderExist($sGetChallenge)) {
			oxRegistry::getUtils()->logger('BLOCKER');
			
			// we might use this later, this means that somebody klicked like mad on order button
			return self::ORDER_STATE_ORDEREXISTS;
		}
		
		// if not recalculating order, use sess_challenge id, else leave old order id
		if (!$blRecalculatingOrder) {
			// use this ID
			$this->setId($sGetChallenge);
			
			// validating various order/basket parameters before finalizing
			if ($iOrderState = $this->validateOrder($oBasket, $oUser)) {
				return $iOrderState;
			}
		}
		
		// copies user info
		$this->_setUser($oUser);
		
		// copies basket info
		$this->_loadFromBasket($oBasket);
		
		#$this->setWhitelabel($label);
		
		/*
		echo '<pre>';
		
		echo 'session:';
		print_r($_SESSION);
		
		/* schon leer
		echo 'files: ';
		print_r($_FILES);
		
		
		echo 'bn_oxorder: ';
		print_r($this);
		
		die();
		*/
		
		
		if(isset($_FILES['lieferschein']['tmp_name'])){
			
			move_uploaded_file(
				$_FILES['lieferschein']['tmp_name'],
				dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxid->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
			);
			
			// FTP Upload auf fritz.nas
			$ftp_server= '92.50.75.170';
			$ftp_user= 'haendlershop';
			$ftp_pass= 'peanuts30';
			$file = dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxid->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$destination_file = 'JetFlash-Transcend16GB-01/FRITZ/salespdf/'.$this->oxorder__oxid->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$cid=ftp_connect($ftp_server);
			ftp_login($cid, $ftp_user, $ftp_pass);
			ftp_pasv($cid, true);
			ftp_put($cid, $destination_file, $file, FTP_BINARY);
			ftp_close($cid);
			
			$name = $this->oxorder__oxid->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			
			$this->oxorder__konstante_lieferschein = new oxField($name);
			$this->oxorder__dateiname = new oxField($name);
			$this->save();
			
		}
		
		
		/*
		if($this->getOrderDateiname() != null){
			/*
			echo oxRegistry::getSession()->getVariable('uploaddir').'<br>';
			echo oxRegistry::getSession()->getVariable('upfile_saved').'<br>';
			echo oxRegistry::getSession()->getVariable('upfile').'<br>';
			echo oxRegistry::getSession()->getVariable('iswhitelabel').'<br>';
			
			die($this->getOrderDateiname());
			
			//old
			$basepath = oxRegistry::getSession()->getVariable('uploaddir');
			$file = oxRegistry::getSession()->getVariable('upfile_saved');
			$filepath = $basepath.$file;
			
			//new
			$name = $this->getOrderDateiname();
			
			// rename
			if(rename($filepath, dirname(__FILE__).'/lieferscheine/'.$name)){
				
				/*
				$this->setWhitelabel(1);
				$this->setOrderDateiname($name);
				$this->oxorder__dateiname = new oxField($name);
				
				$this->oxorder__konstante_lieferschein = new oxField($this->getOrderDateiname());
				
				// FTP Upload auf fritz.nas
				$ftp_server= '92.50.75.170';
				$ftp_user= 'haendlershop';
				$ftp_pass= 'peanuts30';
				$file = dirname(__FILE__).'/lieferscheine/'.$this->getOrderDateiname();
				//$destination_file = 'salespdf/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
				$destination_file = 'JetFlash-Transcend16GB-01/FRITZ/salespdf/'.$this->getOrderDateiname();
				$cid=ftp_connect($ftp_server);
				ftp_login($cid, $ftp_user, $ftp_pass);
				ftp_pasv($cid, true);
				ftp_put($cid, $destination_file, $file, FTP_BINARY);
				ftp_close($cid);
				
			}
			$this->save();
		}*/
		
		
		// payment information
		$oUserPayment = $this->_setPayment($oBasket->getPaymentId());
		
		// set folder information, if order is new
		// #M575 in recalculating order case folder must be the same as it was
		if (!$blRecalculatingOrder) {
			$this->_setFolder();
		}
		
		// marking as not finished
		$this->_setOrderStatus('NOT_FINISHED');
		
		//saving all order data to DB
		$this->save();
		
		// executing payment (on failure deletes order and returns error code)
		// in case when recalculating order, payment execution is skipped
		if (!$blRecalculatingOrder) {
			$blRet = $this->_executePayment($oBasket, $oUserPayment);
			if ($blRet !== true) {
				return $blRet;
			}
		}
		
		if (!$this->oxorder__oxordernr->value) {
			$this->_setNumber();
		} else {
			oxNew('oxCounter')->update($this->_getCounterIdent(), $this->oxorder__oxordernr->value);
		}
		
		// executing TS protection
		if (!$blRecalculatingOrder && $oBasket->getTsProductId()) {
			$blRet = $this->_executeTsProtection($oBasket);
			if ($blRet !== true) {
				return $blRet;
			}
		}
		
		// deleting remark info only when order is finished
		oxRegistry::getSession()->deleteVariable('uploaddir');
		oxRegistry::getSession()->deleteVariable('upfile_saved');
		oxRegistry::getSession()->deleteVariable('upfile');
		oxRegistry::getSession()->deleteVariable('iswhitelabel');
		
		oxRegistry::getSession()->deleteVariable('ordrem');
		oxRegistry::getSession()->deleteVariable('stsprotection');
		
		//#4005: Order creation time is not updated when order processing is complete
		if (!$blRecalculatingOrder) {
			$this->_updateOrderDate();
		}
		
		// updating order trans status (success status)
		$this->_setOrderStatus('OK');
		
		// store orderid
		$oBasket->setOrderId($this->getId());
		
		
		
		
		

		// updating wish lists
		$this->_updateWishlist($oBasket->getContents(), $oUser);
		
		// updating users notice list
		$this->_updateNoticeList($oBasket->getContents(), $oUser);
		
		// marking vouchers as used and sets them to $this->_aVoucherList (will be used in order email)
		// skipping this action in case of order recalculation
		if (!$blRecalculatingOrder) {
			$this->_markVouchers($oBasket, $oUser);
		}
		
		// send order by email to shop owner and current user
		// skipping this action in case of order recalculation
		if (!$blRecalculatingOrder) {
			$iRet = $this->_sendOrderByEmail($oUser, $oBasket, $oUserPayment);
		} else {
			$iRet = self::ORDER_STATE_OK;
		}
		
		
		
		return $iRet;
		
		
		
	}
	
	
	/**
	 * Gathers and assigns to new oxOrder object customer data, payment, delivery
	 * and shipping info, customer order remark, currency, voucher, language data.
	 * Additionally stores general discount and wrapping. Sets order status to "error"
	 * and creates oxOrderArticle objects and assigns to them basket articles.
	 *
	 * @param oxBasket $oBasket Shopping basket object
	 */
	protected function _loadFromBasket(oxBasket $oBasket)
	{
		
		//parent::_loadFromBasket($oBasket);
		
		
		$myConfig = $this->getConfig();
		
		// store IP Address - default must be FALSE as it is illegal to store
		if ($myConfig->getConfigParam('blStoreIPs') && $this->oxorder__oxip->value === null) {
			$this->oxorder__oxip = new oxField(oxRegistry::get("oxUtilsServer")->getRemoteAddress(), oxField::T_RAW);
		}
		
		//setting view mode
		$this->oxorder__oxisnettomode = new oxField($oBasket->isCalculationModeNetto());
		
		// copying main price info
		$this->oxorder__oxtotalnetsum = new oxField($oBasket->getNettoSum());
		$this->oxorder__oxtotalbrutsum = new oxField($oBasket->getBruttoSum());
		$this->oxorder__oxtotalordersum = new oxField($oBasket->getPrice()->getBruttoPrice(), oxField::T_RAW);
		
		// copying discounted VAT info
		$this->_resetVats();
		$iVatIndex = 1;
		foreach ($oBasket->getProductVats(false) as $iVat => $dPrice) {
			$this->{"oxorder__oxartvat$iVatIndex"} = new oxField($this->_convertVat($iVat), oxField::T_RAW);
			$this->{"oxorder__oxartvatprice$iVatIndex"} = new oxField($dPrice, oxField::T_RAW);
			$iVatIndex++;
		}
		
		// payment costs if available
		if (($oPaymentCost = $oBasket->getCosts('oxpayment'))) {
			$this->oxorder__oxpaycost = new oxField($oPaymentCost->getBruttoPrice(), oxField::T_RAW);
			$this->oxorder__oxpayvat = new oxField($oPaymentCost->getVAT(), oxField::T_RAW);
		}
		
		// delivery info
		if (($oDeliveryCost = $oBasket->getCosts('oxdelivery'))) {
			$this->oxorder__oxdelcost = new oxField($oDeliveryCost->getBruttoPrice(), oxField::T_RAW);
			//V #M382: Save VAT, not VAT value for delivery costs
			$this->oxorder__oxdelvat = new oxField($oDeliveryCost->getVAT(), oxField::T_RAW); //V #M382
			$this->oxorder__oxdeltype = new oxField($oBasket->getShippingId(), oxField::T_RAW);
		}
		
		// user remark
		if (!isset($this->oxorder__oxremark) || $this->oxorder__oxremark->value === null) {
			$this->oxorder__oxremark = new oxField(oxRegistry::getSession()->getVariable('ordrem'), oxField::T_RAW);
		}
		
		// currency
		$oCur = $myConfig->getActShopCurrencyObject();
		$this->oxorder__oxcurrency = new oxField($oCur->name);
		$this->oxorder__oxcurrate = new oxField($oCur->rate, oxField::T_RAW);
		
		// store voucher discount
		if (($oVoucherDiscount = $oBasket->getVoucherDiscount())) {
			$this->oxorder__oxvoucherdiscount = new oxField($oVoucherDiscount->getBruttoPrice(), oxField::T_RAW);
		}
		
		// general discount
		if ($this->_blReloadDiscount) {
			$dDiscount = 0;
			$aDiscounts = $oBasket->getDiscounts();
			if (count($aDiscounts) > 0) {
				foreach ($aDiscounts as $oDiscount) {
					$dDiscount += $oDiscount->dDiscount;
				}
			}
			$this->oxorder__oxdiscount = new oxField($dDiscount, oxField::T_RAW);
		}
		
		//order language
		$this->oxorder__oxlang = new oxField($this->getOrderLanguage());
		
		
		// initial status - 'ERROR'
		$this->oxorder__oxtransstatus = new oxField('ERROR', oxField::T_RAW);
		
		// copies basket product info ...
		$this->_setOrderArticles($oBasket->getContents());
		
		// copies wrapping info
		$this->_setWrapping($oBasket);
		
		// copies TS protection info
		$this->_setTsProtection($oBasket);
		
		
		
		/* oxRegistry::getSession()->getVariable('ordrem') */
		#$this->oxorder__iswhitelabel = new oxField(oxRegistry::getSession()->getVariable('iswhitelabel'), oxField::T_RAW);
		#$this->oxorder__dateiname = new oxField(oxRegistry::getSession()->getVariable('upfile'), oxField::T_RAW);
		#$this->setOrderDateiname(oxRegistry::getSession()->getVariable('upfile'));
		
	}
	
	
	/**
	 * Updates/inserts order object and related info to DB
	 *
	 * @return null
	 */
	public function save() {
		if (($blSave = parent::save())) {
			
		
		}
		
		return $blSave;
	}
	
	/**
	 * Checks if delivery address (billing or shipping) was not changed during checkout
	 * Throws exception if not available
	 *
	 * @param oxUser $oUser user object
	 *
	 * @return int
	 */
	/*
	public function validateDeliveryAddress($oUser)
	{
		$iState = parent::validateDeliveryAddress( $oUser );
		
		if(isset($_FILES['lieferschein']['tmp_name'])){
			
			$move = move_uploaded_file(
				$_FILES['lieferschein']['tmp_name'],
				//dirname(__FILE__).'/lieferscheine/'.$oOrder->getId().substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
				dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
			);
			
			echo $move;
			
			
			$this->oxorder__konstante_lieferschein = new oxField($this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.')));
			
			echo '<pre>';
			echo dirname(__FILE__);
			print_r($_FILES);
			print_r($this);
			die();
			
			/*$oDb = oxDb::getDb();
			$sQ = 'update oxorder set Konstante_Lieferschein=' . $oDb->quote($this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.')) ) . ' where oxid=' . $oDb->quote($this->getId());
			
			die($sQ);
			$oDb->execute($sQ);*/
			
			/*
			// FTP Upload auf fritz.nas
			$ftp_server= '92.50.75.170';
			$ftp_user= 'haendlershop';
			$ftp_pass= 'peanuts30';
			$file = dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			//$destination_file = 'salespdf/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$destination_file = 'JetFlash-Transcend16GB-01/FRITZ/salespdf/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$cid=ftp_connect($ftp_server);
			ftp_login($cid, $ftp_user, $ftp_pass);
			ftp_pasv($cid, true);
			ftp_put($cid, $destination_file, $file, FTP_BINARY);
			ftp_close($cid);
			
			
		}
		
		return $iState;
		/*
		
		$sDelAddressMD5 = $this->getConfig()->getRequestParameter('sDeliveryAddressMD5');
		
		$sDeliveryAddress = $oUser->getEncodedDeliveryAddress();
		
		 @var oxRequiredAddressFields $oRequiredAddressFields
		$oRequiredAddressFields = oxNew('oxRequiredAddressFields');
		
		/** @var oxRequiredFieldsValidator $oFieldsValidator *
		$oFieldsValidator = oxNew('oxRequiredFieldsValidator');
		$oFieldsValidator->setRequiredFields($oRequiredAddressFields->getBillingFields());
		$blFieldsValid = $oFieldsValidator->validateFields($oUser);
		
		/** @var oxAddress $oDeliveryAddress
		$oDeliveryAddress = $this->getDelAddressInfo();
		if ($blFieldsValid && $oDeliveryAddress) {
			$sDeliveryAddress .= $oDeliveryAddress->getEncodedDeliveryAddress();
			
			$oFieldsValidator->setRequiredFields($oRequiredAddressFields->getDeliveryFields());
			$blFieldsValid = $oFieldsValidator->validateFields($oDeliveryAddress);
		}
		
		$iState = 0;
		if ($sDelAddressMD5 != $sDeliveryAddress || !$blFieldsValid) {
			$iState = self::ORDER_STATE_INVALIDDElADDRESSCHANGED;
		}
		
		return $iState;
		
	}*/


	public function lieferschein()
	{
		if(isset($_FILES['lieferschein']['tmp_name'])){

			move_uploaded_file(
				$_FILES['lieferschein']['tmp_name'],
				//dirname(__FILE__).'/lieferscheine/'.$oOrder->getId().substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
				dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxid->value.'.pdf' //.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'))
			);
			
			$this->oxorder__konstante_lieferschein = new oxField($this->oxorder__oxid->value.'.pdf');  // .substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.')));
			
			
			/*$oDb = oxDb::getDb();
			$sQ = 'update oxorder set Konstante_Lieferschein=' . $oDb->quote($this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.')) ) . ' where oxid=' . $oDb->quote($this->getId());
			
			die($sQ);
			$oDb->execute($sQ);*/
			
			// FTP Upload auf fritz.nas
			$ftp_server= '92.50.75.170';
			$ftp_user= 'haendlershop';
			$ftp_pass= 'peanuts30';
			$file = dirname(__FILE__).'/lieferscheine/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			//$destination_file = 'salespdf/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$destination_file = 'JetFlash-Transcend16GB-01/FRITZ/salespdf/'.$this->oxorder__oxordernr->value.substr($_FILES['lieferschein']['name'], strrpos($_FILES['lieferschein']['name'], '.'));
			$cid=ftp_connect($ftp_server);
			ftp_login($cid, $ftp_user, $ftp_pass);
			ftp_pasv($cid, true);
			ftp_put($cid, $destination_file, $file, FTP_BINARY);
			ftp_close($cid);

		}
	}
	/*
	 // user remark
        if (!isset($this->oxorder__oxremark) || $this->oxorder__oxremark->value === null) {
            $this->oxorder__oxremark = new oxField(oxRegistry::getSession()->getVariable('ordrem'), oxField::T_RAW);
        }
	 */
	
}
