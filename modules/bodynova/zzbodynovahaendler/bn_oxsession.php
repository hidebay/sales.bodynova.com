<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 11.08.16
 * Time: 16:05
 */

class bn_oxsession extends bn_oxsession_parent{
	/**
	 * Save Basket to Session
	 * @param oxBasket $oBasket
	 * @return $this
	 */
	
	public function saveBasket(){
		
		return $this->setVariable($this->_getBasketName(), serialize($this->getBasket()));
		/*
		$this->setBasket($oBasket);
		$this->setVariable($this->_getBasketName(), serialize($oBasket));
		return $this;
		*/
	}
}
