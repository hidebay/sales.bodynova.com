<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 15.08.16
 * Time: 15:06
 */
class bn_oxuser extends bn_oxuser_parent{
	
    public $test = 'test';
    protected $arFavorites = array();
    protected $arTest2 = array();
    
    protected $iswhitelabel = null;

    /**
     * Class constructor, initiates parent constructor (parent::oxBase()).
     *
     *
     */
    public function __construct()
    {
        parent::__construct();


        if(!$this->hasUserFavorites($this->getId())){
            $this->arFavorites = $this->getUserFavorites($this->getId());
        }

        $this->init('oxuser');
    }
    
    public function getIsWhiteLabel(){
    	return $this->iswhitelabel;
    }
    public function setIsWhiteLabel($wert){
    	$this->iswhitelabel = $wert;
    }

    public function hasUserFavorites($sOXID = null){
        // check User OXID
        if (!$sOXID) {
            $sOXID = $this->getId();
        }
        // if not set Favorites, try to
        if (!count($this->arFavorites) > 0) {
            $this->arFavorites = $this->getUserFavorites($sOXID);
        }
        // return bool
        if(count($this->arFavorites) > 0){
            return true;
        } else {
            return false;
        }


    }

    public function getUserFavorites($sOXID = null){

        if (count($this->arFavorites) === 0) {
            if (!$sOXID) {
                $sOXID = $this->getId();
            }
            $oDb = oxDb::getDb();
            $ssql ='select * from favoriten where oxuserid = ' . oxDb::getDb()->quote($sOXID) . ' order by sort asc';
            $result = $oDb->getAll($ssql);

            if(count($result) === 0){
                $this->arFavorites = $result;
                //echo count($oDb->getAll($ssql));
                //echo 'empty array';
            } else {
                $this->arFavorites = $result;
            }
        }

        return $this->arFavorites;
    }



    /**
     * @param $arr
     */
    public function setUserFavorites($arr = null , $sOXID = null ){
        
        if (!$sOXID) {
            $sOXID = $this->getId();
        }
        $response = null;
        // alle User Favoriten löschen.
        if(count($this->getUserFavorites($sOXID)) > 0 ){

            $oDB = oxDb::getDb();
            $ssql = "delete from favoriten where oxuserid='".$sOXID."' ";
            $oDB->execute($ssql);

            if(count($arr) > 0){

                foreach($arr as $key){
                    $oDB = oxDb::getDb();
                    $ssql = "insert into favoriten ( oxid, oxactive, oxuserid, favid, favtype, sort , text) values ( '".$this->generateUId()."', '".$key[1]."', '".$key[2]."', '".$key[3]."', '".$key[4]."', '".$key[5]."', '".$key[6]."')";
                    $oDB->execute($ssql);
                }

            }
        } else {

            if(count($arr) > 0){

                foreach($arr as $value => $key){
                    $oDB = oxDb::getDb();
                    $ssql = "insert into favoriten ( oxid, oxactive, oxuserid, favid, favtype, sort , text) values ( '".$this->generateUId()."', '".$key[1]."', '".$key[2]."', '".$key[3]."', '".$key[4]."', '".$key[5]."', '".$key[6]."')";
                    $oDB->execute($ssql);
                }

            }
        }
        $response = $ssql;

        /*
        $oDB = oxDb::getDb();
        $ssql = "insert into favoriten ( oxid, oxactive, oxuserid, favid, favtype, sort) values ( '".$this->generateUId()."', '".$arr[1]."', '".$arr[2]."', '".$arr[3]."', '".$arr[4]."', '".$arr[5]."')";
        $oDB->execute($ssql);
        */

        return $response;
    }


    /**
     * Oxid UUID erstellen.
     * @return string
     */
    public function generateUId() {
        return substr(md5(uniqid('', true) . '|' . microtime()), 0, 32);
    }

    public function setStandardFavorites(){

    }

}
