<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eSales Visual CMS PE
 */
/**
 * Class defines what module does on Shop events.
 */
class ddoeVisualCmsEvents extends oxUBase
{

    /**
     * An array of SQL statements, that will be executed only at the first time of module installation.
     *
     * @var array
     */
    private static $_aSetupOxContentSQLs = array(
        "DDHIDETITLE"      => "ALTER TABLE  `oxcontents` ADD  `DDHIDETITLE` TINYINT( 1 ) UNSIGNED NOT NULL;",
        "DDHIDESIDEBAR"    => "ALTER TABLE  `oxcontents` ADD  `DDHIDESIDEBAR` TINYINT( 1 ) UNSIGNED NOT NULL;",

        "DDISBLOCK"        => "ALTER TABLE  `oxcontents`
           ADD  `DDISBLOCK` TINYINT( 1 ) UNSIGNED NOT NULL ,
           ADD  `DDBLOCK` VARCHAR( 250 ) NOT NULL ,
           ADD  `DDOBJECTTYPE` VARCHAR( 50 ) NOT NULL ,
           ADD  `DDOBJECTID` CHAR( 32 ) NOT NULL; ",

        "DDISLANDING"      => "ALTER TABLE  `oxcontents` ADD  `DDISLANDING` TINYINT( 1 ) UNSIGNED NOT NULL; ",

        "DDISTMPL"         => "ALTER TABLE  `oxcontents` ADD  `DDISTMPL` TINYINT( 1 ) UNSIGNED NOT NULL; ",
        "DDACTIVEFROM"     => "ALTER TABLE  `oxcontents` ADD  `DDACTIVEFROM` TIMESTAMP NOT NULL; ",
        "DDACTIVEUNTIL"    => "ALTER TABLE  `oxcontents` ADD  `DDACTIVEUNTIL` TIMESTAMP NOT NULL; ",
        "DDCSSCLASS"       => "ALTER TABLE  `oxcontents` ADD  `DDCSSCLASS` VARCHAR( 100 ) NOT NULL; ",

        "DDCUSTOMCSS"      => "ALTER TABLE  `oxcontents` ADD  `DDCUSTOMCSS` TEXT NOT NULL; ",

        "DDTMPLTARGETID"   => "ALTER TABLE  `oxcontents` ADD  `DDTMPLTARGETID` CHAR( 32 ) NOT NULL; ",
        "DDTMPLTARGETDATE" => "ALTER TABLE  `oxcontents` ADD  `DDTMPLTARGETDATE` TIMESTAMP NOT NULL; ",

    );

    /**
     * SQL statement, that will be executed only at the first time of module installation.
     *
     * @var array
     */
    private static $_sCreateDdMediaSql =
        "CREATE TABLE IF NOT EXISTS `ddmedia` (
           `OXID` char(32) NOT NULL,
           `DDFILENAME` varchar(255) NOT NULL,
           `DDFILESIZE` int(10) unsigned NOT NULL,
           `DDFILETYPE` varchar(50) NOT NULL,
           `DDTHUMB` varchar(255) NOT NULL,
           `OXTIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
           PRIMARY KEY (`OXID`)
         ) ENGINE=InnoDB; ";

    /**
     * An array of SQL statements, that will be executed only at the first time of module installation.
     *
     * @var array
     */
    private static $_aSetupDdMediaSQLs = array(
        "DDIMAGESIZE" => "ALTER TABLE  `ddmedia` ADD  `DDIMAGESIZE` VARCHAR( 100 ) AFTER  `DDTHUMB`; ",

        "OXSHOPID"    => "ALTER TABLE  `ddmedia` ADD `OXSHOPID` INT(10) UNSIGNED NOT NULL AFTER `OXID`; ",

    );

    /**
     * An array of SQL statements, that will be executed only at the update of module.
     *
     * @var array
     */
    private static $__aUpdateSQLs = array(


    );


    /**
     * An array of SQL statements, that will be executed on module activation.
     *
     * @var array
     */
    private static $__aActivateSQLs = array(
        // '',
    );


    /**
     * An array of SQL statements, that will be executed on module deactivation.
     *
     * @var array
     */
    private static $__aDeactivateSQLs = array(
        // '',
    );


    /**
     * Execute action on activate event
     */
    public static function onActivate()
    {
        self::setupModule();

        self::updateModule();

        self::activateModule();

        self::regenerateViews();

        self::clearCache();
    }


    private static function __copyOldMediaFiles()
    {
        $sOldMediaDir =  getShopBasePath() . 'out/pictures/ddvisualeditor/';

        if( is_dir( $sOldMediaDir ) )
        {
            $sNewMediaDir = getShopBasePath() . 'out/pictures/ddmedia/';

            if( !is_dir( $sNewMediaDir ) )
            {
                mkdir( $sNewMediaDir );
            }

            foreach( glob( $sOldMediaDir . '*' ) as $sFile )
            {
                if ( !is_dir( $sFile ) )
                {
                    @copy( $sFile, $sNewMediaDir . basename( $sFile ) );
                    @unlink( $sFile );
                }
            }

            $sOldEditorMediaDir = $sOldMediaDir . 'wysiwyg/';

            if( is_dir( $sOldEditorMediaDir ) )
            {
                foreach ( glob( $sOldEditorMediaDir . '*' ) as $sFile )
                {
                    if ( !is_dir( $sFile ) )
                    {
                        @copy( $sFile, $sNewMediaDir . basename( $sFile ) );
                        @unlink( $sFile );
                    }
                }

                rmdir( $sOldEditorMediaDir );
            }

            rmdir( $sOldMediaDir );
        }
    }


    /**
     * Execute action on deactivate event
     */
    public static function onDeactivate()
    {
        self::executeSQLs( self::$__aDeactivateSQLs );

        /** @var oxConfig $oConfig */
        $oConfig = oxNew( 'oxconfig' );

        // Cache leeren
        /** @var oxUtilsView $oUtilsView */
        $oUtilsView = oxRegistry::get( 'oxUtilsView' );
        $sSmartyDir = $oUtilsView->getSmartyDir();

        if( $sSmartyDir && is_readable( $sSmartyDir ) )
        {
            foreach( glob( $sSmartyDir . '*' ) as $sFile )
            {
                if ( !is_dir( $sFile ) )
                {
                    @unlink( $sFile );
                }
            }
        }

    }

    /**
     * Execute the sql at the first time of the module installation.
     */
    private static function setupModule()
    {
        // Check if oxcontents table has all needed fields, if not add them to the table.
        foreach (self::$_aSetupOxContentSQLs as $sField => $sSql) {
            if (!self::fieldExists($sField, 'oxcontents')) {
                self::executeSQL($sSql);
            }
        }
        // Check if ddmedia table was already created, if not create it.
        if (!self::tableExists('ddmedia')) {
            self::executeSQL(self::$_sCreateDdMediaSql);
        }
        // Check if ddmedia table has all needed fields, if not add them to the table.
        foreach (self::$_aSetupDdMediaSQLs as $sField => $sSql) {
            if (!self::fieldExists($sField, 'ddmedia')) {
                self::executeSQL($sSql);
            }
        }
        /** @var oxConfig $oConfig */
        $oConfig = oxNew( 'oxconfig' );
        $oConfig->saveShopConfVar( 'bool', 'blModuleWasEnabled', 'true', $oConfig->getShopId(), 'module:ddoevisualcms' );

    }

    /**
     * Activate module after installation.
     */
    private static function activateModule()
    {
        self::executeSQLs( self::$__aActivateSQLs );
    }

    /**
     * Updates module if it was already installed.
     */
    private static function updateModule()
    {
        /** @var oxConfig $oConfig */
        $oConfig = oxNew( 'oxconfig' );

        /** @var oxModule $oConfig */
        $oModule = oxNew( 'oxmodule' );
        $oModule->load( 'ddoevisualcms' );

        $sCurrentVersion   = $oModule->getInfo( 'version' );
        $sInstalledVersion = $oConfig->getShopConfVar( 'iInstallledVersion', $oConfig->getShopId(), 'module:ddoevisualcms' );

        if( !$sInstalledVersion || version_compare( $sInstalledVersion, $sCurrentVersion, '<' ) )
        {
            if( self::$__aUpdateSQLs )
            {
                foreach( self::$__aUpdateSQLs as $sUpdateVersion => $aSQLs )
                {
                    if( !$sInstalledVersion || version_compare( $sUpdateVersion, $sInstalledVersion, '>' ) )
                    {
                        self::executeSQLs( $aSQLs );

                        if( $sUpdateVersion == '1.1.0' )
                        {
                            self::__copyOldMediaFiles();
                        }
                    }
                }
            }

            $oConfig->saveShopConfVar( 'str', 'iInstallledVersion', $sCurrentVersion, $oConfig->getShopId(), 'module:ddoevisualcms' );
        }

    }

    /**
     * Regenerate views for changed tables
     */
    protected static function regenerateViews()
    {
        $oDbMetaDataHandler = oxNew('oxDbMetaDataHandler');
        $oDbMetaDataHandler->updateViews();
    }

    /**
     * Empty cache
     */
    private static function clearCache()
    {
        /** @var oxUtilsView $oUtilsView */
        $oUtilsView = oxRegistry::get( 'oxUtilsView' );
        $sSmartyDir = $oUtilsView->getSmartyDir();

        if( $sSmartyDir && is_readable( $sSmartyDir ) )
        {
            foreach( glob( $sSmartyDir . '*' ) as $sFile )
            {
                if ( !is_dir( $sFile ) )
                {
                    @unlink( $sFile );
                }
            }
        }

        // Initialise Smarty
        $oUtilsView->getSmarty( true );
    }

    /**
     * Check if table exists
     *
     * @param string $sTableName table name
     *
     * @return bool
     */
    protected static function tableExists($sTableName)
    {
        $oDbMetaDataHandler = oxNew('oxDbMetaDataHandler');
        return $oDbMetaDataHandler->tableExists($sTableName);
    }

    /**
     * Check if field exists in table
     *
     * @param string $sFieldName field name
     * @param string $sTableName table name
     *
     * @return bool
     */
    protected static function fieldExists($sFieldName, $sTableName)
    {
        $oDbMetaDataHandler = oxNew('oxDbMetaDataHandler');
        return $oDbMetaDataHandler->fieldExists($sFieldName, $sTableName);
    }

    /**
     * Executes given sql statements.
     *
     * @param $aSQLs array
     */
    private static function executeSQLs( $aSQLs )
    {
        if( count( $aSQLs ) > 0 )
        {
            foreach( $aSQLs as $sSQL )
            {
                self::executeSQL( $sSQL );
            }
        }
    }

    /**
     * Executes given sql statement.
     *
     * @param string $sSQL Sql to execute.
     */
    private static function executeSQL( $sSQL )
    {
        @oxDb::getDb()->execute( $sSQL );
    }
}