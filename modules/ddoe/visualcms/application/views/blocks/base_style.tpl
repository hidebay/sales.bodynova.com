[{$smarty.block.parent}]
[{assign var="oConf" value=$oView->getConfig()}]

[{oxstyle include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/css/font-awesome.min.css')}]

[{if !$oViewConf->isRoxiveTheme()}]
    [{if !$oConf->getConfigParam( 'blCustomGridFramework' ) && !$oConf->getConfigParam('blDisableBootstrap')}]
        [{oxstyle include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/css/bootstrap-custom.min.css')}]
    [{/if}]
[{/if}]

[{oxstyle include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/css/photoswipe.min.css')}]
[{oxstyle include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/css/style.min.css')}]