<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eSales Visual CMS PE
 */

/**
 * Metadata version
 */
$sMetadataVersion = '1.1';

/**
 * Module information
 */
$aModule = array(
    'id'          => 'ddoevisualcms',
    'title'       => 'Visual CMS',
    'description' => array(
        'de' => '<span>Einfache Verwaltung von CMS-Inhalten per Drag & Drop.</span>
                 <br>
                 <ul>
                    <li>Viele M&ouml;glichkeiten durch Verwendung von Widgets</li>
                    <li>Kinderleicht per Drag & Drop bedienbar</li>
                    <li>Schnelleres Arbeiten = gro&szlig;e Zeitersparnis</li>
                    <li>Flow Ready / RoxIVE Ready</li>
                    <li>CMS-Seiten Livesuche</li>
                    <li>Responsive Gridsystem</li>
                 </ul>
                 
                 ',
        'en' => '<span>Easy management of CMS content via drag and drop.</span>
                 <br>
                 <ul>
                    <li>Many scenarios possible through usage of widgets</li>
                    <li>Drag & Drop</li>
                    <li>Fast Results</li>
                    <li>Flow Ready / RoxIVE Ready</li>
                    <li>Live search of CMS pages</li>
                    <li>Responsive grid system</li>
                 </ul>

                 ',
    ),
    'thumbnail'   => 'logo.png',
    'version'     => '1.0.0',
    'author'      => 'OXID eSales AG & digidesk - media solutions',
    'url'         => 'http://www.oxid-esales.com',
    'email'       => 'info@oxid-esales.com',
    'extend'      => array(
        'oxviewconfig'  => 'ddoe/visualcms/core/ddoevisualcmsoxviewconfig',
        'oxutilsview'   => 'ddoe/visualcms/core/ddoevisualcmsoxutilsview',
        'oxtheme'       => 'ddoe/visualcms/core/ddoevisualcmsoxtheme',

        'oxcontent'     => 'ddoe/visualcms/application/models/ddoevisualcmsoxcontent',

        'content'       => 'ddoe/visualcms/application/controllers/ddoevisualcmscontent',
        'oxcmp_basket'  => 'ddoe/visualcms/application/components/ddoevisualcmscmpbasket',

        'oxutils'       => 'ddoe/visualcms/core/ddoevisualcmsoxutils',

        // Base Modul >>>

        'oxlang'            => 'ddoe/visualcms/core/ddoevisualcmsoxlang',

        // <<<

    ),
    'files' => array(
        // Admin Controller
        'ddoevisualcmsadmin'         => 'ddoe/visualcms/application/controllers/admin/ddoevisualcmsadmin.php',

        // Views
        'ddoevisualcmspreview'       => 'ddoe/visualcms/application/controllers/ddoevisualcmspreview.php',

        // Cronjob
        'ddoevisualcmscron'          => 'ddoe/visualcms/application/controllers/ddoevisualcmscron.php',

        // Events
        'ddoevisualcmsevents'        => 'ddoe/visualcms/application/events/ddoevisualcmsevents.php',

        // Models
        'ddvisualeditor'                 => 'ddoe/visualcms/application/models/ddvisualeditor.php',
        'ddvisualeditor_shortcode'       => 'ddoe/visualcms/application/models/ddvisualeditor_shortcode.php',
        'ddvisualeditor_block'           => 'ddoe/visualcms/application/models/ddvisualeditor_block.php',
        'ddvisualeditor_templates'       => 'ddoe/visualcms/application/models/ddvisualeditor_templates.php',
        
        // Base Module >>>
        
        'ddoevisualcmslangjs'      => 'ddoe/visualcms/application/controllers/ddoevisualcmslangjs.php',

        // Admin UI
        'ddoevisualcmsmedia'       => 'ddoe/visualcms/application/controllers/admin/ddoevisualcmsmedia.php',

        // Models
        'ddvisualeditor_media'         => 'ddoe/visualcms/application/models/ddvisualeditor_media.php',
        
        // <<<
    ),
    'templates' => array(
        // Admin Templates
        'ddoevisualcmsadmin.tpl'         => 'ddoe/visualcms/application/views/admin/tpl/ddoevisualcmsadmin.tpl',

        'ddoe_azure_content.tpl'               => 'ddoe/visualcms/application/views/azure/tpl/content.tpl',
        'ddoe_azure_content_plain.tpl'         => 'ddoe/visualcms/application/views/azure/tpl/content_plain.tpl',

        'ddoe_roxive_content.tpl'              => 'ddoe/visualcms/application/views/roxive/tpl/content.tpl',
        'ddoe_roxive_content_plain.tpl'        => 'ddoe/visualcms/application/views/roxive/tpl/content_plain.tpl',

        'ddoevisualcms_photoswipe.tpl'    => 'ddoe/visualcms/application/views/blocks/inc/photoswipe.tpl',

        // Widgets
        'widget/ve/article.tpl'              => 'ddoe/visualcms/application/views/tpl/widget/article.tpl',
        
        // Base Module >>>

        // Admin UI
        'ddoevisualcmsadmin_ui.tpl'      => 'ddoe/visualcms/application/views/admin/tpl/ddoevisualcmsadmin_ui.tpl',
        'dialog/ddoevisualcmsmedia.tpl'  => 'ddoe/visualcms/application/views/admin/tpl/dialog/ddoevisualcmsmedia.tpl',
        
        // <<<

    ),
    'blocks' => array(
        array( 'template' => 'layout/base.tpl', 'block' => 'base_style',  'file' => '/application/views/blocks/base_style.tpl' ),
        array( 'template' => 'layout/base.tpl', 'block' => 'base_js',     'file' => '/application/views/blocks/base_js.tpl' ),
    ),
    'events' => array(
        'onActivate'   => 'ddoevisualcmsevents::onActivate',
        'onDeactivate' => 'ddoevisualcmsevents::onDeactivate'
    ),
    'settings' => array(
        array( 'group' => 'frontend', 'name' => 'blCustomGridFramework',      'type' => 'bool', 'value' => 'false' ),
        array( 'group' => 'frontend', 'name' => 'sGridColPrefix',             'type' => 'str',  'value' => 'col-sm-' ),
        array( 'group' => 'frontend', 'name' => 'sGridOffsetPrefix',          'type' => 'str',  'value' => 'col-sm-offset-' ),
        array( 'group' => 'frontend', 'name' => 'sGridRow',                   'type' => 'str',  'value' => 'row' ),
        array( 'group' => 'frontend', 'name' => 'blGridWordNumbers',          'type' => 'bool', 'value' => 'false' ),
        array( 'group' => 'frontend', 'name' => 'iGridSize',                  'type' => 'str',  'value' => '12' ),
        array( 'group' => 'backend',  'name' => 'iGridsterSize',              'type' => 'str',  'value' => '6' ),
        array( 'group' => 'backend',  'name' => 'aPredefinedCssClasses',      'type' => 'arr',  'value' => '' ),
        array( 'group' => 'backend',  'name' => 'iDefaultWidgetSize',         'type' => 'str',  'value' => '1' ),
        array( 'group' => 'other',    'name' => 'blDisableJQuery',            'type' => 'bool', 'value' => 'false' ),
        array( 'group' => 'other',    'name' => 'blDisableBootstrap',         'type' => 'bool', 'value' => 'false' ),
        array( 'group' => 'other',    'name' => 'blVisualEditorDebug',        'type' => 'bool', 'value' => 'false' ),
        array( 'group' => 'other',    'name' => 'sVisualEditorCronKey',       'type' => 'str',  'value'  => '' ),

        //array( 'group' => 'backend',  'name' => 'blEnableVisualEditorBlocks', 'type' => 'bool', 'value' => 'false' ),
    )
);
