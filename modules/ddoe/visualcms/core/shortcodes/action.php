<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eSales Visual CMS PE
 */

class action_shortcode extends ddvisualeditor_shortcode
{

    protected $_sTitle = 'DD_VISUAL_EDITOR_SHORTCODE_ACTION';

    protected $_sBackgroundColor = '#648AA7';

    protected $_sIcon = 'fa-tags';

    public function install()
    {
        $this->setShortCode( basename( __FILE__, '.php' ) );

        $oLang = oxRegistry::getLang();


        if ( $this->getViewConfig()->isAzureTheme() )
        {
            $aColumns = array(
                4  => '4 '  . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE' ),
                8  => '8 '  . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE' ),
                12 => '12 ' . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE' )
            );
        }
        else
        {
            $aColumns = array(
                4  => '4 '  . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE' ),
                6  => '6 '  . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE' ),
                12 => '12 ' . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE' )
            );
        }

        $this->setOptions(
            array(
                'id' => array(
                    'data'        => 'searchAction',
                    'type'        => 'select',
                    'label'       => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ACTION' ),
                    'placeholder' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CHOOSE_ACTION' ),
                    'dataFields'  => array(
                        'name' => 'label'
                    )

                ),
                'title'      => array(
                    'type'    => 'text',
                    'label'   => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ACTION_NAME' )
                ),
                'shortdesc' => array(
                    'type'    => 'textarea',
                    'label'   => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ACTION_SHORTDESC' ),
                ),
                'count'     => array(
                    'type'   => 'select',
                    'label'  => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE_COUNT' ),
                    'values' => $aColumns,
                    'value'  => 6
                ),
                'name'      => array(
                    'type'    => 'hidden',
                    'preview' => true
                ),
            )
        );

    }


    public function parse( $sContent = '', $aParams = array() )
    {
        /** @var oxActions $oAction */
        /** @var oxArticleList $oArtList */
        $oAction      = oxNew( 'oxActions' );
        $oArtList     = oxNew( 'oxArticleList' );
        $blAzure      = $this->getViewConfig()->isAzureTheme();

        $sContent     = '';

        if( !$aParams[ 'count' ] )
        {
            $aParams[ 'count' ] = 6;
        }

        $oArtList->loadActionArticles( $aParams[ 'id' ], $aParams[ 'count' ] );

        if( $oAction->load( $aParams[ 'id' ] ) && $oArtList->count() > 0 )
        {
            $sColSize = 'col-sm-' . floor( 12 / $aParams[ 'count' ] );

            if( !$blAzure )
            {
                $sContent .= '<div class="row infogridView">';
                $sListType = 'infogrid';
            }
            else
            {
                $sContent .= '<ul class="gridView clear">';
                $sListType = 'grid';
            }


            if( $aParams[ 'title' ] )
            {
                $sContent .= '<div class="col-sm-12">
                                  <div class="page-header">
                                      <h2>' . $aParams[ 'title' ] . '</h2>';

                if( $aParams[ 'shortdesc' ] )
                {
                    $sContent .= '<small class="subhead">' . $aParams[ 'shortdesc' ] . '</small>';
                }

                $sContent .= '</div></div>';
            }


            /** @var oxArticle $oActionArticle */
            foreach ( $oArtList->getArray() as $oActionArticle )
            {
                if( !$blAzure )
                {
                    $sContent .= '<div class="productBox productData ' . $sColSize . ' listitem-type-' . $sListType . '">';
                }
                else
                {
                    $sContent .= '<li class="productData">';
                }

                $sContent .= '[{oxid_include_widget cl="oxwArticleBox" _parent=$oView->getClassName() _navurlparams=$oViewConf->getNavUrlParams() iLinkType="' . $oActionArticle->getLinkType() . '" anid="' . $oActionArticle->getId() . '" isVatIncluded=$oView->isVatIncluded() nocookie=1 sWidgetType=product sListType=listitem_' . $sListType . ' inlist=1 skipESIforUser=1}]';

                if( !$blAzure )
                {
                    $sContent .= '</div>';
                }
                else
                {
                    $sContent .= '</li>';
                }
            }

            if( !$blAzure )
            {
                $sContent .= '</div>';
            }
            else
            {
                $sContent .= '</ul>';
            }
        }

        return $sContent;
    }


    /**
     * Ermöglicht das Suchen von Aktionen
     */
    public function searchAction()
    {
        /** @var oxConfig $oConfig */
        $oConfig = $this->getConfig();

        $aActions = array();

        if( $oConfig->getRequestParameter( 'value' ) )
        {
            /** @var oxActions $oAction */
            $oAction = oxNew( 'oxActions' );
            $oAction->load( $oConfig->getRequestParameter( 'value' ) );

            $aActions[] = array(
                'value' => $oAction->getId(),
                'label' => $oAction->oxactions__oxtitle->value,
            );

        }
        elseif( $oConfig->getRequestParameter( 'search' ) )
        {
            $sSearch = $oConfig->getRequestParameter( 'search' );

            /** @var oxActionList $oList */
            $oList = oxNew( 'oxActionList' );

            $sSelect = "SELECT
                            *
                        FROM `oxactions`
                        WHERE ( `OXSHOPID` = '" . $oConfig->getShopId() . "' OR `OXSHOPID` = '' )
                          AND `OXTITLE` LIKE '%" . $sSearch . "%'
                        ";

            $oList->selectString( $sSelect );

            /** @var oxActions $oAction */
            foreach( $oList as $oAction )
            {
                $aActions[] = array(
                    'value' => $oAction->getId(),
                    'label' => $oAction->oxactions__oxtitle->value,
                );
            }
        }

        header( 'Content-Type: application/json' );
        oxRegistry::getUtils()->showJsonAndExit( $aActions );
    }
}