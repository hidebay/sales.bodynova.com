<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eSales Visual CMS PE
 */

class carousel_shortcode extends ddvisualeditor_shortcode
{

    protected $_sTitle = 'DD_VISUAL_EDITOR_SHORTCODE_CAROUSEL';

    protected $_sBackgroundColor = '#34495e';

    protected $_sIcon = 'fa-circle-o';

    protected $_blIsWidget = false; // temp. deaktiviert / feature für kommendes upgrade

    public function install()
    {
        $this->setShortCode( basename( __FILE__, '.php' ) );

        $oLang = oxRegistry::getLang();

        $this->setOptions(
            array(
                'id'               => array(
                    'type'        => 'select',
                    'label'       => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_TYPE' ),
                    'placeholder' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CHOOSE_CAROUSEL_TYPE' ),
                    'dataFields'  => array(
                        'name' => 'label'
                    ),
                    'values'      => array(
                        'brands' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_TYPE_BRANDS' ),
                    ),
                    'value'       => 'brands'

                ),
                'name'             => array(
                    'type'    => 'hidden',
                    'preview' => true
                ),
                /*'excl' => array(
                    'type'        => 'text',
                    'label'       => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_EXCLUDE' ),
                    'placeholder' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_EXCLUDE_INFO' ),
                ),
                'incl' => array(
                    'type'        => 'text',
                    'label'       => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_INCLUDE' ),
                    'placeholder' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_EXCLUDE_INFO' ),
                ),*/
                'interval'         => array(
                    'type'   => 'select',
                    'label'  => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_INTERVAL' ),
                    'values' => array(
                        2000  => 2  . ' ' . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_SECONDS' ),
                        4000  => 4  . ' ' . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_SECONDS' ),
                        6000  => 6  . ' ' . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_SECONDS' ),
                        8000  => 8  . ' ' . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_SECONDS' ),
                        10000 => 10 . ' ' . $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_SECONDS' ),
                    ),
                ),
                'countperslide'    => array(
                    'type'   => 'select',
                    'label'  => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_COUNT_PER_SLIDE' ),
                    'values' => array(
                        2 => 2,
                        4 => 4,
                        6 => 6
                    ),
                ),
                'count'            => array(
                    'type'  => 'text',
                    'label' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_COUNT' ),
                ),
                'background_color' => array(
                    'type'  => 'color',
                    'label' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_BG_COLOR' )
                ),
                'grayscale'        => array(
                    'type'  => 'checkbox',
                    'label' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CAROUSEL_IMG_GRAYSCALE' ),
                ),
                'fullwidth'        => array(
                    'type'  => 'checkbox',
                    'label' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_FULLWIDTH' )
                )
            )
        );

    }

    public function parse( $sContent = '', $aParams = array() )
    {
        return '<div class="dd-shortcode-' . $this->getShortCode() . ( $aParams[ 'class' ] ? ' ' . $aParams[ 'class' ] : '' ) . '">
                    [{include file="widget/manufacturersslider.tpl" countperslide="' . $aParams[ 'countperslide' ] . '" limit="' . (int)$aParams[ 'count' ] . '" interval="' . $aParams[ 'interval' ] . '" grayscale="' . (int)$aParams[ 'grayscale' ] . '" }]
                </div>';
    }

}