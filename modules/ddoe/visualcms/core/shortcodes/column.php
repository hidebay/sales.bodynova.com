<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eSales Visual CMS PE
 */

class column_shortcode extends ddvisualeditor_shortcode
{
    //protected $_blIsWidget = false;

    protected $_sBackgroundColor = '#bdc3c7';


    public function install()
    {
        $this->setShortCode( basename( __FILE__, '.php' ) );
    }


    public function parse( $sContent = '', $aParams = array() )
    {
        $sOutput  = '<div class="dd-shortcode-' . $this->getShortCode() . ' ' . ( $aParams[ 'class' ] ? ' ' . $aParams[ 'class' ] : '' ) . '">';
        $sOutput .= $this->getEditor()->parse( $sContent, false );
        $sOutput .= '</div>';

        return $sOutput;
    }
}