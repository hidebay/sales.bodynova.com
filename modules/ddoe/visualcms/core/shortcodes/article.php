<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eSales Visual CMS PE
 */

class article_shortcode extends ddvisualeditor_shortcode
{

    protected $_sTitle = 'DD_VISUAL_EDITOR_SHORTCODE_ARTICLE';

    protected $_sBackgroundColor = '#e74c3c';

    protected $_sIcon = 'fa-newspaper-o';

    public function install()
    {
        $this->setShortCode( basename( __FILE__, '.php' ) );

        $oLang = oxRegistry::getLang();

        $this->setOptions(
            array(
                'id'   => array(
                    'data'        => 'searchArticle',
                    'type'        => 'select',
                    'label'       => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_ARTICLE' ),
                    'placeholder' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CHOOSE_ARTICLE' ),
                    'dataFields'  => array(
                        'name' => 'label'
                    )

                ),
                'name' => array(
                    'type'    => 'hidden',
                    'preview' => true
                )
            )
        );

    }


    public function parse( $sContent = '', $aParams = array() )
    {
        $sOutput = '';
        $sType = $this->getListDisplayType();

        if( $this->getConfig()->getTopActiveView()->getClassName() == 'start' )
        {
            if( ( $sStartType = $this->getViewConfig()->getViewThemeParam('sStartPageListDisplayType') ) )
            {
                $sType = $sStartType;
            }
        }

        if( $sType == 'grid' && $this->getViewConfig()->isRoxiveTheme() )
        {
            $sType = 'infogrid';
        }

        if( class_exists( 'oxwArticleBox' ) )
        {
            $sOutput .= '<div class="dd-shortcode-' . $this->getShortCode() . ' productData productBox' . ( $aParams[ 'class' ] ? ' ' . $aParams[ 'class' ] : '' ) . '">';
            $sOutput .= '[{oxid_include_widget cl="oxwArticleBox" _parent=$oView->getClassName() _navurlparams=$oViewConf->getNavUrlParams() anid="' . $aParams[ 'id' ] . '" isVatIncluded=$oView->isVatIncluded() nocookie=1 sWidgetType=product sListType="listitem_' . $sType . '" inlist=1 skipESIforUser=1}]';
            $sOutput .= '</div>';
        }
        else
        {
            /** @var oxArticle $oArticle */
            $oArticle = oxNew( 'oxArticle' );
            $oArticle->load( $aParams[ 'id' ] );

            $oSmarty = oxRegistry::get( 'oxUtilsView' )->getSmarty();
            $oSmarty->assign(
                array(
                    'oView'     => $this->getConfig()->getTopActiveView(),
                    'product'   => $oArticle,
                    'type'      => $sType,
                    'css_class' => $aParams[ 'class' ],
                    'shortcode' => $this->getShortCode(),
                )
            );

            $sOutput .= $oSmarty->fetch( 'widget/ve/article.tpl' );
        }

        return $sOutput;

    }


    public function searchArticle()
    {
        /** @var oxConfig $oConfig */
        $oConfig = $this->getConfig();

        $aArticles = array();

        if( $oConfig->getRequestParameter( 'value' ) )
        {
            /** @var oxArticle $oProduct */
            $oProduct = oxNew( 'oxArticle' );
            $oProduct->load( $oConfig->getRequestParameter( 'value' ) );

            $aArticles[] = array(
                'value'       => $oProduct->getId(),
                'label'       => $oProduct->oxarticles__oxtitle->value,
                'description' => 'Art-Nr.: ' . $oProduct->oxarticles__oxartnum->value . ( $oProduct->oxarticles__oxean->value ? ' / EAN: ' . $oProduct->oxarticles__oxean->value : '' ),
                'icon'        => $oProduct->getIconUrl()
            );

        }
        elseif( $oConfig->getRequestParameter( 'search' ) )
        {
            $sSearch = $oConfig->getRequestParameter( 'search' );

            /** @var oxArticleList $oList */
            $oList = oxNew( 'oxArticleList' );
            $sViewName = getViewName( 'oxarticles' );

            $sSelect = "SELECT
                            *
                        FROM " . $sViewName . "
                        WHERE (
                            `OXARTNUM` LIKE '%" . $sSearch . "%' OR
                            `OXTITLE` LIKE '%" . $sSearch . "%' OR
                            `OXEAN` LIKE '" . $sSearch . "'
                          )
                          AND `OXPARENTID` = ''
                        ";

            $oList->selectString( $sSelect );

            /** @var oxArticle $oProduct */
            foreach( $oList as $oProduct )
            {
                $aArticles[] = array(
                    'value'       => $oProduct->getId(),
                    'label'       => $oProduct->oxarticles__oxtitle->value,
                    'description' => 'Art-Nr.: ' . $oProduct->oxarticles__oxartnum->value . ( $oProduct->oxarticles__oxean->value ? ' / EAN: ' . $oProduct->oxarticles__oxean->value : '' ),
                    'icon'        => $oProduct->getIconUrl()
                );
            }
        }

        header( 'Content-Type: application/json' );
        oxRegistry::getUtils()->showJsonAndExit( $aArticles );
    }

}