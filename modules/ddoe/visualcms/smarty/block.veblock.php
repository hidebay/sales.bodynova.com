<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 * @version   OXID eSales Visual CMS PE
 */

function smarty_block_veblock( $params, $content, &$smarty, &$repeat)
{
    $sName = isset( $params['name'] )? $params['name'] : null;

    if ( !$repeat )
    {
        if ( $sName )
        {
            static $aBlockCache = array();

            if ( isset( $aBlockCache[ $sName ] ) )
            {
                $content = $aBlockCache[ $sName ];
            }
            else
            {
                $oBlocks = oxRegistry::get( 'ddvisualeditor_block' );

                if ( ( $sBlock = $oBlocks->getBlock( $sName ) ) )
                {
                    $aBlockCache[ $sName ] = $sBlock;
                    $content = $sBlock;
                }
            }

            $oStr = getStr();
            $blHasSmarty = $oStr->strstr( $content, '[{' );

            if ( $blHasSmarty  )
            {
                $oConfig = oxRegistry::getConfig();
                $content = oxRegistry::get("oxUtilsView")->parseThroughSmarty( $content, $sName.md5($content), $oConfig->getActiveView(), true );
            }
        }

        return $content;
    }

}