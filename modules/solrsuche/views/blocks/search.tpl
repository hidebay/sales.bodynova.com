[{if $oView->showSearch()}]
    [{oxscript include=$oViewConf->getModuleUrl('solrsuche','src/initall.js') priority=10 }]
    [{oxscript add="var objQuickSearchTimoutHandler = null;"}]
    [{oxscript include="js/widgets/oxinnerlabel.js" priority=10 }]
    [{oxscript add="$( '#searchParam' ).oxInnerLabel();"}]
	<div class="center-block" style="text-align: center; padding-top: 10px">
		<div class="input-group center-block">
			<form class="navbar-form" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search" id="search">
				<div class="form-group form-group-suche" id="quicksearch">
					<div id="results" style="overflow: auto;"></div>
					[{$oViewConf->getHiddenSid()}]
					<input type="hidden" name="cl" value="search">
				    <input type="text" class="form-control" id="searchparam" name="searchparam" placeholder="[{oxmultilang ident="SEARCHTEXT"}]" autocomplete="off" onkeyup="autoSuggest();" value="">
				    <span class="input-group-btn" style="width: auto">
				        <button class="btn btn-default bntooltip" type="submit" onclick="$('#search').submit();" data-placement="bottom" title="[{oxmultilang ident="SEARCH"}]"><span class="glyphicon glyphicon-search"></span></button>
				    </span>
				</div>
			</form>
		</div>
	</div>

	[{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'}]

		[{oxscript add="var objSolrQuickSearchTimoutHandler = null;"}]
		[{oxscript include="js/widgets/oxinnerlabel.js" priority=10 }]
		[{oxscript add="$( '#solrsearchparam' ).oxInnerLabel();"}]
		<div class="center-block" style="text-align: center;">
			<div class="input-group center-block">
				<form class="navbar-form" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search" id="solrsearch">
					<div class="form-group form-group-suche" id="solrquicksearch">
						<div id="solrresults" style="overflow: auto;"></div>
						[{$oViewConf->getHiddenSid()}]
						<input type="hidden" name="cl" value="search">
						<input type="text" class="form-control" id="solrsearchparam" name="searchparam" placeholder="Neue Suche" autocomplete="off" onkeyup="solrSuggest();" value="">
						<span class="input-group-btn" style="width: auto">
					        <button class="btn btn-default bntooltip" type="submit" onclick="$('#solrsearch').submit();" data-placement="bottom" title="[{oxmultilang ident="SEARCH"}]"><span class="glyphicon glyphicon-search"></span></button>
					    </span>
					</div>
				</form>
			</div>
		</div>
	[{/if*}]

[{/if}]

