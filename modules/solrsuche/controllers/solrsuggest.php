<?php
if (isset ($_GET['q']) ) :
    if(strlen($_GET['q'])>1) :
        //
        require_once dirname(__FILE__).'/../../../bootstrap.php';

        //
        header("Content-Type: text/html; charset=utf-8");

        // searching ..
        $oSearchHandler = oxNew( 'oxsearch' );
	
	    $strSearchString = $oSearchHandler->getSelectStatement($_GET['q'], 'IF(a.oxparentid!=\'\', a.oxparentid, a.oxid) AS oxid');
	
	
	    $json_url = file_get_contents('http://bodhi-yoga.de:8983/solr/salesconnect/select?fq='.str_replace(' ' , '+', $_GET['q']).'~1&fq=oxactive:1&rows=30&indent=on&q=*:*&wt=json&fl=oxid,oxtitle,oxartnum,oxpic1');
	    
	    $aData = json_decode($json_url);
	    
	    echo 'Anzahl: '.$aData->response->numFound.'<br>';

		if(count($aData->response->docs)):
			echo '<table class="table table-striped" style="margin-bottom: 0; ">';
			foreach($aData->response->docs AS $row) :
				$picname = trim(utf8_encode($row->oxpic1));
				$seourl = 'javascript:jumpToArticle(\''.$row->oxid.'\',\''.$row->oxid.'\')';
				?>
					<tr class="resall" onclick="<?php echo $seourl; ?>" style="cursor: pointer;">
						<td style="vertical-align: baseline;cursor: pointer; padding:0;">
							<?php if($picname): ?>
								<img src="https://bodynova.de/out/pictures/generated/product/1/150_150_100/<?php echo $picname; ?>" width="50" height="50">
							<?php else : ?>
								<img src="https://bodynova.de/out/imagehandler.php?artnum=<?php echo $row->oxartnum[0];?>&size=150_150_100" width="50" height="50" class="img-thumbnail">
							<?php endif; ?>
						</td>
						
						<td style="vertical-align: baseline;cursor: pointer;">
							<?php echo $row->oxartnum[0]; ?>
						</td>
						
						<td style="vertical-align: baseline;cursor: pointer;">
							<?php echo $row->oxtitle[0]; ?>
						</td>
						
					</tr>
				
				<?php
				/*
				echo 'debug:';
				echo '<pre>';
				print_r($row);
				echo '</pre>';
				*/
	        endforeach;
			echo '</table>';
		else:
			echo 'Anzahl: '.$oSearchHandler->getSearchArticleCount($_GET['q']);
			//
			$arrNewWords = $oSearchHandler->getFaultCorrectedSearch($_GET['q']);
			//
			$strWords = '';
			foreach($arrNewWords AS $arrWord)
			{
				$strWords.=$arrWord['word'].' ';
			}
			$strLink = '<a href="index.php?lang='.oxRegistry::getLang()->getBaseLanguage().'&cl=search&searchparam='.urlencode(trim($strWords)).'">';
			
			//
			$strWords = '';
			foreach($arrNewWords AS $arrWord)
			{
				$strWords.= ($arrWord['changed'] ? '<b><i>' : '').$arrWord['word'].($arrWord['changed'] ? '</i></b>' : '').' ';
			}
			$strLink = $strLink.$strWords.'</a>';
			echo oxRegistry::getLang()->translateString('AUTOSUGGEST_DIDYOUMEAN', oxRegistry::getLang()->getBaseLanguage()).$strLink;
			
		endif;
		endif;
	    /*
	    echo 'debug:';
	    echo '<pre>';
	    print_r($aData->response->docs);
	    echo '</pre>';
	    */
	    //die();
	    /*
	    $strSearchString = $oSearchHandler->getSelectStatement($_GET['q'], 'IF(a.oxparentid!=\'\', a.oxparentid, a.oxid) AS oxid');

	    
	    
        # echo $strSearchString;
        // filter results for uniqe oxids
        $arrOXIDs = array();
        foreach(oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString) AS $arrRecord)
        {
            $arrOXIDs[] = $arrRecord['oxid'];
        }
        //
        $arrOXIDs = array_unique($arrOXIDs);

        //
        $strSearchString = 'SELECT '.$oSearchHandler->getAutosuggestSelectColumns().' FROM oxarticles AS a WHERE oxid IN (\''.implode('\',\'', $arrOXIDs).'\')';
        $aData = oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString, ' LIMIT 0, 10');//' LIMIT 0, 10'
		*/
        //
	    /*
        if(count($aData->response->docs)):
            echo '<table class="table table-striped" style="margin-bottom: 0; ">';
            foreach($aData AS $row) :
                $picname = trim(utf8_encode($row['OXPIC1']));
                $seourl = 'javascript:jumpToArticle(\''.$row['OXID'].'\',\''.$row['OXID'].'\')';
                ?>
                <tr class="resall" onclick="<?php echo $seourl; ?>" style="cursor: pointer;">
                    <td style="vertical-align: baseline;cursor: pointer; padding:0;">
                        <?php if($picname): ?>
                            <img src="https://bodynova.de/out/pictures/generated/product/1/150_150_100/<?php echo $picname; ?>" width="50" height="50">
                        <?php else : ?>
                            <img src="https://bodynova.de/out/imagehandler.php?artnum=<?php echo $row['oxartnum'];?>&size=150_150_100" width="50" height="50" class="img-thumbnail">
                        <?php endif; ?>
                    </td>
                    <td style="vertical-align: baseline;cursor: pointer;">
		                <?php echo $row['OXARTNUM']; ?>
                    </td>
                    <td style="vertical-align: baseline;cursor: pointer;">
	                    <?php echo $row['OXTITLE']; ?>
                    </td>
		            
                    <!--<td class="buy">
                        <button class="btn btn-default btn-prima btn-basket ladda-button btn-success pull-right" style="margin-top: 0" type="button" onclick="addBulkCart('<?php echo $row['OXID'];?>')">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </button>
                    </td>-->
                </tr>
                <?php
            endforeach;
	    
            echo '</table>';
        else:
            //
            $arrNewWords = $oSearchHandler->getFaultCorrectedSearch($_GET['q']);
            //
            $strWords = '';
            foreach($arrNewWords AS $arrWord)
            {
                $strWords.=$arrWord['word'].' ';
            }
            $strLink = '<a href="index.php?lang='.oxRegistry::getLang()->getBaseLanguage().'&cl=search&searchparam='.urlencode(trim($strWords)).'">';

            //
            $strWords = '';
            foreach($arrNewWords AS $arrWord)
            {
                $strWords.= ($arrWord['changed'] ? '<b><i>' : '').$arrWord['word'].($arrWord['changed'] ? '</i></b>' : '').' ';
            }
            $strLink = $strLink.$strWords.'</a>';
            echo oxRegistry::getLang()->translateString('AUTOSUGGEST_DIDYOUMEAN', oxRegistry::getLang()->getBaseLanguage()).$strLink;
        endif;
	    */
    //endif;
endif;
?>
