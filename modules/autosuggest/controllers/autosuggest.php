<?php
if (isset ($_GET['q']) ) :
    if(strlen($_GET['q'])>1) :
        //
        require_once dirname(__FILE__).'/../../../bootstrap.php';

        //
        header("Content-Type: text/html; charset=utf-8");

        // searching ..
        $oSearchHandler = oxNew( 'oxsearch' );
        $strSearchString = $oSearchHandler->getSelectStatement($_GET['q'], 'IF(a.oxparentid!=\'\', a.oxparentid, a.oxid) AS oxid');

        # echo $strSearchString;
        // filter results for uniqe oxids
        $arrOXIDs = array();
        foreach(oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString) AS $arrRecord)
        {
            $arrOXIDs[] = $arrRecord['oxid'];
        }
        //
        $arrOXIDs = array_unique($arrOXIDs);

        //
        $strSearchString = 'SELECT '.$oSearchHandler->getAutosuggestSelectColumns().' FROM oxarticles AS a WHERE oxid IN (\''.implode('\',\'', $arrOXIDs).'\')';
        $aData = oxDb::getDb( oxDb::FETCH_MODE_ASSOC )->getAll( $strSearchString);//, ' LIMIT 0, 10');//' LIMIT 0, 10'

        //
        if(count($aData)):
            echo '<table class="table table-striped" style="margin-bottom: 0; ">';
            foreach($aData AS $row) :
                $picname = trim(utf8_encode($row['OXPIC1']));
                $seourl = 'javascript:jumpToArticle(\''.$row['OXID'].'\',\''.$row['OXID'].'\')';
                ?>
                <tr class="resall" onclick="<?php echo $seourl; ?>" style="cursor: pointer;">
                    <td style="vertical-align: baseline;cursor: pointer; padding:0;">
	                    <?php
	                    if($picname){
		
	                    	$url = 'https://bodynova.de/out/pictures/generated/product/1/150_150_100/'.$picname;
		                    $ch = curl_init($url);
		                    curl_setopt($ch, CURLOPT_NOBODY, true); // set to HEAD request
		                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // don't output the response
		                    curl_exec($ch);
		                    $valid = curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200;
		                    curl_close($ch);
		                    
		                    $isExists = $valid;
		
		                    //print_r($isExists);
		                    if ($isExists !== true) {
			                    echo '<img src="https://bodynova.de/out/imagehandler.php?artnum='.$row['OXARTNUM'].'&size=150_150_100" width="50" height="50" class="img-thumbnail" />';
		                    } else {
			                    echo '<img src="'.$url.'" width="50" height="50" class="img-thumbnail" />';
		                    }
	                    } else {
	                    	echo '<img src="https://bodynova.de/out/imagehandler.php?artnum='.$row['OXARTNUM'].'&size=150_150_100" width="50" height="50" class="img-thumbnail" />';
	                    }
	                    ?>
                    </td>
                    <td style="vertical-align: baseline;cursor: pointer;">
		                <?php echo $row['OXARTNUM']; ?>
                    </td>
                    <td style="vertical-align: baseline;cursor: pointer;">
	                    <?php echo $row['OXTITLE']; ?>
                    </td>
		            
                    <!--<td class="buy">
                        <button class="btn btn-default btn-prima btn-basket ladda-button btn-success pull-right" style="margin-top: 0" type="button" onclick="addBulkCart('<?php echo $row['OXID'];?>')">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </button>
                    </td>-->
                </tr>
                <?php
            endforeach;
            echo '</table>';
        else:
            //
            $arrNewWords = $oSearchHandler->getFaultCorrectedSearch($_GET['q']);
            //
            $strWords = '';
            foreach($arrNewWords AS $arrWord)
            {
                $strWords.=$arrWord['word'].' ';
            }
            $strLink = '<a href="index.php?lang='.oxRegistry::getLang()->getBaseLanguage().'&cl=search&searchparam='.urlencode(trim($strWords)).'">';

            //
            $strWords = '';
            foreach($arrNewWords AS $arrWord)
            {
                $strWords.= ($arrWord['changed'] ? '<b><i>' : '').$arrWord['word'].($arrWord['changed'] ? '</i></b>' : '').' ';
            }
            $strLink = $strLink.$strWords.'</a>';
            echo oxRegistry::getLang()->translateString('AUTOSUGGEST_DIDYOUMEAN', oxRegistry::getLang()->getBaseLanguage()).$strLink;
        endif;
    endif;
endif;
?>
