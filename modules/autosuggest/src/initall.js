$(document).ready(function(){

	$('#searchparam').on('focus', function(){
		$(this).val('');
	});
	
	$('body').click(function() {
		$('#results').slideUp();
	});
});

var debug = false;
var count = 0;
var startSuggest = 2;
var requestTimes = 0;
var timeout = null;
var ajaxReq = 'cancel';
var keyups = 0;
var presend = 0;
var success = 0;
var error = 0;
var first = 0;


var globalTimeout = null;


function dresearch(obj){
	if(globalTimeout != null) clearTimeout(globalTimeout);
	globalTimeout = setTimeout(dresearch2(obj), 400);
}


function dresearch2(obj) {
	
	globalTimeout = null;
	
	var requests = ++requestTimes;
	var keyup = ++keyups;
	
	var keyword = obj.value;
	 // you can have it's value anything you like
	
	if(keyword.length == 0) {
		if(debug)
			console.log('query leer');
		clearResults();
		first++;
		return;
	} //You can always alter this condition to a better one that suits you.
	
	if(keyword.length <= startSuggest){
		if(debug)
			console.log('query zu kurz');
		clearResults();
		if(presend > 0){
			ajaxReq.abort();
			first++;
		}
		return;
	}
	
	ajaxReq = $.ajax({
		url: '/modules/autosuggest/controllers/autosuggest.php?q=' + keyword, //Don't forget to replace with your own post URL
		cache: false,
		async: true,
		dataType: "html",
		type: "get",
		//data: {'query': queryString},
		beforeSend : function() {
			if(ajaxReq != 'cancel' && ajaxReq.readyState < 4) {
				var beforesend = ++presend;
				if(debug)
					console.log('['+beforesend+']' + 'before send abort bei ' + keyword);
				ajaxReq.abort();
			}
		},
		success: function(data) {
			var erfolg = ++success;
			setResults(data);
			if(debug)
				console.log('['+erfolg+'] erfolgreich bei ' + keyword );
		},
		error: function(xhr, ajaxOptions, thrownError) {
			var ko = ++error;
			if(debug)
				console.log('['+ko+'] error ' + thrownError + ' bei ' + keyword);
			if(thrownError == 'abort' || thrownError == 'undefined') return;
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	}); //end ajaxReq
}

// show results
function setResults(data){
	if(data) {
		$("#results").html(" ");
		$("#results").html(data);
		$("#results").show();
	}
}
// clear results
function clearResults(){
	$("#results").html(" ");
	$('#results').hide();
}
