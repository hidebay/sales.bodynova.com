$(document).ready(function(){
	
	$('#searchparam').on('focus', function(){
		$(this).val('');
	});
	
	$('body').click(function() {
		$('#results').slideUp();
	});
});

var count = 0;
var startSuggest = 2;
var requestTimes = 0;
var timeout = null;




var ajaxReq = 'cancel';

var keyups = 0;
var presend = 0;
var success = 0;
var error = 0;
var dresearch = function (obj) {
	
	var requests = ++requestTimes;
	var keyup = ++keyups;
	
	var keyword = obj.value;
	// you can have it's value anything you like
	
	if(keyword.length == 0) {
		console.log('query leer');
		clearResults();
		return;
	} //You can always alter this condition to a better one that suits you.
	
	if(keyword.length <= startSuggest){
		console.log('query zu kurz');
		clearResults();
		if(presend > 0){
			ajaxReq.abort();
		}
		return;
	}
	
	ajaxReq = $.ajax({
		url: '/modules/autosuggest/controllers/autosuggest.php?q=' + keyword, //Don't forget to replace with your own post URL
		cache: false,
		async: true,
		dataType: "html",
		type: "get",
		//data: {'query': queryString},
		beforeSend : function() {
			if(ajaxReq != 'cancel' && ajaxReq.readyState < 4) {
				var beforesend = ++presend;
				console.log('['+beforesend+']' + 'before send abort bei ' + keyword);
				ajaxReq.abort();
			}
		},
		success: function(data) {
			var erfolg = ++success;
			setResults(data);
			console.log('['+erfolg+'] erfolgreich bei ' + keyword );
		},
		error: function(xhr, ajaxOptions, thrownError) {
			var ko = ++error;
			console.log('['+ko+'] error ' + thrownError + ' bei ' + keyword);
			if(thrownError == 'abort' || thrownError == 'undefined') return;
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	}); //end ajaxReq
};

// show results
function setResults(data){
	if(data) {
		//$('.navbar-form .searchBox').addClass('resultspresent');
		
		$("#results").html(" ");
		$("#results").html(data);
		$("#results").show();
	}
}
// clear results
function clearResults(){
	$("#results").html(" ");
	$('#results').hide();
}

/*
 
 * ab hier wird nix mehr gebraucht:
 
 */

/*
 var alterQueuer = new jQueue(searchrequest, 'Last', 10);
 
 var a = $.manageAjax.create('queue', {queue: true});
 
 var dresearch2 = function (obj) {
 
 
 var ajaxReq = obj.value;
 var searchkey = obj.value;
 $('#results').hide();
 
 if(searchkey.length <= 1){
 $('#results').hide();
 }
 if(searchkey.length <= startSuggest){
 console.log('Keyword zu kurz: ' + searchkey);
 $('#results').html('');
 $('#results').hide();
 return;
 }
 
 
 ajaxmanager.abort('search');
 
 ajaxmanager.clear('search');
 
 ajaxmanager.add('search',{
 success: function(data) {
 
 setResults(data);
 console.log('['+requests+'] erfolgreich bei ' + keyword );
 
 },
 url: '/modules/autosuggest/controllers/autosuggest.php?q=' + keyword,
 cache: false,
 async: true,
 dataType: "html",
 type: "get"
 });
 
 
 alterQueuer.run(searchkey).done(function () {
 console.log('done');
 }).expired(function() {
 console.log('Discards callback for input: ' + searchkey);
 });
 
 };
 
 
 
 var searchrequest = function(keyword) {
 
 var requests = ++requestTimes;
 if(requests == 1){
 console.log(requestTimes + 'quenque initialisiert: ' + keyword);
 return;
 }
 clearTimeout(timeout);
 timeout = setTimeout(function() {
 $.ajax({
 url: '/modules/autosuggest/controllers/autosuggest.php?q=' + keyword,
 cache: false,
 async: true,
 dataType: "html",
 type: "get",
 success: function (data) {
 setResults(data);
 console.log('['+requests+'] erfolgreich bei ' + keyword );
 }
 });
 }, 200);
 };
 
 var q = new jQueue(function (query){
 var input = query; // jQuery.trim($('#searchparam').val());
 //
 //$('#header .searchBox').addClass('resultspresent');
 $('.navbar-form .searchBox').addClass('resultspresent');
 //
 //console.log(q);
 //console.log(startSuggest);
 
 if (input.length >= startSuggest) {
 // neu: $("#results").hide();
 $.ajax({
 url: '/modules/autosuggest/controllers/autosuggest.php?q=' + input,
 cache: false,
 async: false,
 dataType: "html",
 type: "get",
 success: function(data) {
 if(data) {
 $("#results").html(" ");
 $("#results").html(data);
 $("#results").show();
 }
 }
 });
 } else {
 $('#results').hide();
 }
 }, 'Last', 2000);
 // start after user input >= 2 chars
 //var startSuggest = 3;
 
 Init basic events and effects
 function initEvents()
 {
 initAutoSuggest();
 }
 
 
 
 function initAutoSuggest()
 {
 
 $("#searchparam").bind("keyup", function() {
 var query = jQuery.trim($('#searchparam').val());
 console.log(query);
 console.log(query.length);
 if (query.length >= startSuggest) {
 q.run(query);
 console.log(q);
 }
 });
 
 
 $('#searchparam').bind('keyup', function() {
 autoSuggest();
 });
 
 
 $('#searchparam').focus(function() {
 //neu: $('#header .searchBox #results').show();
 $('#searchparam').val('');
 });
 
 //$('#searchparam').blur(function() { $('#searchparam').val("Suchbegriff eingeben"); } );
 
 $('body').click(function() {
 $('#results').slideUp();
 //neu: $('#header .searchBox #results').hide();
 });
 }
 
 function bring (selecter)
 {
 $('div.shopp:eq(' + selecter + ')').stop().animate(
 {
 opacity: '1.0',
 height: '60px'
 
 }, 300, function() {
 
 if(selecter < 4)
 {
 clearTimeout(Timer);
 }
 }
 );
 
 selecter++;
 var Func = function() {bring(selecter);};
 Timer = setTimeout(Func, 200);
 }
 
 (function($) {
 
 $.fn.bindWithDelay = function( type, data, fn, timeout, throttle ) {
 
 if ( $.isFunction( data ) ) {
 throttle = timeout;
 timeout = fn;
 fn = data;
 data = undefined;
 }
 
 // Allow delayed function to be removed with fn in unbind function
 fn.guid = fn.guid || ($.guid && $.guid++);
 
 // Bind each separately so that each element has its own delay
 return this.each(function() {
 
 var wait = null;
 
 function cb() {
 var e = $.extend(true, { }, arguments[0]);
 var ctx = this;
 var throttler = function() {
 wait = null;
 fn.apply(ctx, [e]);
 };
 
 if (!throttle) { clearTimeout(wait); wait = null; }
 if (!wait) { wait = setTimeout(throttler, timeout); }
 }
 
 cb.guid = fn.guid;
 
 $(this).bind(type, data, cb);
 });
 };
 
 })(jQuery);
 (function($) {
 
 $.fn.onDelay = function(events, selector, data, handler, timeout, throttle) {
 
 // (evt, handler, timeout)
 if ($.isFunction(selector)) {
 throttle = handler;
 timeout = data;
 handler = selector;
 data = undefined;
 selector = undefined;
 }
 // (evt, selector, handler, timeout) OR (evt, data, handler, timeout)
 else if ($.isFunction(data)) {
 throttle = timeout;
 timeout = handler;
 handler = data;
 data = undefined;
 
 // (evt, data, handler, timeout)
 if ( typeof selector !== "string" ) {
 data = selector;
 selector = undefined;
 }
 }
 
 // Allow delayed function to be removed with handler in unbind function
 handler.guid = handler.guid || ($.guid && $.guid++);
 
 // Bind each separately so that each element has its own delay
 return this.each(function() {
 var wait = null;
 
 function callback() {
 var event = $.extend(true, { }, arguments[0]);
 var that = this;
 var throttler = function() {
 wait = null;
 handler.apply(that, [event]);
 };
 
 if (!throttle) { clearTimeout(wait); wait = null; }
 if (!wait) { wait = setTimeout(throttler, timeout); }
 }
 
 callback.guid = handler.guid;
 $(this).on(events, selector, data, callback);
 });
 };
 
 })(jQuery);
 var jQueue = function(func, type, period) {
 var self = this;
 var inited = false;
 var queues = [];
 var queueIdx = 0;
 var lastParams = null;
 var queueMaxIdx = 9999999999;
 var queuer = null;
 
 var exec = (typeof func === 'function') ? func : function() {};
 var mode = type == 'Last' || type == 'Block' || type == 'Flush' || type == 'Append' || type == 'Loop' || (typeof type === 'number' && type % 1 == 0) ? type : 'Append';
 var interval = period ? parseInt(period, 10) : 300;
 
 var start = function() {
 if (queuer !== null) {
 return true;
 }
 
 queuer = setInterval(function() {
 if (queues.length <= 0) {
 /* Don't clear if last param still waiting
if (lastParams !== null && !lastParams.ICC()) {
	return;
}

expireLastParams();

clearInterval(queuer);
queuer = null;

return;
}

var params = queues.shift();

if (params.E) {
	return;
}

var result = exec.apply(self, params.P);

params.Cb.R(result);

if (result) {
	params.Cb.D(result);
} else {
	params.Cb.F(result);
}

if (params.L != 'Forever' && --params.L <= 0) {
	return;
}

queues.push(params);
}, interval);

return true;
};

var Resultr = function(ID) {
	var self = this;
	var paramID = ID;
	var found = false;
	
	var getElFromQueue = function() {
		for (var p in queues) {
			if (queues[p].ID != paramID) {
				continue;
			}
			
			return p;
		}
		
		return -1
	};
	
	var hookCallback = function(type, callback) {
		if (typeof callback !== 'function') {
			return false;
		}
		
		if ((p = getElFromQueue()) == -1) {
			return false;
		}
		
		queues[p].Cb[type] = callback;
		
		return true;
	};
	
	self.result = function(callback) {
		hookCallback('R', callback);
		
		return self;
	};
	
	self.done = function(callback) {
		hookCallback('D', callback);
		
		return self;
	};
	
	self.fail = function(callback) {
		hookCallback('F', callback);
		
		return self;
	};
	
	self.callback = function(callback) {
		hookCallback('C', callback);
		
		return self;
	};
	
	self.expired = function(callback) {
		hookCallback('E', callback);
		
		return self;
	};
	
	return true;
};

var Callbacker = function(callback) {
	var self = this;
	var jCb = ((typeof callback !== 'function') ? function() {
		return callback;
	} : callback);
	
	self.export = function() {
		return jCb;
	};
};

var alterCallback = function(paramID, params, callback, acbFinished) {
	return function() {
		/* Make this callback as called *
		params.IC[paramID] = true;
		
		if (params.E) {
			return;
		}
		
		var cbResult = callback.apply(self, arguments);
		
		acbFinished(cbResult);
	};
};

var expireLastParams = function() {
	if (lastParams === null) {
		return;
	}
	
	if (!lastParams.E) {
		lastParams.E = true;
		lastParams.Cb.E();
	}
	
	lastParams = null;
};

self.reset = function() {
	expireLastParams();
	
	if (queuer !== null) {
		clearInterval(queuer);
		queuer = null;
	}
	
	queues = [];
};

self.cb =
	self.callback = function(callback) {
		return new Callbacker(callback);
	};

self.resume = function() {
	if (queuer !== null) {
		return true;
	}
	
	return start();
};

self.pause = function() {
	if (queuer === null) {
		return true;
	}
	
	clearInterval(queuer);
	queuer = null;
	
	return true;
};

self.run = function() {
	var reslt = null;
	var altered = false;
	var flushAddCheck = function() { return true; };
	var params = {
		ID: (queueIdx++) % queueMaxIdx,     /* Parameter ID *
		E: false,                           /* Expired *
		P: arguments,                       /* Arguments *
		IC: {},                             /* Indexes of callbacks in param *
		ICC: function() {                   /* Check if there is any callback in param is not called *
			for (var i in this.IC) {
				if (!this.IC[i]) {
					return false;
				}
			}
			
			return true;
		},
		L: 1,                               /* Loop Counts *
		Cb: {                               /* Callbacks *
			D: function() {},                   /* Done *
			F: function() {},                   /* Failed *
			R: function() {},                   /* Result *
			C: function() {},                   /* Callback called *
			E: function() {}                    /* Expired *
		}
	};
	
	switch(mode) {
		case 'Block':
			flushAddCheck = function() {
				if (!altered) {
					if (queuer === null) {
						return true;
					}
				} else if (lastParams !== null) {
					return false;
				}
				
				return true;
			};
		
		case 'Last':
			for (var arg in params.P) {
				if (typeof params.P[arg] !== 'object') {
					continue;
				}
				
				if (!(params.P[arg] instanceof Callbacker)) {
					continue;
				}
				
				params.P[arg] = alterCallback(
					arg,
					params,
					params.P[arg].export(),
					function(callbackResult) {
						params.Cb.C(arg, callbackResult);
					}
				);
				
				/* Make current callback as not called *
				params.IC[arg] = false;
				
				altered = true;
			}
		
		case 'Flush':
			if (!flushAddCheck()) {
				return new Resultr(-1);
			}
			
			if (queuer !== null) {
				clearInterval(queuer);
				queuer = null;
			}
			
			queues = [];
			break;
		
		case 'Append':
			break;
		
		case 'Loop':
			params.L = 'Forever';
			break;
		
		default:
			if (typeof mode === 'number' && mode % 1 == 0) {
				params.L = parseInt(mode, 10);
			}
			break;
	}
	
	if (altered) {
		expireLastParams();
	}
	
	lastParams = params;
	
	queues.push(params);
	
	start();
	
	return new Resultr(params.ID);
};

return true;
};



/**
 * Created by andre on 17.04.18.
 */
//

// bodynova.js

/**
 *  execution functions on document ready
 *
$(document).ready(function() {
	// Bind Scroll shrink/unshrink class binding on scrolling
	imageloading();
	
	$(window).scroll(function() {
		setTimeout( function() {
			if ( (window.pageYOffset || document.scrollTop) >= 150 ) {
				//$('body').removeClass('unshrinked' );
				$('body').addClass('shrinked' );
				//$('.logo').removeClass('hidden-xs, hidden-sm ,hidden-md, hidden-lg');
				/*$('.logo').addClass('hidden-md hidden-lg');
				 $('.suche').addClass('hidden-md hidden-lg');
				 $('.rest').addClass('hidden-md hidden-lg');*
			} else {
				//$('body').removeClass('shrinked' );
				$('body').addClass('unshrinked' );
				/*$('.logo').removeClass('hidden-md hidden-lg');
				 $('.suche').removeClass('hidden-md hidden-lg');
				 $('.rest').removeClass('hidden-md hidden-lg');*
			}
		}, 250 );
		
		imageloading();
		
	});
	// trigger scroll for first time execution
	$(window).scroll();
	//
	/*
	 $('[data-toggle="tooltip"]').tooltip();
	 // close tooltips on click otherwise you cause a tooltip inside the popovers
	 $('[data-toggle="tooltip"]').click(function() {
	 $(this).tooltip('hide');
	 });
	 *
	//
	$('#header .suche button').click(function(){
		if ( $('body').hasClass('shrinked')) {
			//
			//orig:
			/*
			 $('#header .searchBox #searchparam').toggle();
			 if($('#header .searchBox #searchparam').css('display')=='block') {
			 $('#header .searchBox #results').show();
			 } else {
			 $('#header .searchBox #results').hide();
			 }
			 if($('#header .searchBox #searchparam').css('display')=='block') {
			 $('#header .searchBox #results').show();
			 } else {
			 $('#header .searchBox #results').hide();
			 }
			 *
			// neu test:
			$('#header .suche #searchparam').toggle();
			$('#header .suche #results').hide();
			// ende
		}
		return false;
	});
	// end
});

/**
 * Fixes Oxid to match css-id criteria
 *
 * @param oxid
 * @returns {XML|string|void}
 *
function fixOxId(oxid) {
	return oxid.replace(/\./g, '_');
}


/**
 * Bind spinner
 *
function bindSpinner() {
	// first unbind to avoid count clash
	$('.spinner .bootstrap-touchspin-up').unbind('click');
	$('.spinner .bootstrap-touchspin-up').on('click', function(e) {
		$(this).parent().siblings('input:first').val( parseInt($(this).parent().siblings('input:first').val(), 10) + 1);
		return false;
	});
	//
	$('.spinner .bootstrap-touchspin-down').unbind('click');
	$('.spinner .bootstrap-touchspin-down').on('click', function() {
		if(parseInt($(this).parent().siblings('input:first').val())>0)
			$(this).parent().siblings('input:first').val( parseInt($(this).parent().siblings('input:first').val(), 10) - 1);
		return false;
	});
	// ende
}
// bind spinners on page ready
$(document).ready(bindSpinner);


/**
 *
 *
 *
function handleAjaxLogout(response) {
	//
	if(response.success != undefined && !response.success) {
		//
		//console.log(response);
		window.scrollTo(0,0);
		//
		$('body').append(
			'<div id="darkInizer"></div>' +
			'<div id="alertLoggedOut" role="alert" class="alert alert-danger alert-dismissible fade in">' +
			'   <h4>' + translations.loggedOutHeadline+ '</h4>' +
			'   <p>' + translations.loggedOutText + '</p>' +
			'   <p>' +
			'       <button class="btn btn-default" type="button" onclick="location.href=\'/index.php?cl=account&sourcecl=start\';">' + translations.loginButton + '</button>' +
			'    </p>' +
			'</div>'
		);
		//
		$('#alertLoggedOut').alert();
	}
	// ende
}

/**
 *
 * @param oxid
 * @param highlightoxid
 *
function loadProductOptions(oxid, highlightoxid) {
	// remove and hide options on second click
	if($('#article_selected_' + fixOxId(oxid)).css('display')!='none') {
		$('#article_selected_' + fixOxId(oxid) + ' td').html('');
		$('#article_selected_' + fixOxId(oxid)).hide();
		return;
	}
	//
	var ladda = Ladda.create( document.querySelector( '#article_' + fixOxId(oxid) + ' .btn-show-variants' ) );
	//
	ladda.start();
	//
	var res = $.ajax({
		type: "GET",
		url: '/index.php?cl=getproductselections&fnc=getList&oxid=' + oxid,
		async : true,
		dataType: "json",
		error: handleAjaxLogout,
		success : function (res) {
			//
			handleAjaxLogout(res);
			// show area for select and inject htmlcode
			$('#article_selected_' + fixOxId(oxid) + ' td').html(res.content);
			//
			bindSpinner();
			//
			ladda.stop();
			//
			$('#article_selected_' + fixOxId(oxid)).show();
			//
			if(highlightoxid!=undefined) {
				$('#article_' + fixOxId(fixOxId(highlightoxid))).addClass('highlight');
			}
			// reorganize tab order
			makeTabOrder();
			imageloading();
			// ende
		}
	});
	
	
	//$(window).scroll();
	//end
}


/**
 *  Läd nur die Bilder, die auch im Browserfenster sichtbar sind.
 *
function imageloading(){
	$('.lazy-img').each(function(){
		if($(this).visible() && !$(this).hasClass('loaded')){
			$(this).imageloader().addClass('loaded');
		}
	});
}

/**
 *
 *
function makeTabOrder() {
	$('input.amount').each(function(key,val) {
		$(this).attr('tabindex', key+1);
	});
}


/**
 *
 *
function loadArticleDetails(oxid) {
	//
	var res = $.ajax({
		type: "GET",
		url: '/index.php?cl=ajaxbasket&fnc=getArticleDetails&oxid=' + oxid,
		async: false,
		dataType: "json",
		error: handleAjaxLogout
	}).responseJSON;
	//
	$('body').append(res.content);
	//
	$('#articledetails').modal();
	//
	$('#articledetails').on('hide.bs.modal', function (e) {
		$('#articledetails').remove();
	});
	// ende
}


function loadAdressbuch(oxid) {
	//
	var res = $.ajax({
		type: "GET",
		url: '/index.php?cl=ajaxbasket&fnc=getAdressbuch&oxid=' + oxid,
		async: false,
		dataType: "json",
		error: handleAjaxLogout
	}).responseJSON;
	
	//console.log(res.content);
	
	//
	$('body').append(res.content);
	//
	$('#adressbuch').modal();
	
	var timeOutHolderAdressbookSearch = null;
	
	$('#searchadressbook').keyup(function() {
		
		console.log('suche im Adressbuch');
		//
		if(timeOutHolderAdressbookSearch)
			window.clearTimeout(timeOutHolderAdressbookSearch);
		//
		timeOutHolderAdressbookSearch = window.setTimeout('searchInAdressbook()', 500);
		// ende
	});
	//
	$('#addressbookid, #oxcompany, #oxsal, #oxfname, #oxlname, #oxstreet, #oxstreetnr, #oxaddinfo, #oxzip, #oxcity, #oxfon, #oxcountryid').change(function() {
		$('#addressbookid').val('');
	});
	
	
	//
	$('#adressbuch').on('hide.bs.modal', function (e) {
		$('#adressbuch').remove();
	});
	// ende
}

function setAdress(oxid, company, sal, fname, lname, street, streetnr, addinfo, zip, city, countryid, fon ) {
	//
	$('#addressbookid').val(oxid);
	$('#oxcompany').val(company).attr('placeholder','');
	$('#oxsal').val(sal);
	$('#oxfname').val(fname).attr('placeholder','');
	$('#oxlname').val(lname).attr('placeholder','');
	$('#oxstreet').val(street).attr('placeholder','');
	$('#oxstreetnr').val(streetnr).attr('placeholder','');
	$('#oxaddinfo').val(addinfo).attr('placeholder','');
	$('#oxzip').val(zip).attr('placeholder','');
	$('#oxcity').val(city).attr('placeholder','');
	$('#oxfon').val(fon).attr('placeholder','');
	$('#oxcountryid').val(countryid).attr('placeholder','');
	//
	$('#adressBookAutocomplete').html('');
	$('#searchadressbook').val('');
	// ende
}



function searchInAdressbook() {
	//
	var res = $.ajax({
		type: "GET",
		dataType: "json",
		url: '/index.php?cl=adressbuch&fnc=getAdressAutocomplete&search=' + $('#searchadressbook').val(),
		async : false
	}).responseJSON;
	
	console.log(res);
	
	//
	$('#adressBookAutocomplete').html('');
	//
	$.each(res, function() {
		//
		$('#adressBookAutocomplete').append(
			'<div onclick="setAdress(\'' + this.oxid + '\',\'' + this.company + '\',\'' + this.sal + '\',\'' + this.fname + '\',\'' + this.lname + '\',\'' + this.street + '\',\'' + this.streetnr + '\',\'' + this.addinfo + '\',\'' + this.zip + '\',\'' + this.city + '\',\'' + this.countryid + '\',\'' + this.fon + '\')$(\'#adressformmodal\').show()" class="addressBookItem">' +
			(this.company!='' ? this.company + '<br>': '') +
			this.fname + ' ' + this.lname + '<br>' +
			this.street + ' ' + this.streetnr + '<br>' +
			this.zip + ' ' + this.city + '<br>' +
			'</div>'
		);
		// ende
	});
	// ende
}









/**
 *
 * @param oxid
 *

function jumpToArticle(oxparentid, oxid) {
	//
	var res = $.ajax({
		type: "GET",
		url: '/index.php?cl=ajaxbasket&fnc=getCategoryURL&oxid=' + oxid,
		async: false,
		dataType: "json",
		error: handleAjaxLogout,
		success : function (r) {
			console.log(r);
		}
	}).responseJSON;
	
	//debugger;
	
	//console.log(res);
	//console.log(res.url);
	
	//if(res.url != undefined ) {
	window.location.href = res.url + (res.url.indexOf('?') < 0 ? '?' : '') + '&open=' + oxparentid + '&highlight=' + oxid;
	//}// ende
}

/**
 *
 * @param sParam
 * @returns {*}
 *
function getUrlParameter(sParam)
{
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
			return sParameterName[1];
		}
	}
}

/**
 * check if we need to highlight a oxid
 *
$(document).ready(function() {
	if(getUrlParameter('open')) {
		//
		location.hash = '#' + fixOxId(getUrlParameter('open'));
		//
		$(document).scrollTop($(document).scrollTop() - 175);
		//
		if(getUrlParameter('open')!=getUrlParameter('highlight')) {
			loadProductOptions(getUrlParameter('open'), fixOxId(getUrlParameter('highlight')));
		} else {
			$('#article_' + fixOxId(fixOxId(getUrlParameter('open')))).addClass('highlight');
			loadProductOptions(getUrlParameter('open'));
		}
		// ende
	}
});


/**
 *
 *
function openAllVariants() {
	$('.btn-show-variants').each(function () {
		$(this).click();
	});
	imageloading();
}


/**
 * organize tab order on page load
 *
$(document).ready(function() {
	makeTabOrder();
});

/**
 *
 * @param response
 *
function handleAddTobasketResponse(response) {
	//
	handleAjaxLogout(response);
	//
	//console.log(response);
	if(response.added.length)
	{
		// update the badge
		$('#miniBasket .badge').text(response.itemscount);
		$('#miniBasket .counter').show();
		//
		$('#miniBasket .badge').tooltip('show');
		window.setTimeout("$('#miniBasket .badge').tooltip('hide');", 2500);
		//
		if(response.oxid) {
			$('#article_selected_' + fixOxId(response.oxid)).hide();
		}
		//
		$('input[name^=\'amount\']').val('0');
	}
	else
	{
		//console.log(response);
		$('#nothingaddedtobasketmodal').modal({});
	}
	// ende
	
	//window.location.reload();
	
}

/**
 * adds a bulkform to the basket
 * @param oxid
 *
function addBulkCart(oxid) {
	
	
	var res = $.ajax({
		async : false,
		type: "POST",
		url: '/index.php?cl=ajaxbasket&fnc=addBulk&oxid=' + oxid,
		data: $('#article_selected_' + fixOxId(oxid) + ' form').serialize(),
		dataType: "json",
		success: handleAddTobasketResponse,
		error: handleAjaxLogout
	}).responseJSON;
	
	if(res != undefined ){
		//console.log('true!!!');
		window.location.reload();
	}
	//window.location.reload();
	
	//window.location.reload();
	
	//console.log('Res:' , res);
	
}

function addVeToInput(oxid , amount){
	var wert = parseInt(amount);
	var input = $('#article_' + fixOxId(oxid) + ' input[name=\'amount[' + oxid + ']\']');
	var gesetzt = parseInt(input.first().val());
	var neu = input.first().val(gesetzt + wert);
	//console.log('input: ' + input + 'amount ' + amount + 'neu:' + neu);
	//console.log(neu);
	
}
/**
 * Adds a Single Item to the Basket
 * @param oxid
 *
function addToBasket(oxid, ok, err, noart){
	//
	var data = {};
	data['amount'] = {};
	data['amount'][oxid] = $('#article_' + fixOxId(oxid) + ' input[name=\'amount[' + oxid + ']\']').val();
	//
	if(data['amount'][oxid]==0){
		alertify.error(noart);
		return;
	}
	/*
	 data['bindex'] = oxid;
	 data['am'] = data['amount'][oxid];
	 data['sel'] = oxid;
	 *
	//console.log(data['amount']);
	//
	$.ajax({
		async : false,
		type: "POST",
		url: '/index.php?cl=ajaxbasket&fnc=addBulk&oxid=' + oxid,
		data: data,
		dataType: "json",
		success: handleAddTobasketResponse,
		error: handleAjaxLogout
	});
	window.location.reload();
	alertify.success(ok);
	// ende
}



/**
 *
 *
function allItemsIntoTheBasket() {
	//
	var data = {}, count = 0;
	data['amount'] = {};
	//
	$('input[name^=\'amount\']').each(function() {
		//
		if($(this).val()<1)
			return;
		//
		var strOXID = $(this).attr('name').substr(7);
		strOXID = strOXID.substr(0, strOXID.length-1);
		//
		count+= $(this).val();
		data['amount'][strOXID] = $(this).val();
		// ende
	});
	//
	if(count) {
		$.ajax({
			async : false,
			type: "POST",
			url: '/index.php?cl=ajaxbasket&fnc=addBulk',
			data: data,
			dataType: "json",
			success: handleAddTobasketResponse,
			error: handleAjaxLogout
		});
	}
	window.location.reload();
	// ende
}


/**
 *
 *
function resetBasketSelection() {
	$('input[name^=\'amount\']').val('0');
}


/**
 * Handle the response from toggleFavoritEntry
 *
function handleFavoriteToggle(response) {
	//
	handleAjaxLogout(response);
	//
	if(response.added) {
		$('#article_' + response.oxid + ' .btn-favorite').addClass('btn-primary');
	} else {
		$('#article_' + response.oxid + ' .btn-favorite').removeClass('btn-primary');
	}
	// ende
}

/**
 * Add or Remove a Item from the favorite list
 * @param oxid
 *
function toggleFavoritEntry(oxid) {
	$.ajax({
		async : false,
		type: "POST",
		url: '/index.php?cl=myfavorites&fnc=toggleFavoritEntry&oxid=' + oxid,
		dataType: "json",
		success: handleFavoriteToggle,
		error: handleAjaxLogout
	});
}


/**
 * Add slahes arround " and '
 * @param str
 * @returns {string}
 *
function addslashes(str) {
	return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0').replace(/"/g, '\'');
}

/**
 * Minibasket Popover
 *
$(document).ready(function() {
	//
	$('#minibasketIcon').popover({
		trigger : 'hover',
		placement : 'bottom',
		content : function () {
			//
			var response = $.ajax({
				async : false,
				type: "POST",
				url: '/index.php?cl=ajaxbasket&fnc=getCurrentBasket',
				dataType: "json"
			}).responseJSON;
			//
			var strRet = '<table class="table table-striped" style="min-width:380px">';
			$.each(response.articles, function() {
				strRet+='<tr>';
				strRet+='   <td class="amount">' + this.amount + 'x</td>';
				strRet+='   <td class="title">' + this.title + '</td>';
				strRet+='   <td style="text-align: right">' + this.price + '</td>';
				strRet+='</tr>';
			});
			// Rabatte die angewendet werden:
			
			if(response.discounts != null){
				if(Object.keys(response.discounts).length > 0){
					strRet+='<tr><td></td><td></td><td></td></tr>';
					$.each(response.discounts, function(){
						strRet+='<tr >';
						strRet+='   <td colspan="2"><strong>'+(parseFloat(this.dDiscount) < parseFloat(0) ? response.AufschlagText : response.DiscountText) + ' ' + this.sDiscount +'</strong></td><td style="text-align:right">'+ (parseFloat(this.dDiscount) < parseFloat(0) ? parseFloat(this.dDiscount * -parseFloat(1)).toFixed(2).toString().replace(".",",") : parseFloat(this.dDiscount).toFixed(2)).toString().replace(".",",") +' €</td>';
						strRet+='</tr>';
					});
				}
			}
			strRet+='<tr>';
			strRet+='<td colspan="2"><strong>' + response.summentext + ': </strong></td><td style="text-align:right">' + parseFloat(response.GesamtPreis).toFixed(2).toString().replace(".",",") + ' €</td>';
			
			strRet+='</table>';
			
			// DEBUG:
			console.log(response);
			//strRet+='<div class="minbsum">Gesamtsumme: ' + response.pricesum + '</div>';
			//
			return strRet;
			// ende
		},
		html: true,
		delay: { "hide": 2000 }
	});
	//
	$(document).click(function(e) {
		if (e.target.id == undefined || e.target.id != 'minibasketIcon') {
			$('#minibasketIcon').popover('hide');
		}
	});
	// ende
});

/**
 * Back to top Button:
 *
$(document).ready(function() {
	var offset = 250;
	var duration = 300;
	
	$(window).scroll(function() {
		if ($(this).scrollTop() > offset) {
			$('.back-to-top').fadeIn(duration);
		} else {
			$('.back-to-top').fadeOut(duration);
		}
	});
	$('.back-to-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, duration);
		return false;
	});
});

/**
 * Basket Content zweite Tabelle positionierung
 *
$(document).ready(function() {
	$('.sumeditCol').width($('.editCol').width());
	$('.sumthumbCol').width($('.thumbCol').width());
	$('.sumtitle').width($('.title').width());
	$('.sumcountCol').width($('.countCol').width());
	$('.sumweightCol').width($('.weightCol').width());
	$('.sumpriceCol').width($('.priceCol').width());
	$('.sumuvp').width($('.uvp').width());
	$('.sumtotalCol').width($('.totalCol').width());
});

/**
 * Newsbox Modal
 *
$('#newsboxmodal').on('shown.bs.modal', function () {
	$('#meinEingabefeld').focus();
});

/**
 *  WK Ansicht speichern
 * @param userid
 * @param wkansicht
 *
function setwkansicht(userid, wkansicht){
	var test = wkansicht;
	//var test = $("input:radio[name=wkAnsicht]:checked").val();
	console.log(userid , test);
	//$.get("/index.php?cl=oxcmp_user&fnc=setWkAnsicht&userid="+userid+"&wkAnsicht="+test);
	var res = $.ajax({
		type: "GET",
		url: '/index.php?cl=ajaxbasket&fnc=setWkAnsicht&userid=' + userid + '&wkAnsicht=' + test,
		async: false,
		dataType: "json",
		error: handleAjaxLogout
	}).responseJSON;
	//console.log(res.content);
	location.reload();
}

function setWhitelabel(id){

}

/**
 * WK sortierbar
 *
$( function() {
	$( "#sortable" ).sortable({
		cursor: "move",
		//forceHelperSize: true,
		//forcePlaceholderSize: true,
		placeholder: "ui-state-highlight",
		deactivate: function( event, ui ) {
			var i = 0;
			//var array = $.makeArray();
			//array['alteNummer'] = ui.item.attr( "nummer" );
			
			var nummer = [];
			var neuenummer = [];
			var oxid = [];
			$(ui.item).parent().children().each(function(index){
				$(this).attr({"neuenummer" : index + 1});
			});
			
			var data = [];
			$(ui.item).parent().children().each(function(index){
				
				/* data = Array[ int array[ int array[ 0 = key , 1 = value  ] ] ];
				 * data[0][0][0] = 'oxid';
				 *
				
				data.push([ ['oxid' , $(this).attr("oxid")] , ['nummer' , $(this).attr("nummer")] , ['neueNummer' , $(this).attr("neuenummer")] ]);
			});
			
			console.log(data);
			
			/*
			 var retNummer = nummer;
			 var retNeueNummer = neuenummer;
			 var retOxid = oxid;
			 var data = [retOxid[ retNummer [ retNeueNummer ]]];
			 *
			
			//console.log("data" , data);
			
			var res = $.ajax({
				type: "post",
				url: '/index.php?cl=ajaxbasket&fnc=basketSortierung',
				data: {"element" : data},
				async: false,
				dataType: "json",
				success: function() {
				
				},
				error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\nError:" + err);
				}
			}); // end ajax call
			
			console.log(res);
			//console.dir(res);
			//console.log("ui:" , ui , "nummer:" ,  ui.item.attr( "nummer" ) , "neuenummer", ui.item.attr("neuenummer"));
		}
	});
	$( "#sortable" ).disableSelection();
	
	//location.reload();
	
} );



function test (id) {
	//var erg = null;
	var data = id;
	var response = $.ajax({
		type: "post",
		url: '/index.php?cl=ajaxbasket&fnc=getUserFavorites',
		data: {"userid" : data},
		async: false,
		dataType: "json",
		success: function() {
		},
		error: function(xhr, desc, err) {
			console.log(xhr);
			console.log("Details: " + desc + "\nError:" + err);
		}
	}).responseJSON; // end ajax call
	console.log("erg: " , response);
	$("#test").html(response);
}

/*
 $(document).ready(function test(){
 var data = ["test"];
 
 var response = $.ajax({
 type: "post",
 url: '/index.php?cl=ajaxbasket&fnc=getUserFavorites',
 data: {"post" : data},
 async: false,
 dataType: "json",
 success: function() {
 
 },
 error: function(xhr, desc, err) {
 console.log(xhr);
 console.log("Details: " + desc + "\nError:" + err);
 }
 }).responseJSON; // end ajax call
 console.log("erg: " , response);
 $("#test").text(response.test);
 });
 */

/**
 *
 * @param nummer
 *
function sortchange(nummer){
	console.log(nummer);
}

/**
 *
 *
$( function() {
	$("#Leftsortable").sortable({
		cursor: "move",
		forceHelperSize: true,
		forcePlaceholderSize: true,
		placeholder: "ui-state-highlight"
	});
	$("#Leftsortable").disableSelection();
});

/**
 *
 *
function setbearbeiten(){
	$(function () {
		if(showBearbeiten()){
			
			// Top Navi Leiste
			$('#naviLeiste').find('span').filter( ".sort-visible" ).css('visibility','visible');
			
			$( "#naviLeiste, #userFavoriten" ).sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				forceHelperSize: true,
				forcePlaceholderSize: true,
				revert: true,
				items: "> li",
				start: function (event, ui) {
					$('#userFavoriten').css('height','30px');
				}
			}).disableSelection();
			
			// Yoga
			$('#navsort1').find('span').filter( ".sort-visible" ).css('visibility','visible');
			$( "#navsort1, #userFavoriten" ).sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				forceHelperSize: true,
				forcePlaceholderSize: true,
				helper:"clone",
				revert: true,
				items: "> li",
				start: function (event, ui) {
					$('#userFavoriten').css('height','30px');
				}
			}).disableSelection();
			
			// Physio Massage
			$('#navsort2').find('span').filter( ".sort-visible" ).css('visibility','visible');
			$( "#navsort2, #userFavoriten" ).sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				forceHelperSize: true,
				forcePlaceholderSize: true,
				revert: true,
				items: "> li",
				start: function (event, ui) {
					$('#userFavoriten').css('height','30px');
				}
			}).disableSelection();
			
			// Meditation
			$('#navsort3').find('span').filter( ".sort-visible" ).css('visibility','visible');
			$( "#navsort3, #userFavoriten" ).sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				forceHelperSize: true,
				forcePlaceholderSize: true,
				revert: true,
				items: "> li",
				start: function (event, ui) {
					$('#userFavoriten').css('height','30px');
				}
			}).disableSelection();
			
			// Shiatsu
			$('#navsort4').find('span').filter( ".sort-visible" ).css('visibility','visible');
			$( "#navsort4, #userFavoriten" ).sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				forceHelperSize: true,
				forcePlaceholderSize: true,
				revert: true,
				items: "> li",
				start: function (event, ui) {
					$('#userFavoriten').css('height','30px');
				}
			}).disableSelection();
			
			// Pilates & Fitness
			$('#navsort5').find('span').filter( ".sort-visible" ).css('visibility','visible');
			$( "#navsort5, #userFavoriten" ).sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				forceHelperSize: true,
				forcePlaceholderSize: true,
				revert: true,
				items: "> li",
				start: function (event, ui) {
					$('#userFavoriten').css('height','30px');
				}
			}).disableSelection();
			
			// Angebote
			$('#navsort6').find('span').filter( ".sort-visible" ).css('visibility','visible');
			$( "#navsort6, #userFavoriten" ).sortable({
				connectWith: "#userFavoriten",
				placeholder: "ui-state-highlight",
				forceHelperSize: true,
				forcePlaceholderSize: true,
				helper: 'clone',
				revert: true,
				items: "> li",
				cursor: "move",
				start: function (event, ui) {
					$('#userFavoriten').css('height','30px');
				}
			}).disableSelection();
		}
	});
}

/**
 *
 *
function speichernUserFavoriten(){
	var sort = 0;
	$('#userFavoriten').find('li').closest('#userFavoriten > li').each(function(sort){
		$(this).attr('sort', sort);
		var name = $(this).text();
		$(this).attr('text', name.trim() );
		sort++;
		//console.log('li:' , this);
		//$(this).remove('.sort-visible');
		//$(this).delay( 800 );
	});
	$('#userFavoriten').find( "span" ).remove('.sort-visible');
	var anzahl = $('#userFavoriten').find('li').closest('#userFavoriten > li').size();
	var userid = $('#userFavoriten').attr('userid');
	var data = [];
	var i = 0;
	$('#userFavoriten').find('li').closest('#userFavoriten > li').each(function(i){
		console.log('this: i', i ,  this);
		data.push(
			[ i ,
				[ ['oxid']   , $(this).attr('oxid') ] ,
				[ ['text']   , $(this).attr('text') ] ,
				[ ['type']   , $(this).attr('type') ] ,
				[ ['sort']   , $(this).attr('sort') ] ,
				[ ['anzahl'] , anzahl ] ,
				[ ['userid'] , userid ]
			]
		);
		i++;
	});
	//console.log('data: ', data);
	var response = $.ajax({
		type: "post",
		url: '/index.php?cl=ajaxbasket&fnc=addUserFavorites',
		data: {"item" : data},
		async: false,
		dataType: "json",
		success: function() {
		},
		error: function(xhr, desc, err) {
			console.log(xhr);
			console.log("Details: " + desc + "\nError:" + err);
		}
	}).responseJSON; // end ajax call
	//console.log("speichern: " , response);
	$(function () {
		endBearbeiten();
	});
	window.setTimeout(location.reload(), 1000);
	//window.location.reload();
}

/* ToDo *
function abbruchSpeichernUserFavoriten(oxid){
	$(function () {
		endBearbeiten();
		deleteAllFavorites(oxid);
	});
}

/**
 * User Favoriten bearbeiten.
 * @returns {boolean}
 *
function showBearbeiten(){
	var userid = $('#userFavoriten').attr('userid');
	var anzahl = $('#userFavoriten').find('li').size();
	if(!anzahl > 0){
		$('#AnleitungFavoriten').removeClass('hidden');
	}
	if(anzahl > 10){
		$('.hidden-trash').css('visibility','visible');
		
		$('#bearbeitbar').after().html('<div class="alert alert-danger" id="alarm" role="alert">Sie haben bereits zu viele Favoriten<br>' +
			'<div class="btn-group">'+
			'<button type="button" class="btn btn-danger" href="#" onclick="deleteAllFavorites(\''+userid+'\')">Alle löschen</button>' +
			'<button type="button" class="btn btn-success" href="#" onclick="endBearbeiten()">OK</button> '+
			'</div>' +
			'</div>');
		return false;
	} else {
		$('#bearbeiten-btn').addClass('active').attr( "disabled", "disabled" );
		$('#speichern-btn').addClass('btn-warning').removeAttr("disabled");
		$('#abbruch-btn').addClass('btn-danger').removeAttr("disabled");
		$('#standardfavoriten').empty(); //.addClass("ui-state-highlight").css('height','20px');
		$('.hidden-trash').css('visibility','visible');
		return true;
	}
}

/**
 * User Favoriten Bearbeiten beenden.
 *
function endBearbeiten(){
	$('#bearbeiten-btn').removeClass('active').removeAttr("disabled");
	$('#speichern-btn').removeClass('btn-warning').attr( "disabled", "disabled" );
	$('#abbruch-btn').removeClass('btn-danger').attr( "disabled", "disabled" );
	//$('#standardfavoriten').removeClass("ui-state-highlight").css('height','0');
	$('.hidden-trash').css('visibility','hidden');
	$('#alarm').remove();
}

/**
 * Ausgewählten User Favoriten löschen
 * @param oxid
 *
function deleteFavorite(oxid){
	var data = oxid;
	var response = $.ajax({
		type: "post",
		url: '/index.php?cl=ajaxbasket&fnc=delUserFavorite',
		data: {"item" : data},
		async: false,
		dataType: "json",
		success: function() {
		},
		error: function(xhr, desc, err) {
			console.log(xhr);
			console.log("Details: " + desc + "\nError:" + err);
		}
	}).responseJSON; // end ajax call
	//console.log("deletet: " , response);
	window.location.reload();
}

/**
 * Alle User Favoriten löschen
 * @param oxid
 *
function deleteAllFavorites(oxid){
	var data = oxid;
	var response = $.ajax({
		type: "post",
		url: '/index.php?cl=ajaxbasket&fnc=delAllFavorites',
		data: {"userid" : data},
		async: false,
		dataType: "json",
		success: function() {
		},
		error: function(xhr, desc, err) {
			console.log(xhr);
			console.log("Details: " + desc + "\nError:" + err);
		}
	}).responseJSON; // end ajax call
	//console.log("deletet: " , response);
	window.location.reload();
}


/*
 Checkout Steps:
 *
$(document).ready(function () {
	//Initialize tooltips
	$('.nav-tabs > li a[title]').tooltip();
	//Wizard
	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
		var $target = $(e.target);
		if ($target.parent().hasClass('disabled')) {
			return false;
		}
	});
	$(".next-step").click(function (e) {
		var $active = $('.wizard .nav-tabs li.active');
		$active.next().removeClass('disabled');
		nextTab($active);
	});
	$(".prev-step").click(function (e) {
		var $active = $('.wizard .nav-tabs li.active');
		prevTab($active);
	});
	
	$('.bntooltip').tooltip({ trigger: "hover" });
	
});

function nextTab(elem) {
	$(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
	$(elem).prev().find('a[data-toggle="tab"]').click();
}

/* end Checkout Steps  *


function changeShipAdress(e){
	//console.log('e: ', e.target);
	//console.log( $(e.target).prop("checked") );
	if($(e.target).prop("checked")){
		console.log('wahr');
		console.log($(document).scrollTop());
		$('#shippingAddress').css('display','none');
	} else {
		$('#shippingAddress').css('display','block');
		console.log($(document).scrollTop());
		$(document).scrollTop(100);
		console.log($(document).scrollTop());
		console.log('unwahr');
	}
	//$('#shippingAddress').toggle( e.is(':not(:checked)'));
}

function myscrollTo(e){
	var set = parseInt(e);
	$('html, body').animate({scrollTop: set}, 300);
}

$('#scroll').click(function(e){
	//console.log('element: ', e);
	//$.scrollTo('+=100px', 800, { axis:'y' });
	console.log('ist da:' , $(window).scrollTop());
	$('html, body').animate({scrollTop: ($(window).scrollTop() + 100)}, 300);
	console.log('ist da:' , $(window).scrollTop());
	/*console.log('ist da:' , $(window).scroll());
	 $(window).scroll(50);
	 console.log('ist da:' , $(window).scroll());*
});

/**
 * Testalert
 *
function testalert(){
	// alert dialog
	alertify.alert("Message");
	// confirm dialog
	alertify.confirm("confirm dialog", function (e) {
		if (e) {
			// user clicked "ok"
		} else {
			// user clicked "cancel"
		}
	});
	// prompt dialog
	alertify.prompt("prompt dialog", function (e, str) {
		// str is the input text
		if (e) {
			// user clicked "ok"
		} else {
			// user clicked "cancel"
		}
	}, "Default Value");
	// standard notification
	// setting the wait property to 0 will
	// keep the log message until it's clicked
	// alertify.log("Notification", type, wait);
	alertify.log("Notification", "log", 0 ); //wait);
	alertify.success("Success notification");
	alertify.error("Error notification");
}


/**
 * Eigene Suche Autosuggest
 *
function doSearch() {
	var q = jQuery.trim($('#searchparam').val());
	//
	//$('#header .searchBox').addClass('resultspresent');
	$('.navbar-form .searchBox').addClass('resultspresent');
	//
	console.log(q);
	console.log(startSuggest);
	
	if (q.length >= startSuggest) {
		// neu: $("#results").hide();
		$.ajax({
			url: '/modules/autosuggest/controllers/autosuggest.php?q=' + q,
			cache: false,
			async: false,
			dataType: "html",
			type: "get",
			success: function(data) {
				if(data) {
					$("#results").html(" ");
					$("#results").html(data);
					$("#results").show();
				}
			}
		});
	} else {
		$('#results').hide();
	}
}
/**
 * Eigene Suche SolrSuggest
 *
function doSolrSearch(){
	var q = jQuery.trim($('#solrsearchparam').val());
	//
	//$('#header .searchBox').addClass('resultspresent');
	$('.navbar-form .searchBox').addClass('resultspresent');
	//
	if (q.length >= startSuggest) {
		// neu: $("#results").hide();
		$.ajax({
			url: '/modules/solrsuche/controllers/solrsuggest.php?q=' + q,
			cache: false,
			async: false,
			dataType: "html",
			type: "get",
			success: function(data) {
				if(data) {
					$("#solrresults").html(" ");
					$("#solrresults").html(data);
					$("#solrresults").show();
				}
			}
		});
	} else {
		$('#solrresults').hide();
	}
}

/**
 * Eigene Suche Autosuggest mit delay
 *
function autoSuggest() {
	
	//$("#foo").bindWithDelay("keyup", function(e) { }, 100);
	
	/*
	 //
	 if(objQuickSearchTimoutHandler)
	 window.clearTimeout(objQuickSearchTimoutHandler);
	 //load with a delay to avaoid duolciate querys on keyup
	 objQuickSearchTimoutHandler = window.setTimeout(doSearch, 100);
	 // end
	 *
}



function solrSuggest(){
	//
	if(objSolrQuickSearchTimoutHandler)
		window.clearTimeout(objSolrQuickSearchTimoutHandler);
	// load with a delay to avaoid duolciate querys on keyup
	objSolrQuickSearchTimoutHandler = window.setTimeout(doSolrSearch, 100);
	// end
}


function bildgross() {
	//$('.mag').toggle('width','100%');
	if($('.detailbild').css('width') == '200px'){
		$('.detailbild').css('width','100%');
	} else {
		$('.detailbild').css('width','200px');
	}
	
}
*/
