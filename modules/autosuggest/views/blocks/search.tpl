[{if $oView->showSearch()}]
    [{oxscript include=$oViewConf->getModuleUrl('autosuggest','src/initall.js') priority=10 }]
    [{oxscript add="var objQuickSearchTimoutHandler = null;"}]
    [{oxscript include="js/widgets/oxinnerlabel.js" priority=10 }]
    [{oxscript add="$( '#searchParam' ).oxInnerLabel();"}]
	<div class="center-block" style="text-align: center; padding-top: 10px">
		<div class="input-group center-block">
			<form class="navbar-form" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search" id="search">
				<div class="form-group form-group-suche" id="quicksearch">
					<div id="results" style="overflow: auto;"></div>
					[{$oViewConf->getHiddenSid()}]
					<input type="hidden" name="cl" value="search">
				    <input type="text" class="form-control" id="searchparam" name="searchparam" placeholder="[{oxmultilang ident="SEARCHTEXT"}]" autocomplete="off" onkeyup="dresearch(this);/*autoSuggest();*/" value="">
				    <span class="input-group-btn" style="width: auto">
				        <button class="btn btn-default bntooltip" type="submit" onclick="$('#search').submit();" data-placement="bottom" title="[{oxmultilang ident="SEARCH"}]"><span class="glyphicon glyphicon-search"></span></button>
				    </span>
				</div>
			</form>
		</div>
	</div>
[{/if}]
