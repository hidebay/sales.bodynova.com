<?php


array_push( $aIncReports, array('name'  => 'artikelhaendlerendkunden',
    'title' => array('de' => 'Alle Artikel die nicht im Endkundenshop sind aber im Händlershop'),
    'desc'  => array('de' => 'Alle Artikel die nicht im Endkundenshop sind aber im Händlershop')
));

if ($cReportType == 'artikelhaendlerendkunden') {
    // bekomme daten von endkundenshop
    require_once 'oxprobs_articles_getarticlesvomendkundenshop.php';

    //$sWhereActive = 'a.oxactivefrom != '0000-00-00 00:00:00' AND a.oxactivefrom < NOW() AND a.oxactiveto != '0000-00-00 00:00:00' AND a.oxactiveto < NOW() ';
    $sWhereActive = 'a.oxactivefrom > NOW() ';

    //
    $sSql1 ='
         SELECT
          oxarticles.oxactive AS oxactive,
          oxarticles.oxstock AS oxstock,
          oxarticles.oxprice AS oxprice,
          oxarticles.oxid,
          oxarticles.oxartnum,
          oxarticles.oxean,
          oxarticles.oxid,
          IF(oxarticles.oxparentid=\'\',oxarticles.oxtitle,(SELECT a1.oxtitle FROM oxarticles a1 WHERE a1.oxid=oxarticles.oxparentid)) AS oxtitle,
          IF(oxarticles.oxparentid=\'\',(SELECT m.oxtitle FROM oxmanufacturers m WHERE oxarticles.oxmanufacturerid = m.oxid),(SELECT m.oxtitle FROM oxarticles a1, oxmanufacturers m WHERE oxarticles.oxparentid = a1.oxid AND a1.oxmanufacturerid = m.oxid)) AS oxmantitle,
          oxarticles.oxvarselect
         FROM
          oxarticles
         LEFT JOIN
          oxarticlenumsendkunden ON oxarticles.oxartnum=oxarticlenumsendkunden.oxartnum
         WHERE
            oxarticlenumsendkunden.oxartnum IS NULL
          AND
            oxarticles.oxactive>0
          AND
            oxarticles.keinabgleichmitendkunden=0
    ';
    $sSql2 = '';
    // ende
}

?>
