<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 13.03.17
 * Time: 11:57
 */
require '../bootstrap.php';
require dirname(__DIR__).'/vendor/apache/log4php/src/main/php/Logger.php';
error_reporting(E_ALL);
ini_set('display_startup_errors', 1);
#ini_set('log_errors', 1);
ini_set('html_errors', 1);
ini_set('display_errors', 1);
Logger::configure('logconfig.xml');

ini_set('auto_detect_line_endings', true);

if(isset($_POST['string'])){
	$teststring = $_POST['string'];
	
	$log = Logger::getLogger('disk');
	$log->info('Ampel '.date("d.m.y h:m:s")."\n");
	$log->info($teststring);
	
	
} else {
	$teststring = 'kur41409576f598d48e4b9f25b697618#15,92#Ja#0####7,7#8,35#8,85#9,1999999999999993#11,1#0,30#4250367915905#20######';
}


$cols = array();
$data = str_getcsv($teststring, '#');

// An Skript übergebene Werteliste:
$cols['oxid']                = $data[0];
$cols['oxtprice']            = round(str_replace(',', '.', $data[1]),2);
$cols['oxactive']            = ($data[2]==='Ja'? 1 : 0);
$cols['bnflagbestand']       = ($data[3]? $data[3]:0);
$cols['oxdelivery']          = ($data[4]>0?date('Y-m-d', strtotime($data[4])): '0000-00-00' );
$cols['oxmindeltime']        = ($data[5] > 0 ? $data[5] : '0' );
$cols['oxmaxdeltime']        = ($data[6] > 0 ? $data[6] : '0' );
$cols['oxpricea']            = ($data[7] ? round(str_replace(',', '.', $data[7]),2) : '0');
$cols['oxpriceb']            = ($data[8] ? round(str_replace(',', '.', $data[8]),2) : '0');
$cols['oxpricec']            = ($data[9] ? round(str_replace(',', '.', $data[9]),2) : '0');
$cols['oxpriced']            = ($data[10]? round(str_replace(',', '.', $data[10]),2): '0');
$cols['oxpricee']            = ($data[11]? round(str_replace(',', '.', $data[11]),2): '0');
$cols['oxpricef']            = ($data[20]? round(str_replace(',', '.', $data[20]),2): '0');
$cols['oxweight']            = (str_replace(',', '.', $data[12]) > 0? (round(str_replace(',', '.', $data[12]),2)): '0');
$cols['oxean']               = ($data[13]? $data[13] : '');
$cols['verpackungseinheit']  = ($data[14]? $data[14] : 0);
$cols['preisneua']           = ($data[15]? round(str_replace(',', '.', $data[15]),2) : 0);
$cols['preisneub']           = ($data[16]? round(str_replace(',', '.', $data[16]),2) : 0);
$cols['preisneuc']           = ($data[17]? round(str_replace(',', '.', $data[17]),2) : 0);
$cols['preisneud']           = ($data[18]? round(str_replace(',', '.', $data[18]),2) : 0);
$cols['preisneue']           = ($data[19]? round(str_replace(',', '.', $data[19]),2) : 0);


$obArticle = oxNew('bn_oxarticle');

//$obArticle->disableLazyLoading();

if($obArticle->load($cols['oxid'])){

	//check ob Variante oder Vater:

    try {
        $oDb = oxDb::getDb();
        /*
        $sOxId = $oDb->quote($this->getId());
        $sOxShopId = $oDb->quote($this->getShopId());
        */
        $sUpdate = "
                update oxarticles
                    set 
                    oxtprice = " . $cols['oxtprice'] . ",
                    oxactive = " . $cols['oxactive'] . ",
                    bnflagbestand = " . $cols['bnflagbestand'] . ",
                    oxdelivery = '" . $cols['oxdelivery'] . "',
                    oxmindeltime = " . $cols['oxmindeltime'] . ",
                    oxmaxdeltime = " . $cols['oxmaxdeltime'] . ",
                    oxpricea = " . $cols['oxpricea'] . ",
                    oxpriceb = " . $cols['oxpriceb'] . ",
                    oxpricec = " . $cols['oxpricec'] . ",
                    oxpriced = " . $cols['oxpriced'] . ",
                    oxpricee = " . $cols['oxpricee'] . ",
                    oxpricef = " . $cols['oxpricef'] . ",
                    oxweight = " . $cols['oxweight'] . ",
                    oxean = '" . $cols['oxean'] . "',
                    verpackungseinheit = " . $cols['verpackungseinheit'] . ",
                    preisneua = " . $cols['preisneua'] . ",
                    preisneub = " . $cols['preisneub'] . ",
                    preisneuc = " . $cols['preisneuc'] . ",
                    preisneud = " . $cols['preisneud'] . ",
                    preisneue = " . $cols['preisneue'] . "
                    where oxid = '" . $cols['oxid'] . "'
            ";
        $oDb->execute($sUpdate);
    }catch (Exception $e){
        $value = date('d.m.y H:i:s').' oxid --- '.$cols['oxid'].' error --- nicht vorhanden string :'.$teststring;
        $log = Logger::getLogger('error');
        $log->error($e."\n");
        /*
        echo '<pre>';
        print_r($e);
        die();
        */
    }

    /*
	$obArticle->__set('oxarticles__oxtprice', new oxField($cols['oxtprice']));
	$obArticle->__set('oxarticles__oxactive', new oxField($cols['oxactive']));
	$obArticle->__set('oxarticles__bnflagbestand', new oxField($cols['bnflagbestand']));
	$obArticle->__set('oxarticles__oxdelivery', new oxField($cols['oxdelivery']));
	$obArticle->__set('oxarticles__oxmindeltime', new oxField($cols['oxmindeltime']));
	$obArticle->__set('oxarticles__oxmaxdeltime', new oxField($cols['oxmaxdeltime']));
	$obArticle->__set('oxarticles__oxpricea', new oxField($cols['oxpricea']));
	$obArticle->__set('oxarticles__oxpriceb', new oxField($cols['oxpriceb']));
	$obArticle->__set('oxarticles__oxpricec', new oxField($cols['oxpricec']));
	$obArticle->__set('oxarticles__oxpriced', new oxField($cols['oxpriced']));
	$obArticle->__set('oxarticles__oxpricee', new oxField($cols['oxpricee']));
	$obArticle->__set('oxarticles__oxpricef', new oxField($cols['oxpricef']));
	$obArticle->__set('oxarticles__oxweight', new oxField($cols['oxweight']));
	$obArticle->__set('oxarticles__oxean', new oxField($cols['oxean']));
	$obArticle->__set('oxarticles__verpackungseinheit', new oxField($cols['verpackungseinheit']));
	$obArticle->__set('oxarticles__preisneua', new oxField($cols['preisneua']));
	$obArticle->__set('oxarticles__preisneub', new oxField($cols['preisneub']));
	$obArticle->__set('oxarticles__preisneuc', new oxField($cols['preisneuc']));
	$obArticle->__set('oxarticles__preisneud', new oxField($cols['preisneud']));
	$obArticle->__set('oxarticles__preisneue', new oxField($cols['preisneue']));


	
	$obArticle->save();
    */
    /*
    echo '<pre>';
    print_r($obArticle);
    die();
    */
	$savestring = implode(" , ", $cols);
	
	$log = Logger::getLogger('disk');
	$log->info('geschrieben: '.date("d.m.y H:m:s"));
	$log->info($savestring);
	
	
}else{
	$value = date('d.m.y H:i:s').' oxid --- '.$cols['oxid'].' error --- nicht vorhanden string :'.$teststring;
	$log = Logger::getLogger('error');
	$log->error($value."\n");
	
	echo 'nicht geladen!';
}



echo '<pre>';


if($obArticle->getVariantsCount() > 0 ){
	echo 'Vater!';
} else {
	
	
	if($obArticle->oxarticles__oxparentid->value > 0){
		$Vater = oxnew('bn_oxarticle');
		echo 'Preisload: '.$Vater->isRangePrice();
		$Vater = $obArticle->getParentArticle();
		echo 'VarMinPreis: <br>';
		echo 'Varcount: '.$Vater->getVariantsCount().'<br><br>';
		$Vater->save();
	} else {
		echo 'Standartartikel<br>';
		
		//print_r($obArticle);
	}
	
	
	//echo 'Preis: <br>'. $Vater->getPreisNeu();
	//echo '<br>';
	//print_r($Vater);
	
	//echo '<pre>';
	//print_r($Vater);
	//$Vater->_setVarMinMaxPrice();
	
	//$Vater->onChange(ACTION_UPDATE_STOCK, $obArticle->oxarticles__oxid->value, $obArticle->oxarticles__oxparentid->value);
	
	
	//$Vater->_onChangeUpdateVarCount($obArticle->oxarticles__oxparentid->value);
	//$Vater->save();
	
}

//$obArticle->__set( 'oxarticles__oxprice' , $cols['oxprice']);

//$oPrice = $obArticle->getPrice();


//$oPrice->setPrice($cols['oxprice']);
//$obArticle->setPrice($oPrice);
//$obArticle->save();


//$obArticle->__set( 'oxprice' , $cols['oxprice']);


echo '<pre>';
//echo 'teststring:';
//print_r($data);
echo 'Ampel geschrieben '.date("d.m.y H:m:s").' : <br>';
print_r($cols);
/*echo 'oxprice:';
print_r($oPrice);
echo 'Artikel:';
print_r($obArticle);
echo 'Vater:';
print_r($Vater);

*/
