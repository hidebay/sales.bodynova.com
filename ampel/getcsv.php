<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 20.12.17
 * Time: 17:02
 */
ini_set('auto_detect_line_endings', true);
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('Europe/Berlin');
error_reporting(E_STRICT);
#error_reporting(E_STRICT);
ini_set('display_startup_errors', 1);
#ini_set('log_errors', 1);
ini_set('html_errors', 1);
ini_set('display_errors', 1);

require '../bootstrap.php';


$db = oxDb::getDb(oxDB::FETCH_MODE_ASSOC);

$query = "
select
	OXARTNUM,
	OXACTIVE,
	bnflagbestand,
	OXDELIVERY
FROM
	oxarticles
";

$erg = $db->getAll($query);
/*
$querydeaktiv ="select
	OXARTNUM,
	OXACTIVE,
	bnflagbestand,
	OXDELIVERY
FROM
	oxarticles
where
	OXACTIVE = 0";

$ergdeaktiv = $db->getAll($querydeaktiv);
*/
$fp = fopen('../export/bestand.csv', 'w');

#$fpdeaktiv = fopen('../export/deaktiv.csv', 'w');

$feldnamen = array('Artnumber','Flag','DeliveryTime');

fputcsv($fp, $feldnamen);

#fputcsv($fpdeaktiv, $feldnamen);
//echo '<pre>';
foreach ($erg as $fields) {
	$artikel = trim($fields['OXARTNUM']);
	if($fields['bnflagbestand'] == 0){
		$bnflag = 100;
	}
	if($fields['bnflagbestand'] == 1){
		$bnflag = 10;
	}
	if($fields['bnflagbestand'] == 2){
		$bnflag = 0;
	}
	
	if($fields['OXACTIVE'] == 0){
		$bnflag = 0;
	}
	$fields['bnflagbestand'] = $bnflag;
	$fields['OXARTNUM'] = 'BDN'.$artikel;
	
	if(date('Y-m-d',$fields['OXDELIVERY']) < date('Y-m-d')){
		$fields['OXDELIVERY'] = '0000-00-00';
	
	}
	
	$felder = array($fields['OXARTNUM'],$bnflag,$fields['OXDELIVERY']);//, $bnflag, $fields['OXACTIVE']);
	fputcsv($fp, $felder);
	//print_r($felder);
}
/*
foreach ($ergdeaktiv as $fields) {
	$artikel = trim($fields['OXARTNUM']);
	/*if($fields['bnflagbestand'] == 0){
		$bnflag = 100;
	}
	if($fields['bnflagbestand'] == 1){
		$bnflag = 10;
	}
	if($fields['bnflagbestand'] == 2){
		$bnflag = 0;
	}
	$bnflag = 0;
	$fields['bnflagbestand'] = $bnflag;
	$fields['OXARTNUM'] = 'BDN'.$artikel;
	
	fputcsv($fpdeaktiv, $fields);
	#print_r($fields);
}
*/
fclose($fp);

#fclose($fpdeaktiv);

sleep(2);
#echo '<a href="'.getenv('HTTP_HOST').'/export/bestand.csv" download>CSV Datei laden</a>';
#echo '<a href="http://sales.bodynova.local/export/bestand.csv" download>Load CSV File</a>';
?>
<div style="width:100%; overflow: hidden;">
	<div style="margin-left:auto;margin-right:auto;width:50%;">
		<!--<img src="https://sales.bodynova.com/ampel/MerryChristmas.jpg" width="100%"><br/>-->
		<div style="text-align: center"><?php echo '<a href="https://sales.bodynova.com/export/bestand.csv" download>Load CSV File</a>';?></div>
		<!--<div style="text-align: center"><img src="https://sales.bodynova.com/ampel/santaDancing.gif" width="25%"></div>-->
		<!--<div style="text-align: center"><?php //echo '<a href="https://sales.bodynova.com/export/deaktiv.csv" download>download deaktive CSV File</a>';?></div>-->
	</div>
</div>
