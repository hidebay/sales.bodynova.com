<?php

require '../bootstrap.php';
ini_set('auto_detect_line_endings', true);

if(isset($_GET['datei']) || isset($_POST['string'])){
	if($_GET['datei'] === 'debug'){
		$debug = true;
		$teststring = "28bc43369d88936962c5268e20353cdc#36,93#Ja#0####19#21,8#22,95#28,8#29,95#2#4250367912966#1";
	} elseif (isset($_POST['string'])) {
		$teststring = $_POST['string'];
	} else{
		// holt die Datei per FTP
		$ftp_server= '92.50.75.170';
		$ftp_user= 'haendlershop';
		$ftp_pass= 'peanuts30';
		$localfile = dirname(__FILE__).'/'.$_GET['datei'];
		$remotefile = 'JetFlash-Transcend16GB-01/FRITZ/ampel/'.$_GET['datei'];
		$cid=ftp_connect($ftp_server);
		ftp_login($cid, $ftp_user, $ftp_pass);
		ftp_pasv($cid, true);
		$erg = ftp_get($cid , $localfile , $remotefile, FTP_BINARY);
		#ftp_put($cid, $destination_file, $file, FTP_BINARY);
		ftp_close($cid);
	}
	if(file_exists($_GET['datei']) || $_GET['datei'] === 'debug' || $teststring != null) {
	// überprüft ob die Datei vorhanden ist.
		if($_GET['datei'] !== 'debug' && $teststring == null) {
			// öffnet die Datei
			$fp = fopen($_GET['datei'], 'r');
			// Schleife über jede Zeile und übergabe an ein array
			$cols = array();
			while (($data = fgetcsv($fp, 6000, "#")) !== false) {
				$cols[] = $data;
			}
		} else {
			//debug bereich
			$cols = array();
			$data = str_getcsv($teststring, '#');
			$cols[] = $data;
		}
	
		if($debug) {
			echo '<pre>';
			print_r($cols);
			echo '</pre>';
		}
		
		// datenbank Verbindung herstellen.
		try{
			$oDb = oxDb::getDb(oxDB::FETCH_MODE_ASSOC);
		} catch(Exception $e){
			echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
			die();
		}
		
		foreach($cols as $key) {
			
			echo 'OXID: ' . $key[0] . '<br>';
			echo 'OXTPRICE: ' . round(str_replace(',', '.', $key[1]), 2) . '<br>';
			echo 'OXACTIVE: ' . ($key[2] == 'Ja' ? 1 : 0) . '<br>';
			echo 'OXSTOCKFLAG: ' . ($key[3] ? $key[3] : 0) . '<br>';
			echo 'OXDELIVERY:' . ($key[4] > 0 ? date('Y-m-d', strtotime($key[4])) : '0000-00-00') . '<br>';
			echo 'OXMINDELTIME: ' . ($key[5] > 0 ? $key[5] : 0) . '<br>';
			echo 'OXMAXDELTIME: ' . ($key[6] > 0 ? $key[6] : 0) . '<br>';
			echo 'OXPRICEA:' . round(str_replace(',', '.', $key[7]), 2) . '<br>';
			echo 'OXPRICEB:' . round(str_replace(',', '.', $key[8]), 2) . '<br>';
			echo 'OXPRICEC:' . round(str_replace(',', '.', $key[9]), 2) . '<br>';
			echo 'OXPRICED:' . round(str_replace(',', '.', $key[10]), 2) . '<br>';
			echo 'OXPRICEE:' . round(str_replace(',', '.', $key[11]), 2) . '<br>';
			echo 'OXWEIGHT: ' . round(str_replace(',', '.', $key[12]), 2) . '<br>';
			echo 'OXEAN: ' . ($key[13] ? $key[13] : null) . '<br>';
			echo 'VE:' . $key[14].'<br>';

			// SQL Query um die Daten in die DB zu speichern.
			$sql = 'update oxarticles set ' .
				'oxtprice = ' . round(str_replace(',', '.', $key[1]), 2) . ' , ' .
				'oxactive = ' . ($key[2] == 'Ja' ? 1 : 0) . ' , ' .
				//'oxstockflag = '.$key[3].' , '.
				'oxstockflag = 1 , ' .
				'bnflagbestand = ' . ($key[3] ? $key[3] : 0) . ' , ' .
				'oxdelivery = \'' . ($key[4] > 0 ? date('Y-m-d', strtotime($key[4])) : 0000-00-00) . '\' , ' .
				'oxmindeltime = ' . ($key[5]>0 ? $key[5] : 0) . ' , ' .
				'oxmaxdeltime = ' . ($key[6]>0 ? $key[6] : 0) . ' , ' .
				'oxpricea = ' . round(str_replace(',', '.', $key[7]), 2) . ' , ' .
				'oxpriceb = ' . round(str_replace(',', '.', $key[8]), 2) . ' , ' .
				'oxpricec = ' . round(str_replace(',', '.', $key[9]), 2) . ' , ' .
				'oxpriced = ' . round(str_replace(',', '.', $key[10]), 2) . ' , ' .
				'oxpricee = ' . round(str_replace(',', '.', $key[11]), 2) . ' , ' .
				'oxweight = ' . round(str_replace(',', '.', $key[12]), 2) . ' , ' .
				'oxean = \'' . ($key[13] ? $key[13] : '') . '\' , ' .
				'verpackungseinheit = \'' . ($key[14] ? $key[14] : null) . '\'' .
				' where oxid = \'' . $key[0] . '\'';
			
			
			if(!$debug) {
				$oDb->execute($sql);
			}
			// ausgabe der Query's
			if(isset($_GET['debug']) && $_GET['debug'] == 1) {
				echo $sql;
			}
		}
		// schließen der Datei.
		if($_GET['datei'] !== 'debug'){
			fclose($fp);
		}
	} else {
		echo '<h1>File '.$_GET['datei'].' existiert nicht!</h1>';
	}
}
