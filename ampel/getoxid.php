<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 05.12.16
 * Time: 11:06
 */
/*
 * Parameter:
 * gibt die oxidid von Artikelnummer 938sy aus
 * artnum=938sy
 *
 * gibt die parentid von Artikelnummer 938sy aus
 * aktion=parent
 */

require dirname(__DIR__).'/bootstrap.php';
ini_set('auto_detect_line_endings', true);
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('Europe/Berlin');
error_reporting(E_STRICT);
ini_set('display_startup_errors', 1);
#ini_set('log_errors', 1);
ini_set('html_errors', 1);
ini_set('display_errors', 1);

if(isset($_GET['artnum'])){
	$artnum = $_GET['artnum'];
	$oDb = oxDb::getDb();
	$sQ = "select oxid, oxparentid from oxarticles where oxartnum = '".$artnum."'";
	
	$sDbValue = $oDb->getAll($sQ);
	if ($sDbValue != false) {
		if(count($sDbValue) == 1){
			if(isset($_GET['aktion']) && $_GET['aktion'] === 'parent'){
				if(isset($sDbValue[0][1]) && !empty($sDbValue[0][1])){
					// gebe Parentid aus
					echo $sDbValue[0][1];
				}
			}else {
				// gebe Oxid aus
				echo $sDbValue[0][0];
			}
			//debug:
			#print_r($sDbValue);
		}else{
			echo 'error: too much oxartnums';
			// error notification send email to admin
			$oxEmail = oxNew('oxemail');
			$oxEmail->setSubject('Ampel Fehler!');
			$oxEmail->setBody("Fehler: too much oxartnums from artnum = ".$artnum."\n".
				" ");
			$oxEmail->setRecipient('a.bender@bodynova.de', 'André Bender');
			$oxEmail->setReplyTo('server@bodynova.de' , 'Server Bodynova');
			$oxEmail->send();
		}
	}
} else {
	echo 'error no artnum';
}
