<?php

require '../bootstrap.php';
ini_set('auto_detect_line_endings', true);
ini_set('error_reporting', E_ALL);
ini_set('log_errors', true);
ini_set('display_errors', true);
ini_set('error_log', __DIR__.'/log/psErrorLog_PHP.log');

$teststring = '0da02443a66dd875658a6a3b8711f5c8#150,42#Ja#2#14.03.2017###88#92#99#108,5#118#20#4250367906156#1#####';

if(isset($_GET['datei'])){
	
	$funktion = null;
	if(isset($_GET['fnc'])){
		$funktion = $_GET['fnc'];
	}
	
	if($funktion != 'test'){
		// holt die Datei per FTP
		$ftp_server= '92.50.75.170';
		$ftp_user= 'haendlershop';
		$ftp_pass= 'peanuts30';
		$localfile = dirname(__FILE__).'/'.$_GET['datei'];
		$remotefile = 'JetFlash-Transcend16GB-01/FRITZ/ampel/'.$_GET['datei'];
		$cid=ftp_connect($ftp_server);
		ftp_login($cid, $ftp_user, $ftp_pass);
		ftp_pasv($cid, true);
		$erg = ftp_get($cid , $localfile , $remotefile, FTP_BINARY);
		#ftp_put($cid, $destination_file, $file, FTP_BINARY);
		ftp_close($cid);
		// überprüft ob die Datei vorhanden ist.
		if(file_exists($_GET['datei'])){
			// öffnet die Datei
			$fp = fopen($_GET['datei'], 'r');
		}
		// Schleife über jede Zeile und übergabe an ein array
		$cols = array();
		while (($data = fgetcsv($fp, 6000, "#")) !== false)
		{
			$cols[] = $data;
		}
	} elseif (isset($_GET['teststring']) && $_GET['teststring'] == 1) {
		if ($teststring != null) {
			//debug bereich
			$cols = array();
			$data = str_getcsv($teststring, '#');
			$cols[] = $data;
		}
	}else{
		
		$fp = fopen(dirname(__FILE__).'/Haendlerbatchneu.tab', 'r');
		// Schleife über jede Zeile und übergabe an ein array
		$cols = array();
		while (($data = fgetcsv($fp, 6000, "#")) !== false)
		{
			$cols[] = $data;
		}
	}
		
		
		echo '<pre>';
		if(isset($_GET['debug']) && $_GET['debug'] == 1) {
			print_r($cols);
		}
		//die();
		$zähler = 0;
		
		// datenbank Verbindung herstellen.
		$oDb = oxDb::getDb( oxDB::FETCH_MODE_ASSOC );
		foreach($cols as $key){
			
			/*
			echo 'OXID: ' . $key[0] . '<br>';
			echo 'OXTPRICE: '.round(str_replace(',', '.', $key[1]),2).'<br>';
			echo 'OXACTIVE: '.($key[2]=='Ja'? 1 : 0).'<br>';
			echo 'OXSTOCKFLAG: '.($key[3]? $key[3]:0).'<br>';
			echo 'OXDELIVERY:'.($key[4] > 0 ? date('Y-m-d', strtotime($key[4])): null).'<br>';
			echo 'OXMINDELTIME: '.$key[5].'<br>';
			echo 'OXMAXDELTIME: '.$key[6].'<br>';
			echo 'OXPRICEA:'.round(str_replace(',', '.', $key[7]),2).'<br>';
			echo 'OXPRICEB:'.round(str_replace(',', '.', $key[8]),2).'<br>';
			echo 'OXPRICEC:'.round(str_replace(',', '.', $key[9]),2).'<br>';
			echo 'OXPRICED:'.round(str_replace(',', '.', $key[10]),2).'<br>';
			echo 'OXPRICEE:'.round(str_replace(',', '.', $key[11]),2).'<br>';
			echo 'OXWEIGHT: '.round(str_replace(',', '.', $key[12]),2).'<br>';
			echo 'OXEAN: '.($key[13]? $key[13] : null).'<br>';
			echo 'verpackungseinheit: '.($key[14]? $key[14]:null).'<br>';
			echo 'Haendlerpreis 1: ' .round(str_replace(',', '.', $key[15]),2).'<br>';
			echo 'Haendlerpreis 2: ' .round(str_replace(',', '.', $key[16]),2).'<br>';
			echo 'Haendlerpreis 3: ' .round(str_replace(',', '.', $key[17]),2).'<br>';
			echo 'Haendlerpreis 4: ' .round(str_replace(',', '.', $key[18]),2).'<br>';
			echo 'Haendlerpreis 5: ' .round(str_replace(',', '.', $key[19]),2).'<br>';
			*/
			
			// SQL Query um die Daten in die DB zu speichern.
			
			$sql =  'update oxarticles set '.
				'oxtprice = '.round(str_replace(',', '.', $key[1]),2).' , '.
				'oxactive = '.($key[2]=='Ja'? 1 : 0).' , '.
				//'oxstockflag = '.$key[3].' , '.
				'oxstockflag = 1 , '.
				'bnflagbestand = '.($key[3]? $key[3]:0).' , '.
				'oxdelivery = "'.($key[4] > 0 ? date('Y-m-d', strtotime($key[4])): '0000-00-00' ).'" , '.
				'oxmindeltime = '.($key[5] > 0 ? $key[5] : '0' ).' , '.
				'oxmaxdeltime = '.($key[6] > 0 ? $key[6] : '0' ).' , '.
				'oxpricea = '.round(str_replace(',', '.', $key[7]),2).' , '.
				'oxpriceb = '.round(str_replace(',', '.', $key[8]),2).' , '.
				'oxpricec = '.round(str_replace(',', '.', $key[9]),2).' , '.
				'oxpriced = '.round(str_replace(',', '.', $key[10]),2).' , '.
				'oxpricee = '.round(str_replace(',', '.', $key[11]),2).' , '.
				'oxweight = '.round(str_replace(',', '.', $key[12]),2).' , '.
				'oxean = \''.($key[13]? $key[13] : '').'\' , '.
				'Verpackungseinheit = \''.($key[14]? $key[14]:null).'\' , '.
				'preisneua = ' . ( $key[15] ? round(str_replace(',', '.', $key[15]),2) : 0).' , '.
				'preisneub = ' . ( $key[16] ? round(str_replace(',', '.', $key[16]),2) : 0).' , '.
				'preisneuc = ' . ( $key[17] ? round(str_replace(',', '.', $key[17]),2) : 0).' , '.
				'preisneud = ' . ( $key[18] ? round(str_replace(',', '.', $key[18]),2) : 0).' , '.
				'preisneue = ' . ( $key[19] ? round(str_replace(',', '.', $key[19]),2) : 0).' '.
				'oxpricef' = round(str_replace(',', '.', $key[20]),2).
				' where oxid = \''.$key[0].'\'';
			
			
			echo $zähler.' ' ;
			if($funktion != 'nix') {
				try {
					$oDb->execute($sql);
				} catch (Exception $e) {
					echo 'ERROR: Artikel ';
					print_r($e);
					die();
				}
				print_r($sql);
				echo '<br>';
			} else {
				print_r($sql);
				echo '<br>';
			}
			
			
			$zähler++;
			/*
			if($zähler > 90){
				die();
			}
			*/
			/*
			if($funktion != 'test'){
				if($oDb->execute($sql)){
					$erg = 'ok';
				} else {
					$erg = 'error';
				}
			}
			
			if(isset($_GET['debug']) && $_GET['debug'] == 1) {
				echo $sql;
			}
			
			if($erg == 'error'){
				die($sql);
			}
			
			/*
			// ausgabe der Query's
			if(isset($_GET['debug']) && $_GET['debug'] == 1) {
				echo 'SUCCESSED: update oxarticles set ' .
					'oxtprice = ' . round(str_replace(',', '.', $key[1]), 2) . ' , ' .
					'oxactive = ' . ($key[2] == 'Ja' ? 1 : 0) . ' , ' .
					'oxstockflag = 1 , ' .
					'bnflagbestand = ' . ($key[3]? $key[3]:0) . ' , ' .
					'oxdelivery = \'' . ($key[4] > 0 ? date('Y-m-d', strtotime($key[4])) : null) . '\' , ' .
					'oxmindeltime = \'' . (isset($key[5]) ? $key[5] : null) . '\' , ' .
					'oxmaxdeltime = \'' . (isset($key[6]) ? $key[6] : null) . '\' , ' .
					'oxpricea = ' . round(str_replace(',', '.', $key[7]), 2) . ' , ' .
					'oxpriceb = ' . round(str_replace(',', '.', $key[8]), 2) . ' , ' .
					'oxpricec = ' . round(str_replace(',', '.', $key[9]), 2) . ' , ' .
					'oxpriced = ' . round(str_replace(',', '.', $key[10]), 2) . ' , ' .
					'oxpricee = ' . round(str_replace(',', '.', $key[11]), 2) . ' , ' .
					'oxweight = ' . round(str_replace(',', '.', $key[12]), 2) . ' , ' .
					'oxean = '.($key[13]?$key[13]:null).' , '.
					'verpackungseinheit = \''.($key[14]? $key[14]:null).'\''.
					' where oxid = \'' . $key[0] . '\' <br>';
			}
			*/
			#echo $key[0] . "\t" . $key[1] . "<br>";
			#$rs[] = $oDb->select('SELECT oxid, oxartnum, oxstock from oxarticles WHERE oxartnum = \''. $key[0].'\'');
			#$oDb->execute( 'update oxarticles set oxarticles.oxstock = '.$key[1].' where oxarticles.oxartnum = \''.$key[0].'\'' );
		}
		// schließen der Datei.
		fclose($fp);
}

