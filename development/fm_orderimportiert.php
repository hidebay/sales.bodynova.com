<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 16.08.18
 * Time: 11:09
 */

ini_set('display_errors', true);
ini_set('html_errors'    , true);
//echo dirname(__DIR__).'/bootstrap.php';
require dirname(__DIR__).'/bootstrap.php';
$arrReturn = array();

if(empty($_GET['ordernr'])) {
	$arrReturn['error'] = "Keine Order Nr übergeben!";
	die(json_encode($arrReturn));
}

if(empty($_GET['fnc'])){
	$arrReturn['error'] = "Keinen Funktionsaufruf";
	die(json_encode($arrReturn));
}

if($_GET['fnc'] === 'getOxid'){
	$sOxOrderNr = $_GET['ordernr'];
	$arrReturn['OXID'] =  getOxidByOrderNr($sOxOrderNr);
}

if($_GET['fnc'] === 'ordernachimport'){
	$sOxOrderNr = $_GET['ordernr'];
	$oxid = getOxidByOrderNr($sOxOrderNr);
	$arrReturn = OrderNachimport($oxid);
	die(json_encode($arrReturn));
}


if($_GET['fnc'] === 'main'){
	$sOxOrderNr = $_GET['ordernr'];
	$orderOxid =  getOxidByOrderNr($sOxOrderNr);
	$oOrder = oxNew('oxorder');
	$oOrder->load($orderOxid);
	
	if(!empty($_GET['rechnungsnr'])) {
		$sRechnungsNr = $_GET['rechnungsnr'];
		$oOrder->oxorder__flagfilemakerrechnungnr = new oxField($sRechnungsNr, oxField::T_TEXT);
		$oOrder->save();
		//debug_to_console($oOrder);
	}
	
	$arrReturn['oxfolder']      = $oOrder->oxorder__oxfolder->value;
	$arrReturn['OXID']          = $oOrder->oxorder__oxid->value;
	$arrReturn['OXORDERFOLDER'] = $oOrder->oxorder__oxfolder->value;
	$arrReturn['OXSENDDATE']    = $oOrder->oxorder__oxsenddate->value;
	$arrReturn['OXSTATUS']      = "NOT SET";
}


if($_GET['debug'] === '1') {
	echo '<pre>';
	echo $oOrder->oxorder__oxfolder->value;
	print_r($oOrder);
	echo '</pre>';
} else {
	die(json_encode($arrReturn));
}


function getOxidByOrderNr($sOrderNr){
	try{
		$oDb = oxDb::getDb();
	} catch (Exception $e){
		return $e;
	}
	
	return $oDb->getOne('select oxid from oxorder where oxordernr = ' . $oDb->quote($sOrderNr), false, false);
}

/*
if( ini_get('allow_url_fopen') ) {
	echo "ON";
}
*/

function debug_to_console( $data ) {
	
	$output = '';
	
	if ( is_array( $data ) ) {
		$output .= "<script>console.warn( 'Debug Objects with Array.' ); console.log( '" . implode( ',', $data) . "' );</script>";
	} else if ( is_object( $data ) ) {
		$data    = var_export( $data, TRUE );
		$data    = explode( "\n", $data );
		foreach( $data as $line ) {
			if ( trim( $line ) ) {
				$line    = addslashes( $line );
				$output .= "console.log( '{$line}' );";
			}
		}
		$output = "<script>console.warn( 'Debug Objects with Object.' ); $output</script>";
	} else {
		$output .= "<script>console.log( 'Debug Objects: {$data}' );</script>";
	}
	
	echo $output;
}

function OrderNachimport($oxid = null){
	if($oxid == null){
		return false;
	}
	try {
		
		$order = oxNew('oxorder');
		
		$order->load($oxid);
		$order->setOrderArray($order);
		$order->setCustNr();
		$order->setUserWhitelabel();
		$order->setBillCountry();
		$order->setDeliveryCountry();
		$order->setPayments();
		$order->setVouchers();
		$order->erstelleOrderXmlDatei();
		$order->ftpUploadFritzBox();
		$order->erstelleOrderXmlDatei();
		$order->ftpUploadFritzBox();
		
		$orderArticles = new orderarticles2xml();
		$orderArticles->setOrderId($order->oxorder__oxid->value);
		$orderArticles->setOrderNr($order->oxorder__oxordernr->value);
		$orderArticles->setOrderArticleIds();
		$orderArticles->setOrderArticles();
		$orderArticles->setOrderArticleXmlArray();
		$orderArticles->erstelleOrderArticlesXmlDatei();
		$orderArticles->ftpUploadFritzBox();
	}catch(Exeption $e){
		print_r($e);
	}
	
	return $order;
	
}

/*

if (!$sOxOrderNr) {
	return false;
}

$oDb = oxDb::getDb();

if ($oDb->getOne('select oxid from oxorder where oxordernr = ' . $oDb->quote($sOxId), false, false)) {
	return true;
}

*/
