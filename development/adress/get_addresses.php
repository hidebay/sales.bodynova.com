<?php

	if(is_array($_REQUEST['address'])){
		$_REQUEST['address'] = $_REQUEST['address'][0];
	}
	$json = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=de&address='.urlencode($_REQUEST['address'])));
	
	if(!empty($json->results)){
	echo "<ul>";
		foreach($json->results as $value){
			echo "<li>".highlightSearchString($value->formatted_address, $_REQUEST['address'])."</li>";
		}
	}
	echo "</ul>";
	
	function highlightSearchString($value, $searchString){
		$regex_chars = '\.+?(){}[]^$';
		for ($i=0; $i<strlen($regex_chars); $i++) {
			$char = substr($regex_chars, $i, 1);
			$searchString = str_replace($char, '\\'.$char, $searchString);
		}
		$searchString = '/(.*)('.$searchString.')(.*)/i';
		return preg_replace($searchString, '\1<b class="highlighted">\2</b>\3', $value);
	}

?>