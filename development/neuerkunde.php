<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 29.08.17
 * Time: 12:13
 */
require_once dirname(__DIR__).'/bootstrap.php';

if(isset($_POST['oxcustnr'])){
	
	$oDb = oxDb::getDb();
	$sQ = "select oxid from oxuser where oxcustnr = ".$oDb->quote($_POST['oxcustnr'])." or oxusername = ".$oDb->quote($_POST['oxusername']);
	
	if ($sUserId = $oDb->getOne($sQ)) {
		
		$oUser = oxNew('oxUser');
		$oUser->load($sUserId);
		
		echo 'ERROR:';
		echo 'Kunde bereits vorhanden<br>';
		echo 'OXID: '.$sUserId;
		
		die();
		
		
	} else {
		
		#echo '<pre>';
		#echo 'kein KD vorhanden: ';
		$oUser = oxNew('oxUser');
		$oUser->oxuser__oxid = new oxField(oxUtilsObject::getInstance()->generateUID(), oxField::T_RAW);
		$oUser->oxuser__oxactive = new oxField(1, oxField::T_RAW);
		$oUser->oxuser__oxrights = new oxField('user', oxField::T_RAW);
		$oUser->oxuser__oxshopid = new oxField('oxbaseshop', oxField::T_RAW);
		$oUser->oxuser__oxusername = new oxField($_POST['oxusername'], oxField::T_RAW);
		$oUser->oxuser__oxcustnr = new oxField($_POST['oxcustnr'], oxField::T_RAW);
		
		if(isset($_POST['oxcompany'])){
			$oUser->oxuser__oxcompany = new oxField($_POST['oxcompany'], oxField::T_RAW);
		}
		if(isset($_POST['oxfname'])){
			$oUser->oxuser__oxfname = new oxField($_POST['oxfname'], oxField::T_RAW);
		}
		if(isset($_POST['oxlname'])){
			$oUser->oxuser__oxlname = new oxField($_POST['oxlname'], oxField::T_RAW);
		}
		if(isset($_POST['oxstreet'])){
			$oUser->oxuser__oxstreet = new oxField($_POST['oxstreet'], oxField::T_RAW);
		}
		if(isset($_POST['oxstreetnr'])){
			$oUser->oxuser__oxstreetnr = new oxField($_POST['oxstreetnr'], oxField::T_RAW);
		}
		if(isset($_POST['oxcity'])){
			$oUser->oxuser__oxcity = new oxField($_POST['oxcity'], oxField::T_RAW);
		}
		if(isset($_POST['oxcountry'])){
			$countryid = $oUser->getUserCountryId($_POST['oxcountry']);
			$oUser->oxuser__oxcountryid = new oxField($countryid, oxField::T_RAW);
		}
		if(isset($_POST['oxzip'])){
			$oUser->oxuser__oxzip = new oxField($_POST['oxzip'], oxField::T_RAW);
		}
		if(isset($_POST['oxfon'])){
			$oUser->oxuser__oxfon = new oxField($_POST['oxfon'], oxField::T_RAW);
		}
		
		$oUser->save();
		
		/*
		oxusername
		*/
		
		print_r($oUser->oxuser__oxid->value);
		
		/*
		$oxid = $oUser->setId(); // oxUtilsObject::getInstance()->generateUID();
		*/
		
		die();
	}
	
	
	
	/*
	$oDb = oxDb::getDb();
	$sQ = 'update oxorder set iswhitelabel=' . $oDb->quote($this->getWhitelabel()) . ' where oxid=' . $oDb->quote($this->getId());
	$oDb->execute($sQ);
	*/
	
	
	/*
	echo '<pre>';
	echo 'kein KD vorhanden: ';
	print_r($oUser);
	*/
	
} else {
	echo 'ERROR:';
	echo 'keine Parameter übergeben.';

}
