<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Adress Vorschlag</title>
	<style>
		html, body {
			height: 100%;
			margin: 0;
			padding: 0;
		}
		#map {
			height: 100%;
		}
		#right-panel {
			font-family: 'Roboto','sans-serif';
			line-height: 30px;
			padding-left: 10px;
		}
		
		#right-panel select, #right-panel input {
			font-size: 15px;
		}
		
		#right-panel select {
			width: 100%;
		}
		
		#right-panel i {
			font-size: 12px;
		}
	
	</style>
</head>
<body>
<div id="right-panel">
	<input id="question" value="<?php echo $_GET['query']?>" />
	<ul id="results"></ul>
</div>
<script>
	// This example retrieves autocomplete predictions programmatically from the
	// autocomplete service, and displays them as an HTML list.
	
	function initService() {
		
		var placeSearch, autocomplete;
		var componentForm = {
			street_number                   : 'short_name',
			route                           : 'long_name',
			locality                        : 'long_name',
			administrative_area_level_1     : 'short_name',
			country                         : 'long_name',
			postal_code                     : 'short_name'
		};
		
		
		autocomplete = new google.maps.places.Autocomplete(
			/** @type {!HTMLInputElement} */
			((document.getElementById('question'))) , {types: ['geocode']}
		);
		autocomplete.addListener('place_changed', fillInAddress);
		
		
		
		function fillInAddress() {
			// Get the place details from the autocomplete object.
			var place = autocomplete.getPlace();
			console.log(place);
			/*for (var question in componentForm) {
				document.getElementById(question).value = '';
				document.getElementById(question).disabled = false;
				// console.log('Component: ' , component);
			}*/
			
			console.log('Place: ', place);
			
			var span = document.createElement('span');
			span.appendChild(document.createTextNode(place.address_components[0]['types'] + ' ' + place.address_components[0]['long_name']));
			span.appendChild(document.createTextNode(place.address_components[1]['types'] + ' ' + place.address_components[1]['long_name']));
			span.appendChild(document.createTextNode(place.address_components[2]['types'] + ' ' + place.address_components[2]['long_name']));
			span.appendChild(document.createTextNode(place.address_components[3]['types'] + ' ' + place.address_components[3]['long_name']));
			span.appendChild(document.createTextNode(place.address_components[4]['types'] + ' ' + place.address_components[4]['long_name']));
			span.appendChild(document.createTextNode(place.address_components[5]['types'] + ' ' + place.address_components[5]['long_name']));
			span.appendChild(document.createTextNode(place.address_components[6]['types'] + ' ' + place.address_components[6]['long_name']));
			span.appendChild(document.createTextNode(place.address_components[7]['types'] + ' ' + place.address_components[7]['long_name']));
			
			document.getElementById('results').appendChild(span);
			
			// Get each component of the address from the place details
			// and fill the corresponding field on the form.
			for (var i = 0; i < place.address_components.length; i++) {
				var addressType = place.address_components[i].types[0];
				if (componentForm[addressType]) {
					var val = place.address_components[i][componentForm[addressType]];
					document.getElementById(addressType).value = val;
				}
			}
		}
		
		var displaySuggestions = function(predictions, status) {
			/*
			if (status != google.maps.places.PlacesServiceStatus.OK) {
				alert(status);
				return;
			}*/
			
			predictions.forEach(function(prediction) {
				var li = document.createElement('li');
				li.appendChild(document.createTextNode(prediction.description));
				document.getElementById('results').appendChild(li);
			});
			
			console.log(predictions);
			
		};
		
		var service = new google.maps.places.AutocompleteService();
		service.getQueryPredictions({ input: '<?php echo $_GET['query']; ?>' }, displaySuggestions);
		
		function geolocate() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					var circle = new google.maps.Circle({
						center: geolocation,
						radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
				});
				// console.log('GEO: '.navigator.geolocation);
			}
		}
	}

</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsZDQwsy3uf1Atoc-bD2V0_pG-H4ZkqLk&signed_in=true&libraries=places&callback=initService"
        async defer></script>
</body>
</html>
