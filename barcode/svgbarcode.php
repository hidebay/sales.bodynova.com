<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 09.05.18
 * Time: 16:20
 */
require_once 'vendor/picqer/php-barcode-generator/src/BarcodeGenerator.php';
require_once 'vendor/picqer/php-barcode-generator/src/BarcodeGeneratorSVG.php';

ini_set('auto_detect_line_endings', true);
ini_set('error_reporting', E_ALL);
ini_set('log_errors', true);
ini_set('display_errors', true);
ini_set('error_log', __DIR__.'/log/psErrorLog_PHP.log');

$generatorSVG = new Picqer\Barcode\BarcodeGeneratorSVG();

$barcode = $_GET['text'];

$code = $generatorSVG->getBarcode($barcode, $generatorSVG::TYPE_CODE_128);

echo '<pre>';
print_r($barcode);

print_r($code);
echo '</pre><br/>';

//echo $generatorSVG->getBarcode($_GET['text'], $generatorSVG::TYPE_CODE_128);

echo '<img src="data:image/svg+xml;base64,' . base64_encode($code) . '" width="600" height="350" />';

