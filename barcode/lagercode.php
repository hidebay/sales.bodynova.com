<?php
// Including all required classes
require_once('class/BCGFontFile.php');
require_once('class/BCGColor.php');
require_once('class/BCGDrawing.php');

// Including the barcode technology
require_once('class/BCGcode39.barcode.php');
#require_once('class/BCGean13.barcode.php');

// Loading Font
$font = new BCGFontFile('./font/Arial.ttf', 72);

// Don't forget to sanitize user inputs
$text = isset($_GET['text']) ? $_GET['text'] : 'HELLO';

// The arguments are R, G, B for color.
$color_black = new BCGColor(0, 0, 0);
$color_white = new BCGColor(255, 255, 255);

$drawException = null;
try {
	$code = new BCGcode39();
	#$code = new BCGean13();
	$code->setScale(8); // Resolution
	$code->setThickness(30); // Thickness
	$code->setForegroundColor($color_black); // Color of bars
	$code->setBackgroundColor($color_white); // Color of spaces
	$code->setFont($font); // Font (or 0)
	$code->parse($text); // Text
} catch(Exception $exception) {
	$drawException = $exception;
}

/* Here is the list of the arguments
1 - Filename (empty : display on screen)
2 - Background color */
$drawing = new BCGDrawing('images/'.$_GET['text'].'.png', $color_white);
if($drawException) {
	$drawing->drawException($drawException);
} else {
	$drawing->setBarcode($code);
	$drawing->setDPI(300);
	//$drawing->setQuality(100);
	$drawing->draw();
}

// Header that says it is an image (remove it if you save the barcode to a file)
//header('Content-Type: image/png');
//header('Content-Disposition: inline; filename="barcode.png"');

// Draw (or save) the image into PNG format.
$drawing->finish(BCGDrawing::IMG_FORMAT_PNG, 100);

// FTP Upload auf fritz.nas
$ftp_server= '92.50.75.170';
$ftp_user= 'haendlershop';
$ftp_pass= 'peanuts30';
#$file = dirname(__FILE__).'/'.barcode_outimage($code,getvar('encoding'),getvar('scale'),getvar('mode'));
#$file = $bild;
$file = 'images/'.$_GET['text'].'.png';
$destination_file = 'JetFlash-Transcend16GB-01/FRITZ/barcodes/lagercode/'.$_GET['text'].'.png';
$cid=ftp_connect($ftp_server);
ftp_login($cid, $ftp_user, $ftp_pass);
ftp_pasv($cid, true);
ftp_put($cid, $destination_file, $file, FTP_BINARY);
ftp_close($cid);

?>
