<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law - it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 */

/**
 * Theme Information
 */
$aTheme = array(
    'id'           => 'flow',
    'title'        => 'Flow',
    'description'  => 'Flow is OXID`s official responsive theme based on the CSS framework Bootstrap 3.',
    'thumbnail'    => 'theme.jpg',
    'version'      => '1.0.0-beta',
    'author'       => '<a href="http://www.oxid-esales.com" title="OXID eSales AG">OXID eSales AG</a>',
);
