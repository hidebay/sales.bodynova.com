<form action="[{ $oViewConf->getSelfActionLink() }]" name="newsletter" method="post" class="form-inline">


            [{ $oViewConf->getHiddenSid() }]
            [{ $oViewConf->getNavFormParams() }]
            <input type="hidden" name="fnc" value="subscribe">
            <input type="hidden" name="cl" value="account_newsletter">
		<div class="form-group">
	        <label for="status">[{oxmultilang ident="NEWSLETTER_SUBSCRIPTION" suffix="COLON" }]</label>
	        <select name="status" id="status" class="form-control">
	            <option value="1" [{if $oView->isNewsletter() }]selected[{/if}] >[{oxmultilang ident="YES" }]</option>
	            <option value="0" [{if !$oView->isNewsletter() }]selected[{/if}] >[{oxmultilang ident="NO" }]</option>
	        </select>
			<button id="newsletterSettingsSave" type="submit" class="btn btn-default btn-success submitButton">
				<span class="glyphicon glyphicon-ok"></span>[{*oxmultilang ident="SAVE" *}]
			</button>

		</div>
             [{if $oView->isNewsletter() == 2}]
	            <div class="info">
	                [{oxmultilang ident="MESSAGE_SENT_CONFIRMATION_EMAIL" }]
	            </div>
            [{/if}]
            <span class="notice help-block">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION" }]</span>
</form>
