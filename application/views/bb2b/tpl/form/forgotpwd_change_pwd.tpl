[{oxscript include="js/widgets/oxinputvalidator.js" priority=10 }]
[{oxscript add="$('form.js-oxValidate').oxInputValidator();"}]
 <form class="js-oxValidate form-horizontal" action="[{ $oViewConf->getSelfActionLink() }]" name="forgotpwd" method="post">
      [{assign var="aErrors" value=$oView->getFieldValidationErrors()}]
      [{ $oViewConf->getHiddenSid() }]
      [{ $oViewConf->getNavFormParams() }]
      <input type="hidden" name="fnc" value="updatePassword">
      <input type="hidden" name="uid" value="[{ $oView->getUpdateId() }]">
      <input type="hidden" name="cl" value="forgotpwd">
      <input type="hidden" id="passwordLength" value="[{$oViewConf->getPasswordLength()}]">
      <div class="form clear form-horizontal">
            <div class="form-group [{if $aErrors.oxuser__oxpassword}]oxInValid[{/if}]">
                <label class="control-label col-sm-2">[{ oxmultilang ident="NEW_PASSWORD" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input type="password" name="password_new" class="form-control js-oxValidate js-oxValidate_notEmpty js-oxValidate_length js-oxValidate_match textbox">
                    <p class="oxValidateError">
                        <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                        <span class="js-oxError_length">[{ oxmultilang ident="ERROR_MESSAGE_PASSWORD_TOO_SHORT" }]</span>
                        <span class="js-oxError_match">[{ oxmultilang ident="ERROR_MESSAGE_USER_PWDDONTMATCH" }]</span>
                        [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxpassword}]
                    </p>
                </div>
            </div>
            <div class="form-group [{if $aErrors.oxuser__oxpassword}]oxInValid[{/if}]">
                <label class="control-label col-sm-2">[{ oxmultilang ident="CONFIRM_PASSWORD" suffix="COLON" }]</label>
                <div class="col-sm-10">
                    <input type="password" name="password_new_confirm" class="form-control js-oxValidate js-oxValidate_notEmpty js-oxValidate_length js-oxValidate_match textbox">
                    <p class="oxValidateError">
                        <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                        <span class="js-oxError_length">[{ oxmultilang ident="ERROR_MESSAGE_PASSWORD_TOO_SHORT" }]</span>
                        <span class="js-oxError_match">[{ oxmultilang ident="ERROR_MESSAGE_USER_PWDDONTMATCH" }]</span>
                        [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxpassword}]
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"></label>
                <div class="col-sm-10">
                    <button class="submitButton btn btn-success btn-large" type="submit" name="save" value="[{ oxmultilang ident="CHANGE_PASSWORD" }]">[{ oxmultilang ident="CHANGE_PASSWORD" }]</button>
                </div>
            </div>
      </div>
</form>
