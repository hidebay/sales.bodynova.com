[{capture append="oxidBlock_content"}]
	[{assign var="template_title" value="CHANGE_PASSWORD"|oxmultilangassign }]
	<div class="panel panel-default">
		<div class="panel-heading">
			<h1 class="panel-title [{*if $oView->isPasswordChanged() }] hide[{/if*}]">
				[{oxmultilang ident="CHANGE_PASSWORD"}]
			</h1>
			<div class="pull-right" style="margin-top: -27px;">
				[{if $oView->getClassName() eq "account_password" && !$blHideBreadcrumb}]
					[{include file="widget/breadcrumb.tpl"}]
				[{/if}]
			</div>
		</div>
		<div class="panel-body">
			[{* Response Message Success: *}]
			[{if $oView->isPasswordChanged() }]
				<div class="status success corners alert alert-success" role="alert">
					[{ oxmultilang ident="MESSAGE_PASSWORD_CHANGED" }]
					<script>
						window.setTimeout('location.href="index.php"', 2000);
					</script>
				</div>
			[{/if}]
			[{* Response Message Error: *}]
			[{if count($Errors) > 0 && count($Errors.user) > 0}]
				<div class="status error corners">
					[{foreach from=$Errors.user item=oEr key=key }]
						<p>[{ $oEr->getOxMessage()}]</p>
					[{/foreach}]
				</div>
			[{/if}]

			[{* Passwort Formular: *}]
			[{include file="form/user_password.tpl"}]
			[{insert name="oxid_tracker" title=$template_title }]
		</div>
	</div>
[{/capture}]

[{* Sidebar Menue: *}]
[{capture append="oxidBlock_sidebar"}]
	[{include file="page/account/inc/account_menu.tpl" active_link="password"}]
[{/capture}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 }]
