[{block name="checkout_steps_main"}]
    <div class="wizard">
        <div class="wizard-inner">
		    <div class="connecting-line"></div>
			<ul class="nav nav-tabs" role="tablist">
				[{if $oxcmp_basket->getProductsCount()}]
					[{assign var=showStepLinks value=true}]
				[{/if}]
				[{block name="checkout_steps_basket"}]
					<li class="step1[{if $active == 1}] active [{elseif $active > 1}] passed [{else}] disabled [{/if}]" >
						<a href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getBasketLink()}][{else}]#[{/if}]"  aria-controls="step1" role="tab" title="[{oxmultilang ident="STEPS_BASKET"}]">
						<!--<a rel="nofollow" href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getBasketLink()}][{else}]#[{/if}]">-->
							[{*oxmultilang ident="STEPS_BASKET"*}]
							<span class="round-tab">
								<i class="glyphicon glyphicon-shopping-cart"></i>
							</span>
						</a>
					</li>
				[{/block}]

				[{assign var=showStepLinks value=false}]
				[{if !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
					[{assign var=showStepLinks value=true}]
				[{/if}]

				[{block name="checkout_steps_send"}]
					<li class="step2[{if $active == 2}] active [{elseif $active > 2}] passed [{else}] disabled [{/if}]" role="presentation">
						<a rel="nofollow" href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getOrderLink()}][{else}]#[{/if}]" aria-controls="step2" role="tab" title="[{oxmultilang ident="STEPS_SEND"}]">
							<span class="round-tab">
								<i class="glyphicon glyphicon-pencil"></i>
							</span>
						</a>
					</li>
				[{/block}]

				[{assign var=showStepLinks value=false}]
				[{if $active != 1 && $oxcmp_user && !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
					[{assign var=showStepLinks value=true}]
				[{/if}]

				[{*
				[{block name="checkout_steps_pay"}]
					<li class="step3[{if $active == 3}] active [{elseif $active > 3}] passed [{/if}]">
						<a rel="nofollow" [{if $oViewConf->getActiveClassName() == "user"}]id="paymentStep"[{/if}] href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getPaymentLink()}][{else}]#[{/if}]">
							[{oxmultilang ident="STEPS_PAY"}]
						</a>
					</li>
					[{oxscript add="$('#paymentStep').click( function() { $('#userNextStepBottom').click();return false;});"}]
				[{/block}]

				$oView->getPaymentList() &&
				*}]

				[{assign var=showStepLinks value=false}]
				[{if $active != 1 && $oxcmp_user && $oxcmp_basket->getProductsCount() && !$oView->isLowOrderPrice()}]
					[{assign var=showStepLinks value=true}]
				[{/if}]

				[{block name="checkout_steps_order"}]
					<li class="step4[{if $active == 4}] active [{elseif $active > 4}] passed [{else}] disabled [{/if}]" role="presentation">
						<a rel="nofollow" [{if $oViewConf->getActiveClassName() == "payment"}]id="orderStep"[{/if}] href="[{if $showStepLinks}][{if $oViewConf->getActiveClassName() == "payment"}]javascript:document.forms.order.submit();[{else}][{oxgetseourl ident=$oViewConf->getOrderConfirmLink()}][{/if}][{else}]#[{/if}]"  aria-controls="step4" role="tab" title="[{oxmultilang ident="STEPS_ORDER"}]">
							<span class="round-tab">
								<i class="glyphicon glyphicon-search"></i>
							</span>
						</a>
					</li>
					[{oxscript add="$('#orderStep').click( function() { $('#paymentNextStepBottom').click();return false;});"}]
				[{/block}]

				[{block name="checkout_steps_laststep"}]
					<li class="step5[{if $active == 5}] activeLast [{else}] defaultLast disabled[{/if}] " role="presentation">
						<a href="#"  aria-controls="step5" role="tab" title="[{oxmultilang ident="READY"}]">
							<span class="round-tab">
								<i class="glyphicon glyphicon-ok"></i>
							</span>
						</a>
					</li>
				[{/block}]
			</ul>
		</div>
    </div>
[{/block}]
