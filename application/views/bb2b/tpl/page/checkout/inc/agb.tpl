<div>
    [{if $oView->isActive('PsLogin') || !$oView->isConfirmAGBActive()}]
        <input type="hidden" name="ord_agb" value="1">
    [{else}]
        <input type="hidden" name="ord_agb" value="0">
    [{/if}]
    <input type="hidden" name="oxdownloadableproductsagreement" value="0">
    <input type="hidden" name="oxserviceproductsagreement" value="0">

    [{if !$hideButtons}]

        [{if !$oView->isActive('PsLogin')}]
            [{if $oView->isConfirmAGBActive()}]
                [{oxifcontent ident="oxrighttocancellegend" object="oContent"}]
					<div class="form-group">
		                <label style="margin-top:0;" class="col-sm-2 control-label norm" for="checkAgbTop">[{$oContent->oxcontents__oxtitle->value}]:</label>
						<div class="col-sm-10">
	                        <input id="checkAgbTop" class="checkbox" type="checkbox" name="ord_agb" value="1">
							<span class="help-block">[{$oContent->oxcontents__oxcontent->value}]</span>
	                    </div>
					</div>
                [{/oxifcontent}]
            [{else}]
                [{oxifcontent ident="oxrighttocancellegend2" object="oContent"}]
					<div class="form-group">
		                <label style="margin-top:0;" class="col-sm-2 control-label norm" for="checkAgbTop">[{$oContent->oxcontents__oxtitle->value}]:</label>
		                <div class="col-sm-10">
			                <input id="checkAgbTop" class="checkbox" type="checkbox" name="ord_agb" value="1">
			                <span class="help-block">[{$oContent->oxcontents__oxcontent->value}]</span>
		                </div>
                    </div>
                [{/oxifcontent}]
            [{/if}]
        [{/if}]

        [{if $oViewConf->isFunctionalityEnabled('blEnableIntangibleProdAgreement') }]
            [{assign var="oExplanationMarks" value=$oView->getBasketContentMarkGenerator() }]
            [{if $oxcmp_basket->hasArticlesWithDownloadableAgreement() }]
                [{oxifcontent ident="oxdownloadableproductsagreement" object="oContent"}]
					<div class="form-group">
		                <label style="margin-top: 10px;" class="col-sm-2 control-label norm" for="oxdownloadableproductsagreement">[{$oExplanationMarks->getMark('downloadable')}] [{$oContent->oxcontents__oxcontent->value}]</label>
						<div class="col-sm-10">
	                        <input id="oxdownloadableproductsagreement" class="checkbox" type="checkbox" name="oxdownloadableproductsagreement" value="1">
	                    </div>
					</div>
                [{/oxifcontent}]
            [{/if}]

            [{if $oxcmp_basket->hasArticlesWithIntangibleAgreement() }]
                [{oxifcontent ident="oxserviceproductsagreement" object="oContent"}]
					<div class="form-group">
						<label style="margin-top: 10px;" class="col-sm-2 control-label norm" for="oxserviceproductsagreement">[{$oExplanationMarks->getMark('intangible')}] [{$oContent->oxcontents__oxcontent->value}]</label>
						<div class="col-sm-10">
							<input id="oxserviceproductsagreement" class="checkbox" type="checkbox" name="oxserviceproductsagreement" value="1">
						</div>
					</div>
                [{/oxifcontent}]
            [{/if}]

        [{/if}]
    [{/if}]
</div>
[{oxscript add="$('#checkAgbTop').click(function(){ $('input[name=ord_agb]').val(parseInt($('input[name=ord_agb]').val())^1);});"}]
[{oxscript add="$('#oxdownloadableproductsagreement').click(function(){ $('input[name=oxdownloadableproductsagreement]').val(parseInt($('input[name=oxdownloadableproductsagreement]').val())^1);});"}]
[{oxscript add="$('#oxserviceproductsagreement').click(function(){ $('input[name=oxserviceproductsagreement]').val(parseInt($('input[name=oxserviceproductsagreement]').val())^1);});"}]
