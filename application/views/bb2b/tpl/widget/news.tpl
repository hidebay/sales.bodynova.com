[{*assign var="oNews" value=$oView->getNews() *}]
[{assign var="news" value=$oNews->getlastNews()}]
[{assign var="newsid" value=$news[0]}]
[{assign var="newstitel" value=$news[1]}]
[{assign var="newstext" value=$news[2]}]
[{assign var="newsdate" value=$news[3]}]
<div class="panel panel-default" style="margin-top: 15px;" id="newsPanel">
    <div class="panel-heading">
        <h3 class="panel-title">[{oxmultilang ident="NEWS"}]</h3>
    </div>
    <div class="panel-body">

        <div class="starttext">
            <div class="newsbox">
	            [{* alte News darstellung }]
                <b>[{$newstitel}]&nbsp;&nbsp;&nbsp;<small></small></b>
                [{$newsdate|date_format:"%d.%m.%Y"}] &nbsp; [{$newstext}]
                <div class="">
                    <a class='' data-target='#newsboxmodal' data-toggle='modal' type='button' style="cursor: pointer;">[{oxmultilang ident="ALLENEWSZEIGEN"}]</a>
                </div>
                [{*}]
	            [{* neue News darstellung *}]
	            [{foreach from=$oNews item=_oNewsItem name=_sNewsList }]
	                <b>[{$_oNewsItem->oxnews__oxshortdesc->value}]&nbsp;&nbsp;&nbsp;<small>[{$_oNewsItem->oxnews__oxdate->value|date_format:"%d.%m.%Y"}]</small></b>
		            <div id="newsBox" class="newsbox">
			            [{$_oNewsItem->getLongDesc()}]
		            </div>
	                <hr/>
	            [{/foreach}]
	            [{* /neue News darstellung *}]
            </div>
        </div>
    </div>
</div>
[{*$oNews|var_dump*}]
[{*debug*}]
[{*}]
<div id="newsboxmodal" class="modal fade" aria-labelledby="TATA" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">[{oxmultilang ident="ALLENEWS"}]</h4>
            </div>
            <div class="modal-body">
                <p>
                    [{foreach from=$oNews item=_oNewsItem name=_sNewsList }]
                            <h5>[{$_oNewsItem->oxnews__oxshortdesc->value}] <small>[{$_oNewsItem->oxnews__oxdate->value}]</small></h5>
                            <div id="newsBox" class="newsbox">
                                [{$_oNewsItem->getLongDesc()}]<br><br>
                            </div>
                            <hr>
                    [{/foreach}]
                </p>
            </div>
            <!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
[{*}]
