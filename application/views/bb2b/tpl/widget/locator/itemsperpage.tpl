[{assign var="_additionalParams" value=$oView->getAdditionalParams()}]

<div id="itemsPerPage" class="dropdown">
	<button type="button" class="btn btn-default" data-toggle="dropdown">
		[{oxmultilang ident="PRODUCTS_PER_PAGE" suffix="COLON" }]
		[{if $oViewConf->getArtPerPageCount() }]
			[{ $oViewConf->getArtPerPageCount() }]
		[{else}]
			[{oxmultilang ident="CHOOSE"}]
		[{/if}]
	</button>
	<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
		[{foreach from=$oViewConf->getNrOfCatArticles() item=iItemsPerPage}]
			<li><a href="[{ $oView->getLink()|oxaddparams:"ldtype=$listType&amp;_artperpage=$iItemsPerPage&amp;pgNr=0&amp;$_additionalParams"}]" rel="nofollow" [{if $oViewConf->getArtPerPageCount() == $iItemsPerPage }] class="selected"[{/if}]>[{$iItemsPerPage}]</a></li>
		[{/foreach}]
	</ul>
</div>