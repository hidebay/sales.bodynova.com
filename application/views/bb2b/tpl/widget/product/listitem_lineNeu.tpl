[{block name="widget_product_listitem_line"}]
	[{assign var="product"              value=$oView->getProduct()                          }]
	[{assign var="owishid"              value=$oView->getWishId()                           }]
	[{assign var="removeFunction"       value=$oView->getRemoveFunction()                   }]
	[{assign var="recommid"             value=$oView->getRecommId()                         }]
	[{assign var="iIndex"               value=$oView->getIndex()                            }]
	[{assign var="showMainLink"         value=$oView->getShowMainLink()                     }]
	[{assign var="blDisableToCart"      value=$oView->getDisableToCart()                    }]
	[{assign var="toBasketFunction"     value=$oView->getToBasketFunction()                 }]
	[{assign var="altproduct"           value=$oView->getAltProduct()                       }]
	[{assign var="aVariantSelections"   value=$product->getVariantSelections(null,null,1)   }]

	[{if $showMainLink}]
		[{assign var='_productLink' value=$product->getMainLink()}]
	[{else}]
		[{assign var='_productLink' value=$product->getLink()}]
	[{/if}]
	<tr id="article_[{$product->oxarticles__oxid|replace:".":"_"}]" class="tabellenspalte">
[{* Artikelbild *}]
		<td class="hidden-xs imagecolumn">
			<a name="[{$product->getID()|replace:".":"_"}]"></a>
			<div class="pictureBox">
				<a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" class="viewAllHover glowShadow corners" title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
					[{*$product->getThumbnailUrl()*}]
					[{if $product->oxarticles__oxpic1 != ''}]
						<img class="lazy-img img-thumbnail" src="" data-src="[{$product->getThumbnailUrl()}]" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
					[{else}]
						<img class="lazy-img img-thumbnail" src="" data-src="https://bodynova.de/out/imagehandler.php?artnum=[{$product->oxarticles__oxartnum}]&size=450_450_100" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
					[{/if}]
				</a>
			</div>
		</td>

		<td class="ebenetitel">
[{* Titel *}]
			<span class="headline">
				[{if $product->isBuyable()}]
					[{$product->oxarticles__oxtitle->value }]
				[{else}]
					[{$product->oxarticles__oxtitle->value }] [{$product->oxarticles__oxvarselect->value}]
				[{/if}]
			</span>
			<br/>
			<form>
[{* Standartartikel *}]
				[{if $product->isBuyable()}]
				<div class="row">
					<div class="col-sm-4">
						<div class="input-line">
							[{* Mengenwähler *}]
							<div class="amount_input spinner input-group bootstrap-touchspin pull-left">
								<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-down">-</button></span>
								<!--<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>-->
								<input class="form-control amount" type="numeric" name="amount[[{$product->oxarticles__oxid}]]" value="[{if $product->orderedamount}][{$product->orderedamount}][{else}]0[{/if}]" style="display: block;">
								<span class="input-group-btn"><button type="button" class="btn btn-default bootstrap-touchspin-up">+</button></span>
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="pull-left">
							[{* Info Button *}]
							<button type="button" class="btn btn-info btn btn-default ladda-button bntooltip pull-left" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>
							[{* Herz Button *}]
							<button type="button" class="btn btn-favorite btn btn-default ladda-button pull-left [{if $product->isFavorite()}]btn-primary[{/if}] bntooltip" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="ADD_TO_FAVORITE" }]"><span class="fa fa-heart-o"></span></button>
							[{* Artikelnummer & VE-Einheit *}]
							<div class="artnr pull-left">
								[{* Artikelnummer *}]
								[{oxmultilang ident="ARTNR" suffix="COLON"}] [{$product->oxarticles__oxartnum}]
							</div>

							[{* Verpackungseinheit *}]
							<div class="pull-right" style="margin-top: 2px; margin-left:5px;">
								[{if $product->oxarticles__verpackungseinheit->value ne "0" and $product->oxarticles__verpackungseinheit->value > 1}]
								<span style="margin:0">
									[{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$product->oxarticles__verpackungseinheit->value}]<a onclick="addVeToInput('[{$product->oxarticles__oxid->value}]' , '[{$product->oxarticles__verpackungseinheit->value}]')" style="margin-top:0" type="button" class="btn btn-default btn-prima btn-basket ladda-button bntooltip" data-placement="top" title="[{oxmultilang ident="TT-VE-ADD"}]"><span style="width: 5px !important;" class=""><strong>+</strong></span></a>
								</span>
								[{/if}]
							</div>

							[{* Anzeige nur bei Admin *}][{* Preis Neu *}]
							[{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'*}]
							[{*if $product->getPreisNeu() > 0}]
								<div class="pull-right" style="margin-top: 2px; margin-left:5px;font-size: small">
									<span style="margin:0">
											[{oxmultilang ident="PREISNEU" suffix="COLON"}] <b>[{oxprice price=$product->getPreisNeu() currency=$oView->getActCurrency()}]</b>
									</span>
								</div>
							[{/if*}]
							[{*/if*}]
						</div>
					</div>
				</div>

				[{else}]
[{* Vaterartikel *}]
					<div class="row">
						<div class="col-sm-4">
							<div class="input-line">
								[{* Varianten Button *}]
								<button type="button" class="btn btn-default btn-show-variants ladda-button optionen bntooltip" data-style="expand-right" onclick="loadProductOptions('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_SHOW_OPTIONS"}]"><span class="glyphicon glyphicon-list"></span></button>
							</div>
						</div>
						<div class="col-sm-8">
							<div class="pull-left">
								[{* Info Button *}]
								<button type="button" class="btn btn-info btn btn-default ladda-button bntooltip pull-left" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>
								[{* Herz Button *}]
								<button type="button" class=" btn btn-favorite btn btn-default ladda-button bntooltip pull-left [{if $product->isFavorite()}]btn-primary[{/if}]" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="ADD_TO_FAVORITE"}]"><span class="fa fa-heart-o"></span></button>
								[{* Artikelnummer *}]
								<div class="artnr pull-left">
									[{oxmultilang ident="ARTNR" suffix="COLON"}] [{$product->oxarticles__oxartnum}]
								</div>
							</div>
						</div>
					</div>
					[{*<div class="input-line">
						[{* Varianten Button }]
						<button type="button" class="btn btn-default btn-show-variants ladda-button optionen bntooltip" data-style="expand-right" onclick="loadProductOptions('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_SHOW_OPTIONS"}]"><span class="glyphicon glyphicon-list"></span></button>
						[{* Info Button }]
						<button type="button" class="btn btn-info btn btn-default ladda-button bntooltip" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>
						[{* Herz Button }]
						<button type="button" class=" btn btn-favorite btn btn-default ladda-button bntooltip [{if $product->isFavorite()}]btn-primary[{/if}]" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="ADD_TO_FAVORITE"}]"><span class="fa fa-heart-o"></span></button>
						[{* Artikelnummer }]
						<div class="artnr">
							[{oxmultilang ident="ARTNR" suffix="COLON"}] [{$product->oxarticles__oxartnum}]
						</div>
					</div>
					*}]
				[{/if}]
			</form>
		</td>
[{* Warenkorb Button *}]
		<td class="Warenkorb">
			<form>
				<div class="input-line pull-right">
					[{if $product->isBuyable()}]
						[{* Warenkorbbutton *}]
						<button type="button" class="btn btn-default btn-prima btn-basket ladda-button btn-success pull-right bntooltip" data-style="expand-right" onclick='addToBasket("[{$product->oxarticles__oxid}]","[{*oxmultilang ident="WK-OK"*}]","[{*oxmultilang ident="WK-ERR"*}]", "[{*oxmultilang ident="WK-NO-ART"*}]")' data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span class="glyphicon glyphicon-shopping-cart"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]</button>
					[{/if}]
				</div>
			</form>
		</td>
[{* Preis *}]
		<td class="selprice">
			<label id="productPrice_[{$iIndex}]" class="price">
				<span>
					[{if $product->isRangePrice()}]
						[{oxmultilang ident="PRICE_FROM" }]
						[{if !$product->isParentNotBuyable() }]
							[{assign var="oPrice" value=$product->getMinPrice()}]
						[{else}]
							[{assign var="oPrice" value=$product->getVarMinPrice()}]
						[{/if}]
					[{else}]
						[{if !$product->isParentNotBuyable() }]
							[{assign var="oPrice" value=$product->getPrice()}]
						[{else}]
							[{assign var="oPrice" value=$product->getVarMinPrice() }]
						[{/if}]
					[{/if}]
				</span>
				[{oxprice price=$oPrice currency=$oView->getActCurrency()}]
				[{if $oView->isVatIncluded() }]
					[{if !($product->hasMdVariants() || ($oViewConf->showSelectListsInList() && $product->getSelections(1)) || $product->getVariants())}]*[{/if}]
				[{/if}]
			</label>
		</td>
[{* Ampel *}]
		<td class="ampelcolumn">
			[{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
		</td>
	</tr>
	[{if !$product->isBuyable()}]
		<tr id="article_selected_[{$product->oxarticles__oxid|replace:".":"_"}]" class="articleselect">
			<td colspan="5">
				[{*
					bn_selections.tpl
					empty for viewing selection area
				*}]
			</td>
		</tr>
	[{/if}]
[{/block}]
