<!-- Navi-Leiste -->
<!--<div class="div-navbar-menue navbar-collapse collapse">-->
<div class="navbar-collapse collapse nav nav-tabs nav-justified" id="naviLeiste">

    <!--<ul id="menu" class="nav navbar-nav navbar-menue">-->
        [{foreach from=$oxcmp_categories item=ocat key=catkey name=root}]
            [{if $ocat->getIsVisible() }]
                [{foreach from=$ocat->getContentCats() item=oTopCont name=MoreTopCms}]
                    [{assign var="iCatCnt" value=$iCatCnt+1 }]
                    [{assign var="iAllCatCount" value=$iAllCatCount+1 }]
                    [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
                        [{assign var="bHasMore" value="true"}]
                        [{assign var="iCatCnt" value=$iCatCnt+1}]
                    [{/if}]

                    [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
                        <li class="" oxid="[{$oTopCont->oxcategories__oxid->value}]" type="oxcategories"><a href="[{$oTopCont->getLink()}]"><span class="glyphicon glyphicon-pushpin sort-visible"></span>[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
                    [{else}]
                        [{capture append="moreLinks"}]
                            <li><a href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
                        [{/capture}]
                    [{/if}]
                [{/foreach}]

                [{assign var="iCatCnt" value=$iCatCnt+1 }]
                [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
                    [{assign var="bHasMore" value="true"}]
                    [{assign var="iCatCnt" value=$iCatCnt+1}]
                [{/if}]

                [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
                    <!--<li class="dropdown [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}]">-->
                    <li role="presentation" class="dropdown  [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}]" oxid="[{$ocat->oxcategories__oxid->value}]" type="oxcategories">
                        [{if $ocat->getSubCats()}]
                            <a class="dropdown [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " data-toggle="dropdown" href="[{$ocat->getLink()}]">[{*}]<span class="glyphicon glyphicon-pushpin sort-visible"></span>[{*}][{$ocat->oxcategories__oxtitle->value}] <span class="caret"></span></a>
                            <ul class="dropdown-menu " role="menu" id="navsort[{$iCatCnt}]">
                                <li><a class="[{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " href="[{$ocat->getLink()}]">[{oxmultilang ident="AlleArtikel"}] <span class="caret"></span></a></li>
                                [{foreach from=$ocat->getSubCats() item=osubcat key=subcatkey name=SubCat}]

                                    [{*foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
                                        <li><a href="[{$ocont->getLink()}]">[{$ocont->oxcontents__oxtitle->value}]</a></li>
                                    [{/foreach*}]

                                    [{if $osubcat->getIsVisible() }]
                                        <li class="[{*connectedSortable*}] [{if $homeSelected == 'false' &&
                                        $osubcat->expanded}]current[{/if}] [{if $osubcat->getSubCats()}]dropdown-submenu[{/if}]" oxid="[{$osubcat->oxcategories__oxid->value}]" type="oxcategories">
                                            <a [{if $homeSelected == 'false' && $osubcat->expanded}]class="current"[{/if}] href="[{$osubcat->getLink()}]"><span class="glyphicon glyphicon-pushpin sort-visible"></span>[{$osubcat->oxcategories__oxtitle->value}]</a>
                                            [{if $osubcat->getSubCats()}]
                                                <ul class="dropdown-menu" role="menu">
                                                    [{foreach from=$osubcat->getSubCats() item=osubsubcat key=subsubcatkey name=SubSubCat}]
                                                        <li><a class="dropdown-toggle"  href="[{$osubsubcat->getLink()}]">[{$osubsubcat->oxcategories__oxtitle->value}]</a></li>
                                                    [{/foreach}]
                                                </ul>
                                            [{/if}]
                                        </li>
                                    [{/if}]
                                [{/foreach}]
                            </ul>
                        [{else}]
                            <a  [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}] href="[{$ocat->getLink()}]"><span class="glyphicon glyphicon-pushpin sort-visible"></span>[{$ocat->oxcategories__oxtitle->value}]</a>
                        [{/if}]
                    </li>
                [{else}]
                    [{capture append="moreLinks"}]
                        <li [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}]>
                            <a href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}]</a>
                        </li>
                    [{/capture}]
                [{/if}]
            [{/if}]
        [{/foreach}]
</div><!-- Ende: Navi-Leiste -->


