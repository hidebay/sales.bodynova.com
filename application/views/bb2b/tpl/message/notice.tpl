<div class="alert alert-warning" role="alert">
    <p>[{ $statusMessage }]</p>
</div>