[{capture append="oxidBlock_pageBody"}]
	[{if $oView->showRDFa()}]
		[{include file="rdfa/rdfa.tpl"}]
	[{/if}]
	[{include file="layout/header.tpl"}]
	[{*include file="widget/newsModal.tpl"*}]
	<div id="wrapper" class="container-fluid">
		<!--<div id="content" class="container">-->
		<div class="row">
			[{if $sidebarLeft == 1}]
				<!-- <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="205">-->
				<div id="sidebarLeft" class="col-xs-3 col-sm-3 col-md-2 col-lg-2 affix-top" data-spy="affix"
				     data-offset-top="205">
					[{*include file="layout/sidebar.tpl"*}]
					[{include file="layout/sidebarLeft.tpl"}]
				</div>
			[{/if}]
			<!--<div id="contentwrapper">-->
			<div id="middelContent"
			     class="col-xs-8 col-sm-8 col-md-7 col-lg-7 col-xs-push-3 col-sm-push-3 col-md-push-2 col-lg-push-2"
			     role="main">
				[{* Banner-Slider *}]
				<!--<div class="slidercontent">-->
				[{* Banner *}]
				[{if $oView->getClassName()=='start' && $oView->getBanners()|@count > 0 }]
					<!--<div class="oxSlider">-->
					[{*include file="widget/promoslider.tpl" *}]
					[{include file="widget/promosliderNeu.tpl" }]
					<!--</div>-->
				[{/if}]
				<!--</div>-->
				[{if $oView->getClassName() ne "start" &&
					!$blHideBreadcrumb &&
					$oView->getClassName() ne "alist" &&
					$oView->getClassName() ne "search" &&
					$oView->getClassName() ne "basket" &&
					$oView->getClassName() ne "user" &&
					$oView->getClassName() ne "order" &&
					$oView->getClassName() ne "thankyou" &&
					$oView->getClassName() ne "quickorder" &&
					$oView->getClassName() ne "contact" &&
					$oView->getClassName() ne "account" &&
					$oView->getClassName() ne "myfavorites" &&
					$oView->getClassName() ne "content" &&
					$oView->getClassName() ne "account_newsletter" &&
					$oView->getClassName() ne "account_password" &&
					$oView->getClassName() ne "account_user" &&
					$oView->getClassName() ne "account_order" }]
					[{include file="widget/breadcrumb.tpl"}]
				[{/if}]
				[{include file="message/errors.tpl"}]
				[{foreach from=$oxidBlock_content item="_block"}]
					[{$_block}]
					<br>
				[{/foreach}]
			</div>
			<!--</div>-->
			[{if $sidebarRight == 1}]
				<div id="sidebarRight" class="hidden-xs hidden-sm col-md-3 col-lg-3 affix" role="complementary">
					<div id="fixedWarenkorb" class="affix-top">
						[{include file="layout/sidebarRight.tpl"}]
					</div>
				</div>
			[{/if}]
		</div>
		<!--</div>-->
		[{*include file="layout/footer.tpl"*}]
		[{*<div id="page" class="container [{if $sidebar}] sidebar[{$sidebar}][{/if}]"></div>*}]
		[{*include file="widget/facebook/init.tpl"*}]
		[{*if $oView->isPriceCalculated() *}]
		[{block name="layout_page_vatinclude"}]
			[{oxifcontent ident="oxdeliveryinfo" object="oCont"}]
			[{assign var="tsBadge" value=""}]
				[{*if $oView->getTrustedShopId()}]
					[{assign var="tsBadge" value="TsBadge"}]
				[{/if*}]
				<div id="incVatMessage[{$tsBadge}]">
					* <span class="deliveryInfo">[{ oxmultilang ident="PLUS" }]<a href="" rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
					[{if $oView->isVatIncluded()}]
						*
						<span class="deliveryInfo">[{ oxmultilang ident="PLUS_SHIPPING" }]<a
									href="[{$oCont->getLink() }]"
									rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
					[{else}]
						*
						<span class="deliveryInfo">[{ oxmultilang ident="PLUS" }]<a href="[{$oCont->getLink()}]" rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
					[{/if}]
				</div>
			[{/oxifcontent }]
			<div id="incVatMessage">
				<a href="#" class="back-to-top"><i class="fa fa-arrow-circle-up"></i></a>
				<span class="deliveryInfo">[{oxmultilang ident="INCL_TAX_AND_PLUS_SHIPPING"}]</span>
			</div>
		[{/block}]
		[{*/if*}]
	</div>
[{/capture}]
[{include file="layout/base.tpl"}]
